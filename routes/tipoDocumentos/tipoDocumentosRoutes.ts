import {Request, Response, Router} from "express";
import TipoDocumentosQueries from "../../queries/tipoDocumentos/TipoDocumentosQueries";
const generales = require('../../general/function');

const tipoDocumentosRoutes = Router();

tipoDocumentosRoutes.get('/:idTabla', async (req: Request, res: Response, next) => {
    try {
        const {idTabla} = req.params;
        if (!idTabla) return generales.manejoErrores('No se envio el id tabla', res);
        const tiposDocumentos = await TipoDocumentosQueries.getByIdTablaAndActivos(idTabla);
        res.status(200).send(tiposDocumentos);
    } catch (err) {
        next(err);
    }
})

export default tipoDocumentosRoutes;
