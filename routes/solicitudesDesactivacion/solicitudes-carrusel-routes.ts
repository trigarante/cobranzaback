import {Request, Response, Router} from "express";
import solicitudesCarruselQueries from "../../queries/solicitudesDesactivacion/solicitudes-carrusel-queries";
import jwtAuthentica from "../../middlewares/jwtAuthentica";
const generales = require('../../general/function');

const solicitudesCarruselRoutes = Router();

solicitudesCarruselRoutes.get('/by-estado', jwtAuthentica, async (req: Request, res: Response, next) => {
    try {
    const idEstadoSolicitudCarrusel = Number(req.headers.idestadosolicitudcarrusel);
    if(!idEstadoSolicitudCarrusel)
        return generales.manejoErrores('No se envio idEstadoSolicitudCarrusel', res)
    const carrusel = await solicitudesCarruselQueries.findAllByIdEstadoSolicitudCarrusel(idEstadoSolicitudCarrusel);
    res.status(200).send( carrusel );
    } catch (e) {
        res.status(500).send(e)
    }
});


export default solicitudesCarruselRoutes;
