import {Request, Response, Router} from "express";
import axios from "axios";
import RegistroQueries from "../../queries/registro/registroQueries";
import PrepagoQueries from "../../queries/prepago/prepagoQueries";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";

const router = Router();

router.get('/linksCobranza', async (req: Request, res: Response, next) => {
    try {
        const {empleado} = req.headers;
        if (!empleado) return res.status(500).send({mensaje: 'No se envió el empleado'});
        const data = await PrepagoQueries.getCobranza(+empleado);
        res.send(data);
    } catch (e) {
        next(e)
    }
});
router.get('/linksENR', async (req: Request, res: Response, next) => {
    try {
        const {empleado} = req.headers;
        if (!empleado) return res.status(500).send({mensaje: 'No se envió el empleado'});
        const data = await PrepagoQueries.getENR(+empleado);
        res.send(data);
    } catch (e) {
        next(e)
    }
});
router.get('/linksSubs', async (req: Request, res: Response, next) => {
    try {
        const {empleado} = req.headers;
        if (!empleado) return res.status(500).send({mensaje: 'No se envió el empleado'});
        const data = await PrepagoQueries.getSubsecuentes(+empleado);
        res.send(data);
    } catch (e) {
        next(e)
    }
});
router.get('/getInfoCliente', async (req: Request, res: Response, next) => {
    try {
        const {idrecibo, idregistro} = req.headers;
        if (!(idrecibo && idregistro)) return res.status(500).send({mensaje: 'No se envió la info'});
        const data = await PrepagoQueries.getInfoCliente(idregistro, idrecibo);
        res.send(data);
    } catch (e) {
        next(e)
    }
});

router.get('/getById', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return res.status(500).send({mensaje: 'No se envió el id'});
        const data = await PrepagoQueries.getById(id);
        res.send(data);
    } catch (e) {
        next(e)
    }
});

router.post('', async (req: Request, res: Response, next) => {
    try {
        const data = req.body;
        const registro: any = await RegistroQueries.getRegistroById(data.idRegistro);
        const id = await PrepagoQueries.post({...data, idMedioContacto: 1, fechaVigencia: registro.fechaInicio});
        const route = `https://core-mejorseguro.com/v1/prepaid/send/link?idPrepaid=${id}`;
        let url;
        await axios.get(route).then(async response => {
            url = response.data.urlWeb;
            await PrepagoQueries.update({url}, id);
        })
        res.send({url});
    } catch (e) {
        next(e)
    }
});
router.get('', async (req: Request, res: Response, next) => {
    try {
        const {empleado} = req.headers;
        if (!empleado) return res.status(500).send({mensaje: 'No se envió el empleado'});
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+empleado);
        let query: string = '';
        switch (permisos.idPermisoVisualizacion) {
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
                const aux: any[] = [];
                for (const departamento of departamentos) {
                    // @ts-ignore
                    aux.push(departamento.idDepartamento);
                }
                if(aux.length === 0) {
                    aux.push(permisos.idDepartamento);
                }
                query = `AND operaciones.registro."idDepartamento" IN (${aux})`;
                break;
            case 3:
                query = `AND operaciones.recibos."idEmpleado" = ${empleado}`;
                break;
        }
        const data = await PrepagoQueries.get(query);
        res.send(data);
    } catch (e) {
        next(e)
    }
});
export default router;
