import {Router, Request, Response} from "express";
import ClienteQueries from "../../queries/cliente/cliente-queries";
import DriveApiService from "../../services/drive/driveApiService";
import routes from "../index";
import edoCuentaRoutes from "../edoCuenta/edoCuentaRoutes";
const generales = require('../../general/function');

const clienteRoutes = Router();
// lo usa cotizador  online
clienteRoutes.get('/byId/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Cliente', res);
        const cliente = await ClienteQueries.clienteAll(id);
        res.status(200).send( cliente );
    } catch (e) {
        res.sendStatus(500);
    }
});
// lo usa cotizador  online
clienteRoutes.get('/existeCurp', async(req:Request, res: Response, next) => {
    try {
        const {curp} = req.headers;
        if (!curp ) return generales.manejoErrores('No se envio el Curp', res);
        const cliente = await ClienteQueries.getByCurp(curp);
        res.status(200).send(cliente);
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.get('/existeRFC', async(req:Request, res: Response, next) => {
    try {
        const {rfc} = req.headers;
        if (!rfc ) return generales.manejoErrores('No se envio el RFC', res);
        const cliente = await ClienteQueries.getByRFC(rfc);
        res.status(200).send(cliente);
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.post('', async(req:Request, res: Response, next) => {
    try {
        const id = await ClienteQueries.post(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});

clienteRoutes.put('/:idCliente', async(req:Request, res: Response, next) => {
    try {
        const {idCliente} = req.params;
        if (!idCliente) return generales.manejoErrores('No se envio el id', res);
        const edito = await ClienteQueries.update(req.body, idCliente)
        res.status(200).send(edito)
    } catch (e) {
        res.sendStatus(500);
    }
});
// Subida de archivos a drive  cliente
// routes.post('/subir-archivo', async (req: Request, res: Response, next) => {
//     try {
//         if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
//         // Recibo archivo
//         const files = req['files'].file;
//         /* Se deben enviar 2 id, el de cliente y producto cliente
//         la carpreta del cliente se creara con el id cliente
//         y dentro se guardara un idProductoCliente-cliente para tener las carpetas de cliente pero con
//         varios productos cliente
//          */
//         const {idcliente} = req.headers;
//         if (!(idcliente  )) return generales.manejoErrores('No se envio el id', res);
//         const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
//         const archivo = await DriveApiService.subirArchivo(idFolder, files,  idcliente + '-cliente')
//         await ClienteQueries.update({archivo}, idcliente)
//         return res.status(200).send({archivo});
//         // return res.status(200).send();
//     } catch (err) {
//         next(err)
//     }
// });

export default clienteRoutes;
