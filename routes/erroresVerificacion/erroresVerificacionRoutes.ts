import {Request, Response, Router} from "express";
import ErroresVerificacionQueries from "../../queries/erroresVerificacion/ErroresVerificacionQueries";
import ErroresAutorizacionQueries
    from "../../queries/AutorizacionErrores/ErroresAutorizacion/ErroresAutorizacionQueries";
const generales = require('../../general/function');
const erroresVerificacionRoutes = Router();

erroresVerificacionRoutes.get('/:idVerificacionRegistro/:idTipoDocumento', async (req: Request, res: Response, next) => {
    try {
        const {idVerificacionRegistro,idTipoDocumento} = req.params;
        if (!(idVerificacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se envio el id', res);
        const tiposDocumentos = await ErroresVerificacionQueries.getByIdAutorizacionAndDocumento(idVerificacionRegistro, idTipoDocumento);
        res.status(200).send(tiposDocumentos);
    } catch (err) {
        next(err);
    }
})

erroresVerificacionRoutes.put('/:idErrorVerificacion', async(req:Request, res: Response, next) => {
    try {
        const {idErrorVerificacion} = req.params;
        if (!idErrorVerificacion) return generales.manejoErrores('No se envio el id', res);
        await ErroresVerificacionQueries.update({idEstadoCorreccion: 2, fechaCorreccion: new Date()}, idErrorVerificacion)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});
// erroresVerificacionRoutes.post('/', async (req: Request, res: Response, next) => {
//     try {
//         await ErroresAnexosAQueries.post(req.body)
//         return res.status(200).send();
//     } catch (err) {
//         next(err)
//     }
// });

export default erroresVerificacionRoutes;

