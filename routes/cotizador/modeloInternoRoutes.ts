import {Request, Response, Router} from "express";
import generales from "../../general/function";
import ModeloInternoQueries from "../../queries/cotizador/modeloInternoQueries";

const router = Router();

router.get('/getByIdTipoSubramo', async(req:Request, res: Response, next) => {
    try {
        const modelos = await ModeloInternoQueries.getAll();
        res.status(200).send(modelos);
    } catch (e) {
        res.status(500).send(e)
    }
})

export default router;
