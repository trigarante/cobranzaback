import {Request, Response, Router} from "express";
import generales from "../../general/function";
import ProductosSocioQueries from "../../queries/productosSocio/productosSocioQueries";

const router = Router();

router.get('/getByIdSubRamo', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await ProductosSocioQueries.getByIdSubRamo(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
router.get('/id-subramoNombre/:idSubramo/:nombre', async (req: Request, res: Response, next) => {
    try {
        const idSubRamo = req.params.idSubramo;
        const nombre = req.params.nombre;

        if (!idSubRamo) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await ProductosSocioQueries.getByIdSubRamoNombre(idSubRamo, nombre);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})

export default router;
