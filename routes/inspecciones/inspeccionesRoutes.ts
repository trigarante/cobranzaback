import {Request, Response, Router} from "express";
import generales from "../../general/function";
import DriveApiService from "../../services/drive/driveApiService";
import RegistroQueries from "../../queries/registro/registroQueries";
import InspeccionesQueries from "../../queries/inspecciones/InspeccionesQueries";
const routes = Router();

// Subida de archivos a drive producto cliente
routes.post('/All', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        const file = req['files'].file;
        let inspeccion = JSON.parse(req.body.inspeccion);
        const ids = await RegistroQueries.getAllIdsSteps(+req.body.id)
        const idFolder = await DriveApiService.findOrCreateFolder(1, ids.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, ids.id, idFolder);
        inspeccion.archivo = await DriveApiService.subirArchivo(idFolderPago, file, ids.id + '-inspeccion');
        await InspeccionesQueries.post(inspeccion);
        await RegistroQueries.update({emitida: 1}, +req.body.id);
        return res.status(200).send();
    } catch (err) {
        next(err)
        console.log(err)
    }
});

export default routes;
