import {Request, Response, Router} from "express";
import jwtAuthentica from "../../middlewares/jwtAuthentica";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
const generales = require('../../general/function');

const solicitudesInternoRoutes = Router();

solicitudesInternoRoutes.get('/by-idEmpleado', jwtAuthentica, async (req: Request, res: Response, next) => {
    try {
        const idEmpleado = Number(req.headers.idempleado);
        if (!idEmpleado)
            return generales.manejoErrores('No se envió algún parámetro requerido', res)
        const solicitudes = await SolicitudesInternoQueries.getSolicitudes(idEmpleado)
        res.status(200).send(solicitudes);

    } catch (err) {
        next(err);
    }
});

solicitudesInternoRoutes.get('/todas/:idEmpleado/:fechaInicio/:fechaFin/:idTipoContacto/:idEstadoSolicitud', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin && idEstadoSolicitud && idTipoContacto)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let solicitudes;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                solicitudes = await SolicitudesInternoQueries.getAll(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     solicitudes = await SolicitudesInternoQueries.getAllByHijos(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud,aux)
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    solicitudes = await SolicitudesInternoQueries.getAllByDepartamentos(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud, aux)
                // }
                break;
            case 3:
                solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud)
                break;
        }
        res.status(200).send(solicitudes);
    } catch (err) {
        next(err);
    }
});

// solicitudesInternoRoutes.get('/fecha/:idEstado/:idTipoContacto/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
//     try {
//         const {idEstado, idTipoContacto, idEmpleado, fechaInicio, fechaFin} = req.params;
//         if (!(idEstado && idTipoContacto && idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
//         const permisos = await SolicitudesInternoQueries.getPermisosVisualizacion(+idEmpleado);
//         let solicitudes;
//         switch (+permisos.idPermisoVisualizacion) {
//             case 1:
//                 solicitudes = await SolicitudesInternoQueries.getAllTipo(idEstado, idTipoContacto, fechaInicio, fechaFin)
//                 break;
//             case 2:
//                 const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
//                 const aux = [];
//                 for (const departamento of departamentos) {
//                     aux.push(departamento.idDepartamento)
//                 }
//                 solicitudes = await SolicitudesInternoQueries.getAllByDepartamentosTipo(idEstado, idTipoContacto, fechaInicio, fechaFin, aux)
//                 break;
//             case 3:
//                 solicitudes = await SolicitudesInternoQueries.getAllByDepartamentoAndIdEmpleadoTipo(permisos.idDepartamento, idEstado, idTipoContacto, idEmpleado, fechaInicio, fechaFin)
//                 break;
//         }
//         res.status(200).send(solicitudes);
//     } catch (err) {
//         next(err);
//     }
// });

solicitudesInternoRoutes.get('/interno', async (req: Request, res: Response, next) => {
    try {
        const idEstadoContacto = Number(req.headers.idestadocontacto);
        if (!idEstadoContacto)
            return generales.manejoErrores('No se envió algún parámetro requerido', res)
        const solicitudes = await SolicitudesInternoQueries.getByIdEstadoContacto(idEstadoContacto)
        res.status(200).send(solicitudes);
    } catch (err) {
        next(err);
    }
});
// lo usa cotizador  online
solicitudesInternoRoutes.get('/get-by-id/:idSolicitud', async (req: Request, res: Response, next) => {
    try {
        const idSolicitud = Number(req.params.idSolicitud);
        const solicitud = await SolicitudesInternoQueries.getById(idSolicitud)
        if (!solicitud)
            return generales.manejoErrores('No se encontro ninguna solicitud con tu id', res)
        res.status(200).send(solicitud);
    } catch (err) {
        next(err);
    }
});

solicitudesInternoRoutes.get('/steps/:idSolicitud', async (req: Request, res: Response, next) => {
    try {
        const {idSolicitud} = req.params;
        if (!idSolicitud)
            return generales.manejoErrores('No se envio ningun id', res)
        const solicitud = await SolicitudesInternoQueries.getStepsById(idSolicitud)
        res.status(200).send(solicitud);
    } catch (err) {
        next(err);
    }
});
// Traer la info del prospecto en step cliente
solicitudesInternoRoutes.get('/stepCliente', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res)
        const solicitud = await SolicitudesInternoQueries.getInfoProspectoStepCliente(id)
        res.status(200).send(solicitud);
    } catch (err) {
        next(err);
    }
});
solicitudesInternoRoutes.put('/cambiar-estado/:idSolicitud/:idEstadoSolicitud', async (req: Request, res: Response, next) => {
   try {
       await SolicitudesInternoQueries.update({idEstadoSolicitud: +req.params.idEstadoSolicitud}, +req.params.idSolicitud);
       res.json(`Estado solicitud de lead ${+req.params.idSolicitud} actualizado`);
   } catch (e) {
       res.status(500).json(e);
   }
});
// lo usa cotizador  online
solicitudesInternoRoutes.get('/get-by-idOnline/:idSolicitud', async (req: Request, res: Response, next) => {
    try {
        const idSolicitud = Number(req.params.idSolicitud);
        const solicitud = await SolicitudesInternoQueries.getByIdOnline(idSolicitud)
        if (!solicitud)
            return generales.manejoErrores('No se encontro ninguna solicitud con tu id', res)
        res.status(200).send(solicitud);
    } catch (err) {
        next(err);
    }
});

solicitudesInternoRoutes.put('/update-empleado/:id/:idEmpleado', async (req: Request, res: Response, next) => {
    try {
        const {id, idEmpleado} = req.params;
        if (!(id && idEmpleado)) return generales.manejoErrores('No se envio la info', res);
        await SolicitudesInternoQueries.update({idEmpleado}, +id);
        res.status(200).send();
        console.log(req.params);
    } catch (err) {
        next(err);
    }
});


export default solicitudesInternoRoutes;
