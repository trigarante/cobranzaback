import {Router, Request, Response} from "express";
import CorreccionErroresAutorizacionQueries from "../../queries/correccionErrores/CorreccionErroresAutorizacionQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import ErroresAutorizacionPagoQueries
    from "../../queries/AutorizacionErrores/erroresAutorizacionPago/erroresAutorizacionPagoQueries";

const generales = require('../../general/function');
const correccionErroresAutorizacionRoutes = Router();

correccionErroresAutorizacionRoutes.get('/autorizacion/anexo/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await CorreccionErroresAutorizacionQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                break;
            case 3:
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})
correccionErroresAutorizacionRoutes.get('/autorizacion/datos/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await CorreccionErroresAutorizacionQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAllDatos(fechaInicio, fechaFin)
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAllByDepartamentosDatos(fechaInicio, fechaFin,aux)
                break;
            case 3:
                autorizacionRegistro = await CorreccionErroresAutorizacionQueries.getAllByDepartamentoAndIdEmpleadoDatos(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})

correccionErroresAutorizacionRoutes.get('/erroresAutorizacionPago/:idAutorizacionPago', async(req:Request, res: Response, next) => {
    try {
        const {idAutorizacionPago} = req.params;
        const erroresAutorizacionPago = await ErroresAutorizacionPagoQueries.getByIdAutorizacionAndDocumento(idAutorizacionPago);
        let exite;
        if (erroresAutorizacionPago === undefined || erroresAutorizacionPago == null)
            exite = false;
        else
            exite = true;
        res.status(200).send(exite);
    } catch (e) {
        res.status(500).send(e)
    }
})

export default correccionErroresAutorizacionRoutes;
