import {Request, Response, Router} from "express";
import ErroresAnexosAQueries from "../../queries/erroresAnexos/erroresAnexosAQueries";
const generales = require('../../general/function');
const erroresAnexosRoutes = Router();

erroresAnexosRoutes.get('/:idAutorizacionRegistro/:idTipoDocumento', async (req: Request, res: Response, next) => {
    try {
        const {idAutorizacionRegistro,idTipoDocumento} = req.params;
        if (!(idAutorizacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se envio el id', res);
        const tiposDocumentos = await ErroresAnexosAQueries.getByIdAutorizacionAndDocumento(idAutorizacionRegistro, idTipoDocumento);
        res.status(200).send(tiposDocumentos);
    } catch (err) {
        next(err);
    }
})
erroresAnexosRoutes.post('/', async (req: Request, res: Response, next) => {
    try {
        await ErroresAnexosAQueries.post(req.body)
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

export default erroresAnexosRoutes;
