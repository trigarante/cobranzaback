import {Request, Response, Router} from "express";
import VisualizacionesGacetaQueries
    from "../../../queries/desarrolloOrganizacional/gaceta/visualizacionesGacetaQueries";
const generales = require('../../../general/function')

const VisualizacionesGacetaRoutes = Router();

VisualizacionesGacetaRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        const visualizar = await VisualizacionesGacetaQueries.create(req.body);
        return res.status(200).send(visualizar);
    } catch (err) {
        next(err);
    }
});

VisualizacionesGacetaRoutes.get('/visualizacionByIdEmpleado/:idEmpleado', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envió el id', res);
        const visualizacion = await VisualizacionesGacetaQueries.getVisualizacionByIdEmpleado(+idEmpleado);
        return res.status(200).send(visualizacion);
    } catch (err) {
        next(err);
    }
})

VisualizacionesGacetaRoutes.get('/byIdGaceta/:idGaceta', async (req: Request, res: Response, next) => {
    try {
        const {idGaceta} = req.params;
        if (!idGaceta) return generales.manejoErrores('No se envió el id', res);
        const visualizacion = await VisualizacionesGacetaQueries.getByIdGaceta(+idGaceta);
        return res.status(200).send(visualizacion);
    } catch (err) {
        next(err);
    }
})

VisualizacionesGacetaRoutes.get('/gacetaNoVisualizada/:idGaceta', async (req: Request, res: Response, next) => {
    try {
        const {idGaceta} = req.params;
        if (!idGaceta) return generales.manejoErrores('No se envió el id', res);
        const visualizacion = await VisualizacionesGacetaQueries.getNoVisualizadas(+idGaceta);
        return res.status(200).send(visualizacion);
    } catch (err) {
        next(err);
    }
})

export default VisualizacionesGacetaRoutes;
