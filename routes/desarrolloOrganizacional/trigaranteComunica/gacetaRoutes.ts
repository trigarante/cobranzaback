import {Request, Response, Router} from "express";
import DriveApiService from "../../../services/drive/driveApiService";
import GacetaQueries from "../../../queries/desarrolloOrganizacional/gaceta/gacetaQueries";
import ImagenesGacetaQueries from "../../../queries/desarrolloOrganizacional/gaceta/imagenesGacetaQueries";
const generales = require("../../../general/function");

const gacetaRoutes = Router();


gacetaRoutes.get('', async (req: Request, res: Response, next) => {
    try {
        const gacetaData = await GacetaQueries.getGacetas();
        return res.status(200).send(gacetaData);
    } catch (err) {
        next(err);
    }
});

gacetaRoutes.post('/subir-gaceta', async (req: Request, res: Response, next) => {
    try {
        let file = req['files'].file;
        if(!file) return generales.manejoErrores('No se envió archivo', res);
        if (!file.length) {
            file = [file];
        }
        const datos = JSON.parse(req.body.datos);
        console.log(datos)
        const idGaceta = await GacetaQueries.post(datos)
        const idFolder = await DriveApiService.findOrCreateFolder(5, idGaceta);
        for (let i = 0; i < file.length; i++) {
            const imagen = {
                idGaceta,
                numero: i+1,
                archivo: await DriveApiService.subirArchivo(idFolder, `${idGaceta}-${file[i].name}`, file[i]),
                link: datos.datosImagenes[0][file[i].name]
            }
            await ImagenesGacetaQueries.post(imagen)
        }
        return res.status(200).send()
    } catch (e) {
        next(e);
    }
})

gacetaRoutes.get('/getByIdGaceta/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envió el idGaceta', res);
        const gacetas = await ImagenesGacetaQueries.getById(+id);
        return res.status(200).send(gacetas);
    } catch (err) {
        next(err);
    }
})

gacetaRoutes.get('/activa',async (req: Request, res: Response, next) => {
    try {
        const gacetaActual = await ImagenesGacetaQueries.getGacetaActual()
        return res.status(200).send(gacetaActual);
    } catch (err) {
        next(err);
    }
})

gacetaRoutes.get('/del-mes', async (req: Request, res: Response, next) => {
    try {
        const gacetasDelMes = await GacetaQueries.getGacetasDelMes()
        return res.status(200).send(gacetasDelMes);
    } catch (err) {
        next(err);
    }
})

gacetaRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = Number(req.params.id);
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const gaceta = await GacetaQueries.update(id, req.body);
        return res.status(200).send(gaceta);
    } catch (err) {
        next(err);
    }
});

export default gacetaRoutes;
