import {Router} from "express";
import TrigaranteEscuchaRoutes from "./TrigaranteEscuchaRoutes";
import gacetaRoutes from "./trigaranteComunica/gacetaRoutes";
import VisualizacionesGacetaRoutes from "./trigaranteComunica/visualizacionesGacetaRoutes";

const router = Router();
// rutas
router.use("/trigarante-escucha", TrigaranteEscuchaRoutes);
router.use("/gaceta", gacetaRoutes);
router.use("/visualizaciones-gaceta", VisualizacionesGacetaRoutes);



export default router;
