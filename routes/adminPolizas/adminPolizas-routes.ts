import {Router, Request, Response} from "express";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
const generales = require('../../general/function');

const adminPolizasRoutes = Router();

adminPolizasRoutes.get('/cobranza/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        const {idpuesto} = req.headers;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        if (idpuesto === '8') {
            adminPolizas = await AdministradorPolizasQueries.getAllByIdEmpleado(fechaInicio, fechaFin,idEmpleado);
        }
        else {
            adminPolizas = await AdministradorPolizasQueries.getAll(fechaInicio, fechaFin);
        }
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasRoutes.get('/cobranza/:idEmpleado/polizas-totales/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        console.log('permisos ', permisos);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getAllPolizasTotales(fechaInicio, fechaFin)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})


adminPolizasRoutes.get('/cobranza/:idEmpleado/sin-aplicar/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                adminPolizas = await AdministradorPolizasQueries.getAllSinAPlciar(fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     adminPolizas = await AdministradorPolizasQueries.getAllByHijos(aux, fechaInicio, fechaFin)
                // } else {
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                adminPolizas = await AdministradorPolizasQueries.getAllSinAplicarByDepartamentos(aux, fechaInicio, fechaFin)
                // }
                break;
            case 3:
                adminPolizas = await AdministradorPolizasQueries.getAllSAByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasRoutes.get('/cobranza/:idEmpleado/sin-pagar/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                adminPolizas = await AdministradorPolizasQueries.getAllSinPagar(fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     adminPolizas = await AdministradorPolizasQueries.getAllByHijos(aux, fechaInicio, fechaFin)
                // } else {
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                adminPolizas = await AdministradorPolizasQueries.getAllSinPagarByDepartamentos(aux, fechaInicio, fechaFin)
                // }
                break;
            case 3:
                adminPolizas = await AdministradorPolizasQueries.getAllSPByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasRoutes.get('/cobranza/:idEmpleado/polizas/buscar-poliza/:poliza', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, poliza} = req.params;
        if (!(idEmpleado && poliza)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getPoliza(poliza)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasRoutes.get('/cobranza/:idEmpleado/polizas/buscar-numero/:numero', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, numero} = req.params;
        if (!(idEmpleado && numero)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getSerie(numero)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

// Subsecuentes
adminPolizasRoutes.get('/subsecuentes/:idEmpleado/internas/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getAllInternas(fechaInicio, fechaFin)
        if (adminPolizas === undefined)
            adminPolizas = [];
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasRoutes.get('/subsecuentes/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getAllSubsecuentes(fechaInicio, fechaFin)
        if (adminPolizas === undefined)
            adminPolizas = [];
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/subsecuentes/buscar-poliza', async(req:Request, res: Response, next) => {
    try {
        const {poliza, valor} = req.headers;
        if (!(poliza && valor)) return generales.manejoErrores('No se envio la poliza', res);
        const query = +valor === 2 ? 'and tp."cantidadPagos" > 1' : '';
        const adminPolizas = await AdministradorPolizasQueries.getAllPolizas(poliza, query)
        if (adminPolizas === undefined)
            adminPolizas = [];
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/subsecuentes/buscar-serie', async(req:Request, res: Response, next) => {
    try {
        const {numero, valor} = req.headers;
        if (!(numero && valor)) return generales.manejoErrores('No se envio el numero de serie', res);
        const query = +valor === 2 ? 'and tp."cantidadPagos" > 1' : '';
        const adminPolizas = await AdministradorPolizasQueries.getAllBySerie(numero, query)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/subsecuentes/:idEmpleado/polizas-totales/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getAllPolizasTotalesSubsecuentes(fechaInicio, fechaFin)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/ENR/PolizasCanceladas/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        if (!(fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun parametro', res);
        const polizasCanceladas = await AdministradorPolizasQueries.getAllPolizasCanceladasENR(fechaInicio, fechaFin);
        res.status(200).send( polizasCanceladas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/ENR/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        const {idpuesto} = req.headers;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        const adminPolizas = await AdministradorPolizasQueries.getAllENR(fechaInicio, fechaFin);
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/ENR/:idEmpleado/polizas/buscar-numero/:numero', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, numero} = req.params;
        if (!(idEmpleado && numero)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getENRBySerie(numero)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/ENR/:idEmpleado/polizas/buscar-poliza/:poliza', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, poliza} = req.params;
        if (!(idEmpleado && poliza)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let adminPolizas;
        adminPolizas = await AdministradorPolizasQueries.getENRByPoliza(poliza)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.post('/ENR/Cancelacion', async(req:Request, res: Response, next) => {
    try {
        const cancelacion = await AdministradorPolizasQueries.postCancelacionENR(req.body);
        res.status(200).send(cancelacion);
    } catch (e) {
        res.status(500).send(e)
    }
})
adminPolizasRoutes.get('/ENR/MotivosCancelacion', async(req:Request, res: Response, next) => {
    try {
        const motivos = await AdministradorPolizasQueries.getMotivosCancelacionENR();
        res.status(200).send( motivos );
    } catch (e) {
        res.status(500).send(e)
    }
})
export default adminPolizasRoutes;
