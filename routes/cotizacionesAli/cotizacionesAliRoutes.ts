import {Request, Response, Router} from "express";
import CotizacionesAliQueries from "../../queries/cotizador/cotizacionesAliQueries";
const generales = require('../../general/function');
const cotizacionesAliRoutes = Router();

cotizacionesAliRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const empleado = await CotizacionesAliQueries.getById(id);
        res.status(200).send(empleado);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
cotizacionesAliRoutes.get('/Online/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const empleado = await CotizacionesAliQueries.getByIdOnline(id);
        res.status(200).send(empleado);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
cotizacionesAliRoutes.put('/Online/:idCotizacionAli', async(req:Request, res: Response, next) => {
    try {
        const {idCotizacionAli} = req.params;
        if (!idCotizacionAli) return generales.manejoErrores('No se envio el id', res);
        await CotizacionesAliQueries.update(req.body, idCotizacionAli)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});


export default cotizacionesAliRoutes;

