import {Request, Response, Router} from "express";
import generales from "../../general/function";
import SubRamoQueries from "../../queries/subramo/subRamoQueries";

const router = Router();

router.get('/getByIdRamo', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await SubRamoQueries.getByIdRamo(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
router.get('/alias/:idtipoSubramo/:alias', async (req: Request, res: Response, next) => {
    try {
        const idtipoSubramo = req.params.idtipoSubramo;
        const alias: string = req.params.alias;
        const aliasv = await SubRamoQueries.getAlias(idtipoSubramo, alias);
        res.status(200).send(aliasv);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
router.get('/subramo/:id', async (req: Request, res: Response, next) => {
    try {
        const id = req.params.id;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await SubRamoQueries.getSubramobyId(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
router.get('/id-ramo/:id', async (req: Request, res: Response, next) => {
    try {
        const id = req.params.id;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await SubRamoQueries.getByIdRamoView(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})

export default router;
