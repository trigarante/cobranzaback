import {Request, Response, Router} from "express";
import PeriodicidadQueries from "../../queries/periodicidad/periodicidadQueries";

const router = Router();

router.get('', async (req: Request, res: Response, next) => {
    try {
        const periodicidad = await PeriodicidadQueries.getActivos();
        res.status(200).send(periodicidad);
    } catch (err) {
        next(err);
    }
})

export default router;
