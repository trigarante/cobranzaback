import {Router, Request, Response} from "express";
import UsuariosDepartamentoQueries from "../../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import AutorizacionRegistroQueries
    from "../../../queries/AutorizacionErrores/autorizacionRegistro/autorizacionRegistroQueries";
import PlazaQueries from "../../../queries/RRHH/plazaQueries";
import AdministradorPolizasQueries from "../../../queries/adminPolizas/administradorPolizas-queries";
const generales = require('../../../general/function');
const autorizacionRegistroRoutes = Router();

autorizacionRegistroRoutes.get('/tabla/:tipo/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {tipo ,idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!(tipo && idEmpleado)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        let aux2;
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1)`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 AND "verificadoProducto" > 1 AND "verificadoRegistro" > 1 AND "verificadoPago" > 1) and "idEstadoVerificacion" = 2`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2`
                break;
        }
        // if (+tipo !==  1 && permisos.idPermisoVisualizacion === 3) {
        //     aux2 += ` AND ar."idEmpleado" = ${idEmpleado}`
        // }

        switch (permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAll(aux2, fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     autorizacionRegistro = await AutorizacionRegistroQueries.getAllByHijos(aux2, fechaInicio, fechaFin, aux)
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                    let aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentos(aux2, fechaInicio, fechaFin, aux)
                // }
                break;
            case 3:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentos(aux2, fechaInicio, fechaFin, [permisos.idDepartamento])
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})

autorizacionRegistroRoutes.get('/tabla/subsecuentes/:tipo/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {tipo ,idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!(tipo && idEmpleado)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        let aux2;
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1)`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 AND "verificadoProducto" > 1 AND "verificadoRegistro" > 1 AND "verificadoPago" > 1) and "idEstadoVerificacion" = 2`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2`
                break;
        }
        // if (+tipo !==  1 && permisos.idPermisoVisualizacion === 3) {
        //     aux2 += ` AND ar."idEmpleado" = ${idEmpleado}`
        // }

        switch (permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllSubsecuentes(aux2, fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     autorizacionRegistro = await AutorizacionRegistroQueries.getAllByHijos(aux2, fechaInicio, fechaFin, aux)
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                    let aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentosSubs(aux2, fechaInicio, fechaFin, aux)
                // }
                break;
            case 3:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentosSubs(aux2, fechaInicio, fechaFin, [permisos.idDepartamento])
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})

autorizacionRegistroRoutes.get('/tabla/ENR/:tipo/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {tipo ,idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!(tipo && idEmpleado)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let autorizacionRegistro;
        let aux2;
        switch (+tipo) {
            case 1:
                aux2 = `("verificadoCliente" = 1 or "verificadoProducto" = 1 or "verificadoRegistro" = 1 or "verificadoPago" = 1)`
                break;
            case 2:
                aux2 = `("verificadoCliente" > 1 AND "verificadoProducto" > 1 AND "verificadoRegistro" > 1 AND "verificadoPago" > 1) and "idEstadoVerificacion" = 2`
                break;
            case 3:
                aux2 = `("verificadoCliente" = 3 or "verificadoProducto" = 3 or "verificadoRegistro" = 3 or "verificadoPago" = 3) AND "idEstadoVerificacion" = 2`
                break;
        }
        // if (+tipo !==  1 && permisos.idPermisoVisualizacion === 3) {
        //     aux2 += ` AND ar."idEmpleado" = ${idEmpleado}`
        // }

        switch (permisos.idPermisoVisualizacion) {
            case 1:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllENR(aux2, fechaInicio, fechaFin)
                break;
            case 2:
                // if (permisos.idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     let hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     autorizacionRegistro = await AutorizacionRegistroQueries.getAllByHijos(aux2, fechaInicio, fechaFin, aux)
                // } else {
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentosENR(aux2, fechaInicio, fechaFin, aux)
                // }
                break;
            case 3:
                autorizacionRegistro = await AutorizacionRegistroQueries.getAllByDepartamentosENR(aux2, fechaInicio, fechaFin, [permisos.idDepartamento])
                break;
        }
        res.status(200).send( autorizacionRegistro );
    } catch (e) {
        res.status(500).send(e)
    }
})
autorizacionRegistroRoutes.get('/get-by-id/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await AutorizacionRegistroQueries.getById(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
})
autorizacionRegistroRoutes.get('/get-by-idRegistro/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await AutorizacionRegistroQueries.getByIdRegistro(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
})
autorizacionRegistroRoutes.put('/datos', async(req:Request, res: Response, next) => {
    try {
        const { idAutorizacionRegistro, idDocumentoVerificado, estado } = req.body;
        if (!(idAutorizacionRegistro && idDocumentoVerificado && estado)) return generales.manejoErrores('No se enviaron los parametros requeridos', res);
        let autorizacion = await AutorizacionRegistroQueries.getByIdEstado(idAutorizacionRegistro)
        switch (idDocumentoVerificado) {
            case 1:
                autorizacion.verificadoCliente = estado
                break;
            case 2:
                autorizacion.verificadoRegistro = estado
                break;
            case 3:
                autorizacion.verificadoProducto = estado
                break;
            case 4:
                autorizacion.verificadoPago = estado
                break
        }
        if (estado === 2 || estado === 5) {
            autorizacion.numero++
        }

        if (autorizacion.idEstadoVerificacion === 1) {
            autorizacion.idEstadoVerificacion = 2
        }
        await AutorizacionRegistroQueries.updateEstado(idAutorizacionRegistro, autorizacion);
        res.status(200).send();
    } catch (e) {
        res.status(500).send(e)
    }
});

autorizacionRegistroRoutes.put('/:id', async(req:Request, res: Response, next) => {
    try {
        const id = +req.params.id;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const autorizacionRegistro = AutorizacionRegistroQueries.update(req.body, id);
        res.status(200).send(autorizacionRegistro);
    } catch (e) {
        res.status(500).send(e)
    }
});


export default autorizacionRegistroRoutes;
