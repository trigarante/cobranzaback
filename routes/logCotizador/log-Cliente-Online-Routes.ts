import {Router, Request, Response} from "express";
import ClienteQueries from "../../queries/cliente/cliente-queries";
import clienteRoutes from "../cliente/cliente-routes";
import LogCotizadorQueries from "../../queries/logCotizador/log-Cotizador-Queries";
const generales = require('../../general/function');

const logCotizadorOnlineRoutes = Router();

logCotizadorOnlineRoutes.post('/logQualitasEmision', async(req:Request, res: Response, next) => {
    try {
        const id = await LogCotizadorQueries.postQualitasEmision(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});
logCotizadorOnlineRoutes.post('/logQualitasPago', async(req:Request, res: Response, next) => {
    try {
        const id = await LogCotizadorQueries.postQualitasPago(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});
logCotizadorOnlineRoutes.post('/logGnpEmision', async(req:Request, res: Response, next) => {
    try {
        const id = await LogCotizadorQueries.postGnpEmision(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});
logCotizadorOnlineRoutes.post('/logGnpPago', async(req:Request, res: Response, next) => {
    try {
        const id = await LogCotizadorQueries.postGnpPago(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});


export default logCotizadorOnlineRoutes;