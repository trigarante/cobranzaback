import {Request, Response, Router} from "express";
import SociosQueries from "../../queries/socios/sociosQueries";
const generales = require('../../general/function');

const router = Router();
// lo usa cotizador  online
router.get('/byIdPaisStep', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const socios = await SociosQueries.getByIdPaisStep(id);
        res.status(200).send(socios);
    } catch (err) {
        next(err);
    }
})

export default router;
