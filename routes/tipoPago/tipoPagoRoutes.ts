import {Request, Response, Router} from "express";
import TipoPagoQueries from "../../queries/tipoPago/tipoPagoQueries";

const router = Router();

router.get('', async (req: Request, res: Response, next) => {
    try {
        const tipos = await TipoPagoQueries.getActivos();
        res.status(200).send(tipos);
    } catch (err) {
        next(err);
    }
})

export default router;
