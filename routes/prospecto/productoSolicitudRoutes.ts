import {Request, Response, Router} from "express";
import generales from "../../general/function";
import ProductoSolicitudQueries from "../../queries/prospecto/ProductoSolicitudQueries";
import CotizacionesQueries from "../../queries/cotizador/cotizacionesQueries";
import CotizacionesAliQueries from "../../queries/cotizador/cotizacionesAliQueries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";

const router = Router();

router.get('/getById', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoSolicitudQueries.getById(id);
        res.status(200).send(producto);
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/All', async(req:Request, res: Response, next) => {
    try {
        const {idempleado, iddepartamento} = req.headers;
        if (!(idempleado && iddepartamento)) return generales.manejoErrores('No se envio ningun id', res);
        const idProducto = await ProductoSolicitudQueries.post(req.body);
        const cotizaciones = {
            idProducto,
            idTipoContacto: 3,
        };
        const idCotizacion = await CotizacionesQueries.post(cotizaciones);
        const cotizacionesAli = {
            idCotizacion,
            idSubRamo: 50,
            peticion: req.body.datos,
        };
        const idCotizacionAli = await CotizacionesAliQueries.post(cotizacionesAli);
        const solicitud = {
            idCotizacionAli,
            idEmpleado: +idempleado,
            idDepartamento: +iddepartamento,
            idEstadoSolicitud: 2,
        };
        const idSolicitud = await SolicitudesInternoQueries.post(solicitud);
        res.status(200).send({idSolicitud});
    } catch (e) {
        res.status(500).send(e)
    }
})
router.post('', async(req:Request, res: Response, next) => {
    try {
        const id = await ProductoSolicitudQueries.post(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});
// lo usa cotizador  online
router.post('/solicitudOnline', async(req:Request, res: Response, next) => {
    try {
        const {cotizacionRequestOnline, cotizacionResponse, idProspecto, idTipoContacto,idEmpleado, iddepartamento } = req.headers;
        if (!(iddepartamento)) return generales.manejoErrores('No se envio ningun id', res);
        const productoSolicitud = {
            idProspecto: +req.body.idProspecto,
            datos: req.body.cotizacionRequestOnline,
            idTipoSubRamo: 1,
        };
        const idProducto = await ProductoSolicitudQueries.post(productoSolicitud);
        const cotizaciones = {
            idProducto,
            idTipoContacto: 3,
        };
        const idCotizacion = await CotizacionesQueries.post(cotizaciones);
        const cotizacionesAli = {
            idCotizacion,
            idSubRamo: +req.body.idSubRamo,
            peticion: req.body.cotizacionRequestOnline,
            respuesta: req.body.cotizacionResponse,
        };
        const idCotizacionAli = await CotizacionesAliQueries.post(cotizacionesAli);
        const solicitud = {
            idCotizacionAli,
            idEstadoSolicitud: 1,
            idEmpleado: +req.body.idEmpleado,
            idDepartamento: +iddepartamento,
        };
        const idSolicitud = await SolicitudesInternoQueries.postOnline(solicitud);
        res.status(200).send({idSolicitud});
    } catch (e) {
        res.sendStatus(500);
    }
});
// online
// lo usa cotizador  online
router.get('/getByIdProducto', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoSolicitudQueries.getByIdProducto(id);
        res.status(200).send(producto);
    } catch (e) {
        res.status(500).send(e)
    }
});
router.put('/producto/:id', async(req:Request, res: Response, next) => {
    try {
        const id = req.params.id;
        if (!id) return generales.manejoErrores('No se envió el id', res);
        await ProductoSolicitudQueries.update(req.body, id);
        res.status(200).send({id});
    } catch (e) {
        res.status(500).send(e)
    }
})
export default router;
