import {Request, Response, Router} from "express";
import ProspectoQueries from "../../queries/prospecto/prospectoQueries";
import ProductoSolicitudQueries from "../../queries/prospecto/ProductoSolicitudQueries";
import axios from "axios";
const generales = require('../../general/function');

const router = Router();
// lo usa cotizador interno online
router.get('/getByCorreo', async(req:Request, res: Response, next) => {
    try {
        const {correo} = req.headers;
        if (!correo) return generales.manejoErrores('No se envio el correo', res);
        const prospecto = await ProspectoQueries.getByCorreo(correo);
        let productos;
        if (prospecto)
            productos = await ProductoSolicitudQueries.getByIdProspecto(prospecto.id)
        res.status(200).send({...prospecto, productos});
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('', async(req:Request, res: Response, next) => {
    try {
        axios
            .post('https://app.core-ahorraseguros.com/v1/users', {
                correo: req.body.correo,
                numeroMovil: req.body.numero,
                password: '',
                nombre: req.body.nombre,
            })
            .then(async data => {
                const id = await ProspectoQueries.post({...req.body, idUsuarioApp: data.data.id});
                res.status(200).send({id});
            })
            .catch(error => {
                console.error(error);
            });

    } catch (e) {
        res.status(500).send(e)
    }
})

router.put('', async(req:Request, res: Response, next) => {
    try {
        axios
            .post('https://app.core-ahorraseguros.com/v1/users', {
                correo: req.body.correo,
                numeroMovil: req.body.numero,
                password: '',
                nombre: req.body.nombre,
            })
            .then(async data => {
                const {id} = req.headers;
                if (!id) return generales.manejoErrores('No se envió el id', res);
                await ProspectoQueries.update({...req.body, idUsuarioApp: data.data.id}, id);
                res.status(200).send({id});
            })
            .catch(error => {
                console.error(error);
            });

    } catch (e) {
        res.status(500).send(e)
    }
})


// lo usa cotizador interno online
router.get('/getById/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id', res);
        const prospecto = await ProspectoQueries.getById(id);
        res.status(200).send(prospecto);
    } catch (e) {
        res.sendStatus(500);
    }
})

export default router;
