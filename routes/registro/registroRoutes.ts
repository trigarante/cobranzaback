import {Router, Request, Response} from "express";
import ProductoClienteQueries from "../../queries/solicitudes/producto-cliente/producto-cliente-queries";
import DriveApiService from "../../services/drive/driveApiService";
import RegistroQueries from "../../queries/registro/registroQueries";
import ReciboQueries from "../../queries/recibo/reciboQueries";
const generales = require('../../general/function');

const registroRoutes = Router();

registroRoutes.get('/viewer/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        console.log(id)
        const registroviewer = await RegistroQueries.findAllByIdRegistroViewer(id);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.get('/detalles-poliza/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registroviewer = await RegistroQueries.detallesPoliza(id);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.get('/existePoliza/poliza', async(req:Request, res: Response, next) => {
    try {
        const {poliza, idsocio, fechainicio} = req.headers;
        if (!(poliza && idsocio && fechainicio) ) return generales.manejoErrores('No se envio la información necesaria', res);
        const registroviewer = await RegistroQueries.getPolizaExiste(poliza, idsocio, fechainicio);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.post('/All', async(req:Request, res: Response, next) => {
    try {
        const file = req['files'].file;
        let registro = JSON.parse(req.body.registro);
        const {id} = req.body
        const recibos = registro.recibos;
        const producto = await ProductoClienteQueries.getByIdSolicitud(id);
        registro.idProducto = producto.id;
        const idRegistro = await RegistroQueries.post(registro);
        for (const recibo of recibos) {
            recibo.idRegistro = idRegistro;
            await ReciboQueries.post(recibo);
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, idRegistro, idFolder);
        const archivo = await DriveApiService.subirArchivo(idFolderPago, file, idRegistro + '-poliza')
        await RegistroQueries.update({archivo: idFolderPago}, idRegistro)
        res.status(200).send({idRegistro});
    } catch (e) {
        res.status(500).send(e)
    }
})
// igual que el anterior solo se agrega campo online a registro Online
registroRoutes.post('/AllOnline', async(req:Request, res: Response, next) => {
    try {
        const file = req['files'].file;
        let registro = JSON.parse(req.body.registro);
        const {id} = req.body
        console.log(id);
        const recibos = registro.recibos;
        const producto = await ProductoClienteQueries.getByIdSolicitud(id);
        registro.idProducto = producto.id;
        const idRegistro = await RegistroQueries.postOnline(registro);
        for (const recibo of recibos) {
            recibo.idRegistro = idRegistro;
            await ReciboQueries.post(recibo);
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, idRegistro, idFolder);
        const archivo = await DriveApiService.subirArchivo(idFolderPago, file, idRegistro + '-poliza')
        await RegistroQueries.update({archivo: idFolderPago}, idRegistro)
        res.status(200).send({idRegistro});
    } catch (e) {
        res.status(500).send(e)
    }
})
// lo usa cotizador  online
registroRoutes.get('/noSerieExiste', async (req: Request, res: Response, next) => {
    const {noserie} = req.headers;
    if(!noserie) return generales.manejoErrores('No se envio la informacion', res)
    const noSerie = await ProductoClienteQueries.findNoSerie(noserie);
    res.status(200).send(noSerie);
});

registroRoutes.get('/forInpecciones', async (req: Request, res: Response, next) => {
    const {id} = req.headers;
    if(!id) return generales.manejoErrores('No se envio la informacion', res)
    const data = await ProductoClienteQueries.getForInspecciones(id);
    res.status(200).send(data);
});
// Para actualizar el registro
registroRoutes.get('/getForRegistroById/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.getForRegistroById(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})

// NO SE USA
// Traer todos los id de los steps
// registroRoutes.get('/getAllIds', async(req:Request, res: Response, next) => {
//     try {
//         const {id} = req.headers;
//         if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
//         const registro = await RegistroQueries.getAllIdsSteps(id);
//         res.status(200).send( registro );
//     } catch (e) {
//         res.status(500).send(e)
//     }
// })

registroRoutes.get('/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.findAllByIdRegistro(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})
registroRoutes.get('/Online/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.findAllByIdRegistroOnline(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.put('/', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.body;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        await RegistroQueries.update(req.body.registro, id);
        res.status(200).send();
    } catch (e) {
        next(e)
    }
})
registroRoutes.put('/UpdateCancelada', async(req:Request, res: Response, next) => {
    try {
        const {idRegistro, estado, socio} = req.body;
        if (!(idRegistro && estado)) return generales.manejoErrores('No se envio un parametro', res);
        await RegistroQueries.update({idEstadoPoliza: estado, idSocio: socio}, idRegistro);
        res.status(200).send();
    } catch (e) {
        next(e)
    }
})
// se usa en cotizacion online, se modifica el campo online
registroRoutes.put('/UpdateOnline/', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.body;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        await RegistroQueries.updateOnline(req.body.registro, id);
        res.status(200).send();
    } catch (e) {
        next(e)
    }
})
export default registroRoutes;
