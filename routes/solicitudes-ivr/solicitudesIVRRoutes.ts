import {Request, Response, Router} from "express";
import SolicitudesIVRQueries from "../../queries/solicitudesIVR/solicitudesIVRQueries";
const router = Router();

router.get('', async (req: Request, res: Response, next) => {
    try {
        const data = await SolicitudesIVRQueries.getAll();
        res.send(data);
    } catch (e) {
        next(e)
    }
});
export default router;
