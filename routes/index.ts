import {Request, Response, Router} from 'express';
import fs from "fs";
import DriveApiService from "../services/drive/driveApiService";
import stepUnoRoutes from "./solicitudes/step-uno";
import productoClienteRoutes from "./solicitudes/producto-cliente-routes";
import registroPolizaRoutes from "./solicitudes/registro-poliza-routes";
import adminPolizasRoutes from "./adminPolizas/adminPolizas-routes";
import solicitudesCarruselRoutes from "./solicitudesDesactivacion/solicitudes-carrusel-routes";
import jwtAuthentica from "../middlewares/jwtAuthentica";
import solicitudesInternoRoutes from "./solicitudesInterno/solicitudes-interno-routes";
import clienteRoutes from "./cliente/cliente-routes";
import ProspectoRoutes from "./prospecto/prospectoRoutes";
import stepCotizadorRoutes from "./step-cotizador/step-cotizador-routes";
const generales = require('../general/function');
import ProductoSolicitudRoutes from "./prospecto/productoSolicitudRoutes";
import TipoSubRamoRoutes from "./cotizador/tipoSubRamoRoutes";
import MarcaInternoRoutes from "./cotizador/marcaInternoRoutes";
import ModeloInternoRoutes from "./cotizador/modeloInternoRoutes";
import DetalleInternoRoutes from "./cotizador/detalleInternoRoutes";
import tipoEndosoRoutes from "./tipoEndoso/tipoEndosoRoutes";
import detallesPolizaRoutes from "./detalles-poliza/detallesPolizaRoutes";
import SubsecuentesRoutes from "./tipoEndoso/tipoEndosoRoutes";
import motivoEndosoRoutes from "./motivoEndoso/motivoEndosoRoutes";
import reciboRoutes from "./recibo/reciboRoutes";
import formaPagoRoutes from "./formaPago/formaPagoRoutes";
import bancosRoutes from "./banco/bancosRoutes";
import registroRoutes from "./registro/registroRoutes";
import TipoPagoRoutes from "./tipoPago/tipoPagoRoutes";
import PeriodicidadRoutes from "./periodicidad/periodicidadRoutes";
import SociosRoutes from "./socios/sociosRoutes";
import RamoRoutes from "./ramo/ramoRoutes";
import SubRamoRoutes from "./subRamo/subRamoRoutes";
import ProductosSocioRoutes from "./productosSocio/productosSocioRoutes";
import estadoInspeccionRoutes from "./estadoInspeccion/estadoInspeccionRoutes";
import PagoRoutes from "./pago/pagoRoutes";
import autorizacionRegistroRoutes from "./autorizacionErrores/autorizacionRegistro/autorizacionRegistroRoutes";
import tipoDocumentosRoutes from "./tipoDocumentos/tipoDocumentosRoutes";
import campoCorregirRoutes from "./campoCorregir/campoCorregirRoutes";
import PaisesRoutes from "./paises/paisesRoutes";
import correccionErroresAutorizacionRoutes from "./correccionErrores/correccionErroresAutorizacionRoutes";
import correccionErroresVerificacionRoutes from "./correccionErrores/correccionErroresVerificacionRoutes";
import erroresAnexosRoutes from "./erroresAnexo/erroresAnexosRoutes";
import InspeccionesRoutes from "./inspecciones/inspeccionesRoutes";
import VerificacionRegistroRoutes from "./verificacionRegistro/VerificacionRegistroRoutes";
import erroresAutorizacionRoutes from "./autorizacionErrores/erroresAutorizacion/erroresAutorizacionRoutes";
import solicitudEndosoRoutes from "./solicitudEndoso/solicitudEndosoRoutes";
import erroresAnexosVRoutes from "./erroresAnexoVerificacion/erroresAnexosVRoutes";
import erroresVerificacionRoutes from "./erroresVerificacion/erroresVerificacionRoutes";
import empleadosRoutes from "./empleados/empleadosRoutes";
import cotizacionesAliRoutes from "./cotizacionesAli/cotizacionesAliRoutes";
import DesarrolloOrganizacionalRoutes from "./desarrolloOrganizacional/DesarrolloOrganizacionalRoutes";
import SepomexRoutes from "./sepomex/sepomexRoutes";
import logCotizadorOnlineRoutes from "./logCotizador/log-Cliente-Online-Routes";
import DashboardRoutes from "./dashboard/DashboardRoutes";
import SolicitudesCarruselRoutes from "./activacionesEmpleado/solicitudesCarruselRoutes";
import AdministracionPersonalRouter from "./administracion-personal/administracionPersonalRouter";
import SubidaArchivosRoutes from "./SubidaArchivos/SubidaArchivosRoutes";
import edoCuentaRoutes from "./edoCuenta/edoCuentaRoutes";
import prepagoRoutes from "./prepago/prepagoRoutes";
import solicitudesIVRRoutes from "./solicitudes-ivr/solicitudesIVRRoutes";

const jwt = require('jsonwebtoken');

const routes = Router();

// Ruta de acceso denegado
routes.get('', jwtAuthentica, async (request: Request, response: Response) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('src/index.html', null, (err, data) => {
        if (err) {
            response.writeHead(404);
            response.write('No está el archivo y eso me da ansiedad D:');
        } else {
            response.write(data);
        }
        response.end();
    });
});

// Ruta de autenticacion JWT
routes.post('/autenticar', async (request: Request, response: Response, next) => {
    try {
        const token = jwt.sign({check: true, idEmpleado: 9159}, process.env.llave, {
            expiresIn: 1440,
        })
        response.status(200).send({mensaje: "Autenticacion correcta", token});
    } catch (e) {
        next(e);
    }
});

// Ruta de prueba
routes.get('/prueba', jwtAuthentica,async (request: Request, response: Response, next) => {
    try {
        console.log(request.headers.idempleado)
        const prueba = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjMzNDc3NDIwLCJleHAiOjE2MzM0Nzg4NjB9.1z8_itpwZH4I448DCLClw104Vw8NWn4CsKiGUCwqbBY';
        console.log(jwt.verify(prueba, process.env.llave))
        // console.log(jwt.verify(prueba, process.env.PORT))
        // const tok = request.headers.token;
        // console.log(request.headers)
        const token = jwt.sign({nombre: 'Juan', apellido: 'Perez'}, process.env.PORT, {
            expiresIn: 1440,
        })
        response.status(200).send(token);
    } catch (e) {
        next(e);
    }
});

// rutas
routes.use('/', stepUnoRoutes
    // #swagger.tags = ['Step uno']
);
routes.use('/producto-cliente', productoClienteRoutes
    // #swagger.tags = ['Producto cliente']
);
routes.use('/', registroPolizaRoutes
    // #swagger.tags = ['Registro']
);
routes.use('/administracion-polizas', adminPolizasRoutes
    // #swagger.tags = ['Administrador de pólizas']
);
routes.use('/', solicitudesCarruselRoutes
    // #swagger.tags = ['Solicitudes carrusel']
);
routes.use('/solicitudes-vn', solicitudesInternoRoutes
    // #swagger.tags = ['Solicitudes interno']
);
routes.use('/cliente', clienteRoutes
    // #swagger.tags = ['Cliente']
);
routes.use('/tipo-endoso', tipoEndosoRoutes
    // #swagger.tags = ['Tipo endoso']
);
routes.use('/detalles-poliza', detallesPolizaRoutes
    // #swagger.tags = ['Detalles póliza']
);
routes.use('/subsecuentes-view', SubsecuentesRoutes
    // #swagger.tags = ['Subsecuentes']
);
routes.use('/motivo-endoso', motivoEndosoRoutes
    // #swagger.tags = ['Motivo endoso']
);
routes.use('/recibo', reciboRoutes
    // #swagger.tags = ['Recibo']
);
routes.use('/registro-vn', registroRoutes
    // #swagger.tags = ['Registro']
);
routes.use('/forma-pago', formaPagoRoutes
    // #swagger.tags = ['Forma de pago']
);
routes.use('/prospecto', ProspectoRoutes
    // #swagger.tags = ['Prospecto']
);
routes.use('/producto-solicitud', ProductoSolicitudRoutes
    // #swagger.tags = ['Producto solicitud']
);
routes.use('/tipoSubRamo', TipoSubRamoRoutes
    // #swagger.tags = ['Tipo subramo']
);
routes.use('/marca-interno', MarcaInternoRoutes
    // #swagger.tags = ['Marca interno']
);
routes.use('/modelo-interno', ModeloInternoRoutes
    // #swagger.tags = ['Modelo interno']
);
routes.use('/detalle-interno', DetalleInternoRoutes
    // #swagger.tags = ['Detalle interno']
);
routes.use('/step-cotizador', stepCotizadorRoutes
    // #swagger.tags = ['Step cotizador']
);
routes.use('/bancos', bancosRoutes
    // #swagger.tags = ['Bancos']
);
routes.use('/tipo-pago', TipoPagoRoutes
    // #swagger.tags = ['Tipo pago']
);
routes.use('/periodicidad', PeriodicidadRoutes
    // #swagger.tags = ['Peridicidad']
);
routes.use('/socios', SociosRoutes
    // #swagger.tags = ['Socios']
);
routes.use('/ramo', RamoRoutes
    // #swagger.tags = ['Ramo']
);
routes.use('/subRamo', SubRamoRoutes
    // #swagger.tags = ['Subramo']
);
routes.use('/producto-socio', ProductosSocioRoutes
    // #swagger.tags = ['Producto socio']
);
routes.use('/estado-inspeccion', estadoInspeccionRoutes
    // #swagger.tags = ['Estado inspección']
);
routes.use('/pago', PagoRoutes
    // #swagger.tags = ['Pago']
);
routes.use('/autorizacion-registro', autorizacionRegistroRoutes
    // #swagger.tags = ['Autorización registro']
);
routes.use('/tipo-documentos', tipoDocumentosRoutes
    // #swagger.tags = ['Tipo documentos']
);
routes.use('/campo-corregir', campoCorregirRoutes
    // #swagger.tags = ['Campo corregir']
);
routes.use('/paises', PaisesRoutes
    // #swagger.tags = ['Paises']
);
routes.use('/correcciones', correccionErroresAutorizacionRoutes
    // #swagger.tags = ['Corrección errores autorización']
);
routes.use('/correccion-errores-verificacion', correccionErroresVerificacionRoutes
    // #swagger.tags = ['Correccióbn errores verificación']
);
routes.use('/errores-anexos', erroresAnexosRoutes
    // #swagger.tags = ['Errores anexos']
);
routes.use('/subida-archivos', SubidaArchivosRoutes
    // #swagger.tags = ['Subida de archivos']
);
routes.use('/inspecciones', InspeccionesRoutes
    // #swagger.tags = ['Inspecciones']
);
routes.use('/verificacion-registro', VerificacionRegistroRoutes
    // #swagger.tags = ['Verificación registro']
);
routes.use('/errores-autorizacion', erroresAutorizacionRoutes
    // #swagger.tags = ['Errores autorización']
);
routes.use('/solicitud-endoso', solicitudEndosoRoutes
    // #swagger.tags = ['Solicitud endoso']
);
routes.use('/errores-anexos-verificacion', erroresAnexosVRoutes
    // #swagger.tags = ['Errores anexosV']
);
routes.use('/errores-verificacion', erroresVerificacionRoutes
    // #swagger.tags = ['Errores verificación']
);
routes.use('/empleados', empleadosRoutes
    // #swagger.tags = ['Empleados']
);
routes.use('/cotizacionesAli', cotizacionesAliRoutes
    // #swagger.tags = ['Cotizaciones Ali']
);
routes.use("/desarrollo-organizacional", DesarrolloOrganizacionalRoutes
    // #swagger.tags = ['Desarrollo organizacional']
);
routes.use("/logCotizador", logCotizadorOnlineRoutes
    // #swagger.tags = ['Log cotizador online']
);
routes.use("/sepomex", SepomexRoutes
    // #swagger.tags = ['Sepomex']
);
routes.use("/solicitudes-carrusel", SolicitudesCarruselRoutes
    // #swagger.tags = ['Solicitudes carrusel']
);
// rutas vacaciones
routes.use("/administracion-personal", AdministracionPersonalRouter
    // #swagger.tags = ['Administración de personal']
);
// rutas edo-cuenta
routes.use("/estado-cuenta", edoCuentaRoutes);

// rutas dashboard
routes.use("/dashboard-new", DashboardRoutes
    // #swagger.tags = ['Dashboard']
);
routes.use("/prepago", prepagoRoutes
    // #swagger.tags = ['Prepago']
);
routes.use("/solicitudes-ivr", solicitudesIVRRoutes
    // #swagger.tags = ['Prepago']
);

// Subida de archivos a drive producto cliente
routes.post('/subir-archivo', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        /* Se deben enviar 2 id, el de cliente y producto cliente
        la carpreta del cliente se creara con el id cliente
        y dentro se guardara un idProductoCliente-cliente para tener las carpetas de cliente pero con
        varios productos cliente
         */
        const {idcliente, idproducto} = req.headers;
        if (!(idcliente  && idproducto)) return generales.manejoErrores('No se envio el id', res);
        const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
        const id = await DriveApiService.subirArchivo(idFolder, files, idproducto + '-cliente')
        return res.status(200).send({id});
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

// Subida de archivos a drive registro, pago, inspeccion etc
routes.post('/subir-archivo-otro', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idpago, idcliente, idregistro} = req.headers;
        if (!(idpago  && idcliente && idregistro)) return generales.manejoErrores('No se envio el id', res);
        const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, idregistro, idFolder);
        const id = await DriveApiService.subirArchivo(idFolderPago, files, idpago + '-pago')
        return res.status(200).send({id});
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

// Descargar archivo
routes.get('/archivo', async (req: Request, res: Response, next) => {
    try {
        const {archivoid} = req.headers;
        if (!archivoid) return generales.manejoErrores('No se envió el id de la carpeta', res);
        const file = await DriveApiService.obtenerArchivo(archivoid);
        return res.status(200).send([file]);
    } catch (err) {
        next(err)
    }
});

export default routes;
