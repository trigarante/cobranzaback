import {Router, Request, Response} from "express";
import BancosQueries from "../../queries/bancos/bancosQueries";
import CarrierQueries from "../../queries/carrierTerjetas/carrierQueries";

const bancosRoutes = Router();

bancosRoutes.get('/', async(req:Request, res: Response, next) => {
    try {
        const banco = await BancosQueries.findAll();
        res.status(200).send( banco );
    } catch (e) {
        res.status(500).send(e)
    }
})
bancosRoutes.get('/carrier', async(req:Request, res: Response, next) => {
    try {
        const carrier = await CarrierQueries.findAll();
        res.status(200).send( carrier );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default bancosRoutes;
