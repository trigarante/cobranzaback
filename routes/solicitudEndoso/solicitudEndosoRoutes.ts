import {Request, Response, Router} from "express";
import SolicitudEndosoQueries from "../../queries/solicitudEndoso/solicitudEndosoQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');
const solicitudEndosoRoutes = Router();

solicitudEndosoRoutes.get('/get-endosoPendiente/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado,fechaInicio, fechaFin} = req.params;
        if (!idEmpleado) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await SolicitudEndosoQueries.getPermisosVisualizacion(+idEmpleado);
        let SolicitudEndoso;
        switch (permisos.idPermisoVisualizacion) {
            case 1:
                SolicitudEndoso = await SolicitudEndosoQueries.getAll(fechaInicio, fechaFin)
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                let aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (departamentos.length === 0){
                    aux.push(permisos.idDepartamento)
                }
                SolicitudEndoso = await SolicitudEndosoQueries.getAllByDepartamentos(fechaInicio, fechaFin, aux)
                break;
            case 3:
                SolicitudEndoso = await SolicitudEndosoQueries.getAllByDepartamentoAndIdEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin)
                break;
        }
        res.status(200).send( SolicitudEndoso );
    } catch (e) {
        res.status(500).send(e)
    }
})

solicitudEndosoRoutes.post('/', async (req: Request, res: Response, next) => {
    try {
        await SolicitudEndosoQueries.post(req.body)
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});


export default solicitudEndosoRoutes;
