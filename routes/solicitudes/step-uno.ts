import {Request, Response, Router} from "express";
import CrearClienteQueries from "../../queries/solicitudes/crear-cliente/crear-cliente-queries";
import ClienteModel from "../../models/venta-nueva/solicitudes/steps/crear-cliente/cliente-model";
import ClienteModelOnline from "../../models/venta-nueva/solicitudes/steps/crear-cliente/cliente-model-online";
import routes from "../index";
import DriveApiService from "../../services/drive/driveApiService";
import ClienteQueries from "../../queries/cliente/cliente-queries";
const generales = require('../../general/function');

const stepUnoRoutes = Router();

// NO SE USA
// stepUnoRoutes.post('/cliente', async (req: Request, res: Response, next) => {
//     try {
//         const nuevoCliente = await ClienteModel.create({...req.body}, {
//             fields: ['idPais', 'razonSocial', 'nombre', 'paterno', 'materno', 'cp', 'calle', 'numInt', 'numExt', 'idColonia',
//                 'genero', 'telefonoFijo', 'telefonoMovil', 'correo', 'curp', 'rfc', 'archivo', 'archivoSubido']
//         });
//         if (nuevoCliente) {
//             console.log(nuevoCliente);
//             res.status(200).send( nuevoCliente );
//         }
//     } catch (e) {
//         console.log(e);
//         res.status(500).send( e );
//     }
// });

// stepUnoRoutes.put('/cliente', async (req: Request, res: Response, next) => {
//     const idCliente = Number(req.headers.idcliente);
//     const idEmpleado = Number(req.headers.idempleado);
//     if (!idCliente || !idEmpleado)
//         return  generales.manejoErrores('No se enviaron los parámetros requeridos', res);
//     const clienteActuaizado = CrearClienteQueries.updateCliente(idCliente, {...req.body});
//     res.status(200).send( clienteActuaizado );
// });


stepUnoRoutes.get('/paises', async (req: Request, res: Response, next) => {
    try {
        const paisesData = await CrearClienteQueries.getPaises();
        res.status(200).send( paisesData );
    } catch (e) {
        res.status(500).send( e );
    }
});
// lo usa cotizador  online
stepUnoRoutes.get('/sepomex', async (req: Request, res: Response, next) => {
    try {
        const cp = req.headers.cp;
        if (!cp)
            return generales.manejoErrores('No se envió cp', res);
        const coloniaData = await CrearClienteQueries.getColoniaByCp(cp);
        res.status(200).send( coloniaData );
    } catch (e) {
        res.status(500).send( e );
    }
});

stepUnoRoutes.get('/cliente', async (req: Request, res: Response, next) => {
    try {
        const idCliente = +req.headers.idcliente;
        if (!idCliente)
            return generales.manejoErrores('No se envió idCliente', res);
        const clienteData = await CrearClienteQueries.getCLienteById(idCliente);
        res.status(200).send( clienteData );
    } catch (e) {
        res.status(500).send( e );
    }
});

// stepUnoRoutes.post('/cliente', async (req: Request, res: Response, next) => {
//     try {
//         const nuevoCliente = await ClienteModel.create({...req.body}, {
//             fields: ['idPais', 'razonSocial', 'nombre', 'paterno', 'materno', 'cp', 'calle', 'numInt', 'numExt', 'idColonia',
//                 'genero', 'telefonoFijo', 'telefonoMovil', 'correo', 'curp', 'rfc', 'archivo', 'archivoSubido']
//         });
//         if (nuevoCliente) {
//             res.status(200).send( nuevoCliente );
//         }
//     } catch (e) {
//         console.log(e);
//         res.status(500).send( e );
//     }
// });
// lo usa cotizador  online
stepUnoRoutes.post('/clienteOnline', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body)
        const nuevoCliente = await ClienteModelOnline.create({...req.body}, {
            fields: ['idPais', 'razonSocial', 'nombre', 'paterno', 'materno', 'cp', 'calle', 'numInt', 'numExt', 'idColonia',
                'genero', 'telefonoFijo', 'telefonoMovil', 'correo', 'curp', 'rfc', 'archivo', 'archivoSubido', 'fechaNacimiento']
        });
        if (nuevoCliente) {
            console.log(nuevoCliente);
            res.status(200).send( nuevoCliente );
        }
    } catch (e) {
        console.log(e);
        res.status(500).send( e );
    }
});
// Subida de archivos a drive  cliente
stepUnoRoutes.post('/clienteOnline/subir-archivo', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // cliente archivo
        const files = req['files'].file;

        // const {idcliente} = req.headers;
        // const {idcliente} = req.body.idcliente;
        console.log(req.body.idcliente)
        let idcliente = req.body.idcliente
        if (!(idcliente  )) return generales.manejoErrores('No se envio el id', res);
        const idFolder = await DriveApiService.findOrCreateFolder(1, idcliente);
        const archivo = await DriveApiService.subirArchivo(idFolder, files,  idcliente + '-cliente')
        await ClienteQueries.update({archivo}, idcliente)
        return res.status(200).send({archivo});
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

stepUnoRoutes.put('/cliente', async (req: Request, res: Response, next) => {
    const idCliente = Number(req.headers.idcliente);
    const idEmpleado = Number(req.headers.idempleado);
    if (!idCliente || !idEmpleado)
        return  generales.manejoErrores('No se enviaron los parámetros requeridos', res);
    const clienteActuaizado = CrearClienteQueries.updateCliente(idCliente, {...req.body});
    res.status(200).send( clienteActuaizado );
});



export default stepUnoRoutes;
