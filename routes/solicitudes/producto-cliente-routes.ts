import {Request, Response, Router} from "express";
import ProductoClienteQueries from "../../queries/solicitudes/producto-cliente/producto-cliente-queries";
import ProductoClienteModel from "../../models/venta-nueva/solicitudes/steps/producto-cliente/productoClienteModel";
import DriveApiService from "../../services/drive/driveApiService";
import ClienteQueries from "../../queries/cliente/cliente-queries";
const generales = require('../../general/function');


const productoClienteRoutes = Router();

// NO SE USA
// productoClienteRoutes.get('/noSerieExiste', async (req: Request, res: Response, next) => {
//     const {noserie} = req.headers;
//     if(!noserie) return generales.manejoErrores('No se envio la informacion', res)
//     const noSerie = await ProductoClienteQueries.findNoSerie(noserie);
//     res.status(200).send(noSerie);
// });

productoClienteRoutes.post('/All', async (req: Request, res: Response, next) => {
    try {
        let file;
        if (req['files']) {
            file = req['files'].file;
        }
        let producto = JSON.parse(req.body.producto);
        if (file) {
            const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
            const archivo = await DriveApiService.subirArchivo(idFolder, file, producto.idCliente + '-cliente')
            producto.archivo = idFolder;
            await ClienteQueries.update({archivoSubido: 1}, producto.idCliente);
        }
        const id = await ProductoClienteQueries.post(producto);
        const contrasenaApp = Math.random().toString(36).slice(-8);
        await ClienteQueries.update({contrasenaApp}, producto.idCliente);
        res.status(200).send({id})
    } catch (e) {
        res.status(500).send( e );
    }
});

productoClienteRoutes.put('/All', async (req: Request, res: Response, next) => {
    try {
        let file;
        if (req['files']) {
            file = req['files'].file;
        }
        let producto = JSON.parse(req.body.producto);
        const {idP} = req.body;
        if (file) {
            const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
            const archivo = await DriveApiService.subirArchivo(idFolder, file, producto.idCliente + '-cliente')
            producto.archivo = archivo;
        }
        await ProductoClienteQueries.update(producto, idP);
        res.status(200).send()
    } catch (e) {
        res.status(500).send( e );
    }
});
// online
productoClienteRoutes.put('/AllOnline', async (req: Request, res: Response, next) => {
    try {
        let file;
        if (req['files']) {
            file = req['files'].file;
        }
        let producto = JSON.parse(req.body.producto);
        const {idP} = req.body;
        if (file) {
            const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
            const archivo = await DriveApiService.subirArchivo(idFolder, file, producto.idCliente + '-cliente')
            const contrasenaApp = Math.random().toString(36).slice(-8);
            await ClienteQueries.update({contrasenaApp}, producto.idCliente)
            producto.archivo = idFolder;
        }
        await ProductoClienteQueries.update(producto, idP);
        res.status(200).send()
    } catch (e) {
        res.status(500).send( e );
    }
});
// lo usa cotizador  online
productoClienteRoutes.get('/getById', async (req: Request, res: Response, next) => {
    const {idproductocliente} = req.headers;
    if(!idproductocliente)
        return generales.manejoErrores('No se envio idProductoCliente', res)
    const productoCliente = await ProductoClienteQueries.getById(idproductocliente);
    res.status(200).send( productoCliente );
});
productoClienteRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        let noSerie = req.body.datos.numeroSerie;
        if (!noSerie || noSerie === '') {
            noSerie = 'Número de serie no agregado'
        }
        const nuevoProductoCliente = await ProductoClienteModel.create({...req.body, noSerie});
        if (nuevoProductoCliente) {
            res.status(200).send( nuevoProductoCliente );
        }
    } catch (e) {
        console.log(e);
        res.status(500).send( e );
    }
});
productoClienteRoutes.put('', async (req: Request, res: Response, next) => {
    const {idProductoCliente} = req.body;
    if(!idProductoCliente) return generales.manejoErrores('No se envio la informacion', res)
    await ProductoClienteQueries.updateProductoCliente(idProductoCliente, req.body.productoCliente);
    res.status(200).send();
});

export default productoClienteRoutes;
