import {Request, Response, Router} from "express";
const generales = require('../../general/function');

const registroPolizaRoutes = Router();

// NO SE USA
// registroPolizaRoutes.get('/registro', async (req: Request, res: Response, next) => {
//     const idRegistro = +req.headers.idregistro;
//     if (!idRegistro)
//         return generales.manejoErrores('No se envió idRegistro', res);
//     const registro = await RegistroPolizaQueries.findRegistroById(idRegistro);
//     res.status(200).send( registro );
// });

// NO SE USA
// registroPolizaRoutes.get('/registro-vn/existe-poliza', async (req: Request, res: Response, next) => {
//     const poliza = req.body.poliza;
//     const idSocio = req.body.idSocio;
//     const mes = req.body.mes;
//     const anio = req.body.anio;
//     const registro = await RegistroPolizaQueries.buscarPoliza(poliza, idSocio, mes, anio);
//     res.status(200).send( registro );
// });

// NO SE USA
// registroPolizaRoutes.get('/recibovn/registro', async (req: Request, res: Response, next) => {
//     const idRegistro = +req.headers.idregistro;
//     if (!idRegistro)
//         return generales.manejoErrores('No se envió idRegistro', res);
//     const registro = await RegistroPolizaQueries.findAllByIdRegistroAndActivo(idRegistro);
//     res.status(200).send( registro );
// });

export default registroPolizaRoutes;
