import { Request, Response, Router } from 'express'
import EmpleadoService from "../../../queries/administracion-personal/empleados";
import UsuariosDepartamentoServices from "../../../queries/ti/usuariosDepartamentoServices";
import SolicitudVacacionesService from "../../../queries/administracion-personal/vacaciones/solicitudVacaciones";
import Empleado from "../../../models/administracion-personal/empleados/Empleado";
import EsquemaVacacionesService from "../../../queries/administracion-personal/vacaciones/esquemaVacacionesService";
import PlazaQueries from "../../../queries/RRHH/plazaQueries";
const generales = require('../../../general/function');
const SolicitudVacacionesRoutes = Router();
/// NEW.2
SolicitudVacacionesRoutes.get('/byDepartamento', async (req: Request, res: Response, next) => {
    try {
        const {idestado, idempleado} = req.headers;
        console.log(req.headers);
        if (!(idestado && idempleado)) return generales.manejoErrores('No se envio el id', res);
        const permisos = await EmpleadoService.getPermisosVisualizacion(+idempleado);
        // let cantidadVacaciones;
        // let query = "";
        const aux = [];
        // switch (+permisos.idPermisoVisualizacion) {
        //     case 2:
        if (+idestado === 3) {
            // Supervisores solo deben de ver lo de sus chavos
            const hijos = await EmpleadoService.getPlazasHijo(permisos.id);
            for (const hijo of hijos) {
                aux.push(hijo.id)
            }
            // if (hijos.length === 0){
            //     aux.push(+id)
            // }
            // query = `WHERE empleado.id IN (${aux})`
        }

        //         } else {
        //             const departamentos = await UsuariosDepartamentoServices.getDepartamentoByIdUsuario(permisos.idUsuario);
        //             for (const departamento of departamentos) {
        //                 aux.push(departamento.idDepartamento)
        //             }
        //             if (aux.length === 0) {
        //                 aux.push(permisos.idDepartamento)
        //             }
        //             query = `WHERE plaza."idDepartamento" IN (${aux})`
        //         }
        //         break;
        //     case 3:
        //         query = `WHERE empleado.id IN (${id})`
        //         break
        // }
        const cantidadVacaciones = (aux.length === 0 && +idestado === 3) ? [] : await SolicitudVacacionesService.byDepartamentos(+idestado, +idempleado, aux);
        // const cantidadVacaciones = await SolicitudVacacionesService.byDepartamentos(+idestado, +idempleado);

        return res.status(200).send(cantidadVacaciones);
    } catch (err) {
        next(err);
    }
});
SolicitudVacacionesRoutes.get('/byDepartamento2', async (req: Request, res: Response, next) => {
    try {
        const {idestado, idempleado} = req.headers;
        console.log(req.headers);
        if (!(idestado && idempleado)) return generales.manejoErrores('No se envio el id', res);
        const permisos = await EmpleadoService.getPermisosVisualizacion(+idempleado);
        // let cantidadVacaciones;
        // let query = "";
        const aux = [];
        // switch (+permisos.idPermisoVisualizacion) {
        //     case 2:
        if (+idestado === 3) {
            // Supervisores solo deben de ver lo de sus chavos
            const hijos = await EmpleadoService.getPlazasHijo(permisos.id);
            for (const hijo of hijos) {
                aux.push(hijo.id)
            }
            // if (hijos.length === 0){
            //     aux.push(+id)
            // }
            // query = `WHERE empleado.id IN (${aux})`
        }

        //         } else {
        //             const departamentos = await UsuariosDepartamentoServices.getDepartamentoByIdUsuario(permisos.idUsuario);
        //             for (const departamento of departamentos) {
        //                 aux.push(departamento.idDepartamento)
        //             }
        //             if (aux.length === 0) {
        //                 aux.push(permisos.idDepartamento)
        //             }
        //             query = `WHERE plaza."idDepartamento" IN (${aux})`
        //         }
        //         break;
        //     case 3:
        //         query = `WHERE empleado.id IN (${id})`
        //         break
        // }
        const cantidadVacaciones = (aux.length === 0 && +idestado === 3) ? [] : await SolicitudVacacionesService.byDepartamentos2(+idestado, +idempleado, aux);
        // const cantidadVacaciones = await SolicitudVacacionesService.byDepartamentos(+idestado, +idempleado);

        return res.status(200).send(cantidadVacaciones);
    } catch (err) {
        next(err);
    }
});


SolicitudVacacionesRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = +(req.params.id);
        const {idEstadoVacacion} = req.body;
        await SolicitudVacacionesService.updateSolicitudVacaciones(id, req.body);
        if (idEstadoVacacion === 3 || idEstadoVacacion === 4) {
            const {idEmpleado, cantidadDias} = await SolicitudVacacionesService.getById(id);
            const empleado = await Empleado.findByPk(idEmpleado)
            await empleado.update({cantidadVacaciones: empleado.cantidadVacaciones + cantidadDias});
        }
        return res.status(200).send();
    } catch (err) {
        next(err);
    }
});

SolicitudVacacionesRoutes.get('/cantidad-vacaciones/:idEmpleado', async (req: Request, res: Response, next) => {
    try {
        const idEmpleado = +(req.params.idEmpleado);
        if (!idEmpleado) return generales.manejoErrores('No se envio el idempleado', res);
        const cantidadVacaciones = await SolicitudVacacionesService.getCantidadVacacionesByIdEmpleado(idEmpleado);
        return res.status(200).send(cantidadVacaciones);
    } catch (err) {
        next(err);
    }
});



SolicitudVacacionesRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body)
        const { cantidadDias, idEmpleado, fechaInicio, fechaFinal } = req.body;
        const fi= fechaInicio.split('T')[0];
        const ff= fechaFinal.split('T')[0];
        const vacacionesSolicitadas = await SolicitudVacacionesService.getPasadas(idEmpleado, fi, ff)
        console.log(vacacionesSolicitadas)
        if (vacacionesSolicitadas.length > 0){
            return res.status(200).send(false);
        } else {
            await SolicitudVacacionesService.create(req.body);
            const empleado = await Empleado.findByPk(idEmpleado);
            console.log(empleado)
            if (empleado) {
                await empleado.update({cantidadVacaciones: empleado.cantidadVacaciones - cantidadDias});
                return res.status(200).send(true);
            }
        }
    } catch (err) {
        next(err);
    }
});

// Fin NEW.2

// OLD NEW


SolicitudVacacionesRoutes.get('/byEmpleado', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const cantidadVacaciones = await SolicitudVacacionesService.byIdEmpleado(+id);
        return res.status(200).send(cantidadVacaciones);
    } catch (err) {
        next(err);
    }
});


/////
SolicitudVacacionesRoutes.get('/esquema-vacaciones', async (req: Request, res: Response, next) => {
    try {
        const esquemas = await EsquemaVacacionesService.getActivos();
        return res.status(200).send(esquemas);
    } catch (err) {
        next(err);
    }
});

SolicitudVacacionesRoutes.get('/autorizar-completas/:id', async (req: Request, res: Response, next) => {
    try {
        const idVacaciones = +req.params.id;
        const SolicitudVacacioness = await SolicitudVacacionesService.getCompletoAutorizarSolicitudesById(idVacaciones);
        return res.status(200).send(SolicitudVacacioness);
    } catch (err) {
        next(err);
    }
});

SolicitudVacacionesRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        const Vacaciones = await SolicitudVacacionesService.create(req.body);
        return res.status(200).send(Vacaciones);
    } catch (err) {
        next(err);
    }
});
export default SolicitudVacacionesRoutes;
