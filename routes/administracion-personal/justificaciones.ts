import { Request, Response, Router } from 'express'
import SolicitudIncapacidadService from '../../queries/administracion-personal/justificaciones';
import DriveApiService from "../../services/drive/driveApiService";

const generales = require('../../general/function');

const JustificacionesRoutes = Router();


JustificacionesRoutes.get('', async (req: Request, res: Response, next) => {
    try {
        const prospectos = await SolicitudIncapacidadService.get();
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.get('/byEmpleadoAndEstado', async (req: Request, res: Response, next) => {
    try {
        const { idempleado, idestado, permiso } = req.headers;
        if (!(idempleado && idestado && permiso)) return generales.manejoErrores('No se envio la info', res);
        const prospectos = await SolicitudIncapacidadService.getByEstado(+idempleado, +idestado, +permiso);
        // const prospectos = await SolicitudIncapacidadService.getByEmpleadoAndEstado(+idempleado, +idestado, +permiso);
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.get('/obtenerArchivo', async (req: Request, res: Response, next) => {
    try {
        const id = req.headers.id;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const archivo = await DriveApiService.obtenerArchivo(id);
        return res.status(200).send([archivo]);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.get('/byEstado', async (req: Request, res: Response, next) => {
    try {
        const { idestado } = req.headers;
        if (!idestado) return generales.manejoErrores('No se envio el id', res);
        const prospectos = await SolicitudIncapacidadService.getByEstadoAdp(+idestado);
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.get('/byMismoFolio', async (req: Request, res: Response, next) => {
    try {
        const { id, folio } = req.headers;
        if (!(id && folio)) return generales.manejoErrores('No se envio el id', res);
        const prospectos = await SolicitudIncapacidadService.getByMismoFolio(+id, folio);
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = +(req.params.id);
        const prospectos = await SolicitudIncapacidadService.getJustificacionesById(id);
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body);
        const {datos, nombreArchivo} = req.body;
        const file = req['files'].file;
        console.log(file);
        const { idEmpleado, ...data } = JSON.parse(datos);
        if (!(idEmpleado && nombreArchivo && file)) return generales.manejoErrores('No se envió la información necesaria', res);
        // const unico = await SolicitudIncapacidadService.folioUnico(folio)
        const idFolder = await DriveApiService.findOrCreateFolder(3, idEmpleado);
        const idFile = await DriveApiService.subirArchivo(idFolder, file, nombreArchivo);
        await SolicitudIncapacidadService.create({...data, documento: idFile, idEmpleado});
        return res.status(200).send();
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.put('/actualizarEstatus', async (req: Request, res: Response, next) => {
    try {
        console.log(req.body);
        const { id, ...data } = req.body;
        await SolicitudIncapacidadService.updateEstatus(id, data);
        return res.status(200).send();
    } catch (err) {
        next(err);
    }
});

JustificacionesRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = Number(req.params.id);
        const prospectos = await SolicitudIncapacidadService.update(id, req.body);
        return res.status(200).send(prospectos);
    } catch (err) {
        next(err);
    }
});

export default JustificacionesRoutes;
