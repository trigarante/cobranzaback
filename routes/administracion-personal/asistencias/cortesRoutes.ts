import {Request, Response, Router} from "express";
import CortesService from "../../../queries/administracion-personal/asistencias/cortesService";
const generales = require('../../../general/function');
const cortes = Router();

cortes.get('/ultimo', async (req:Request, res: Response, next) => {
    try {
        const fechas = await CortesService.traigoCorte();
        return res.status(200).send(fechas);
    } catch (err) {
        next(err);
    }
});
export default cortes;
