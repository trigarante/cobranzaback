import {NextFunction, Request, Response, Router} from "express";
import RegistroQueries from "../../queries/registro/registroQueries";

const router = Router();

router.get('/:idRegistro', async(req: Request, res: Response, next: NextFunction) => {
   try {
       const detalles = await RegistroQueries.getDetallesRegistro(+req.params.idRegistro);
       res.json(detalles);
   } catch (e) {
       res.status(500).json(e);
   }
});

export default router;