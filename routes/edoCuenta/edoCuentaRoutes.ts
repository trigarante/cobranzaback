import {Request, Response, Router} from "express";
const generales = require('../../general/function');
import EdoCuentaQueries from '../../queries/edoCuenta/edoCuentaQueries';

const edoCuentaRoutes = Router();

edoCuentaRoutes.get('/getRegistro/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        console.log('d')
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun paramtero', res);
        let edo;
        edo = await EdoCuentaQueries.edoCuentaRegistro(fechaInicio, fechaFin).catch(e => console.log(e));
        res.status(200).send(edo);
    } catch (err) {
        next(err);
    }
});
edoCuentaRoutes.get('/getAplicado/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        console.log('aaa')
    } catch (err) {
        next(err);
    }
});

export default edoCuentaRoutes;
