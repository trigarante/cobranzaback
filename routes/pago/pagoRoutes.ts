// Subida de archivos a drive registro, pago, inspeccion etc
import {Request, Response, Router} from "express";
import DriveApiService from "../../services/drive/driveApiService";
import ProductoClienteQueries from "../../queries/solicitudes/producto-cliente/producto-cliente-queries";
import PagosQueries from "../../queries/pagos/pagosQueries";
import AutorizacionRegistroQueries
    from "../../queries/AutorizacionErrores/autorizacionRegistro/autorizacionRegistroQueries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";
import RegistroQueries from "../../queries/registro/registroQueries";
import ClienteQueries from "../../queries/cliente/cliente-queries";
import AutorizacionPagoModel from "../../models/venta-nueva/autorizacionErrores/autorizacionPago/autorizacionPago";
import axios from "axios";
const PagoRoutes = Router();
const generales = require('../../general/function');


PagoRoutes.get('/byId/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await PagosQueries.getById(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
})
// lo usa cotizador  online
PagoRoutes.post('/subir-archivo-otro', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const file = req['files'].file;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idp} = req.body;
        let pago = JSON.parse(req.body.pago);
        if (!(idp)) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoClienteQueries.percaPago(idp);
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, producto.id, idFolder);
        pago.archivo = await DriveApiService.subirArchivo(idFolderPago, file, producto.id + '-pago-1');
        const id = await PagosQueries.post(pago)
        await AutorizacionRegistroQueries.post({idRegistro: producto.id});
        await SolicitudesInternoQueries.update({emitida: 1}, idp);
        await RegistroQueries.update({idEstadoPoliza: 4}, producto.id)
        if(producto.idUsuarioApp === 0) {
            axios
                .post('https://app.core-ahorraseguros.com/v1/users', {
                    correo: producto.correo,
                    numeroMovil: producto.numero,
                    password: '',
                    nombre: producto.nombre,
                })
                .then(async data => {
                    await ClienteQueries.update({idUsuarioApp: data.data.id}, producto.idCliente);
                })
                .catch(error => {
                    console.error(error);
                });
        } else {
            await ClienteQueries.update({idUsuarioApp: producto.idUsuarioApp}, producto.idCliente);
        }
        return res.status(200).send({id});
    } catch (err) {
        next(err)
    }
});
PagoRoutes.post('/subir-archivo-otro-pago/:numeroRecibo', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const file = req['files'].file;
        const numeroRecibo = req.params.numeroRecibo;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idp} = req.body;
        let pago = JSON.parse(req.body.pago);
        if (!(idp)) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoClienteQueries.percaPagoOtro(idp);
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, producto.id, idFolder);
        /*Recibe el idForlder, el archivo y el nombre del archivo y retorna un id de archivo*/
        pago.archivo = await DriveApiService.subirArchivo(idFolderPago, file, producto.id + '-pago-' + numeroRecibo);
        /*Crea un registro del pago con el id de el archivo que se guardo*/
        const id = await PagosQueries.post(pago);
        console.log('Id de PAGO');
        console.log(id);
        const data = {
            idPago: id,
            idEstadoVerificacion: 1,
            idEmpleado: JSON.parse(req.body.pago).idEmpleado,
        }
        await AutorizacionPagoModel.create(data);
        // await RegistroQueries.update({idEstadoPoliza: 4}, producto.id)
        return res.status(200).send({id});
    } catch (err) {
        next(err)
    }
});
PagoRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        await PagosQueries.post(req.body)
        return res.status(200).send();
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});
PagoRoutes.post('/subir-archivo-otro-reno', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const file = req['files'].file;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idp} = req.body;
        let pago = JSON.parse(req.body.pago);
        if (!(idp)) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoClienteQueries.percaPagoReno(idp);
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, producto.id, idFolder);
        pago.archivo = await DriveApiService.subirArchivo(idFolderPago, file, producto.id + '-pago-1');
        const id = await PagosQueries.post(pago)
        await AutorizacionRegistroQueries.post({idRegistro: producto.id});
        await SolicitudesInternoQueries.update({emitida: 1}, idp);
        await RegistroQueries.update({idEstadoPoliza: 4}, producto.id)
        return res.status(200).send({id});
    } catch (err) {
        next(err)
    }
});

PagoRoutes.post('/subir-archivo-otro-reno-enr', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const file = req['files'].file;
        /* Se deben enviar 3 id, el cliente, el del registro y del pago, inspeccion, etc
         */
        const {idp} = req.body;
        let pago = JSON.parse(req.body.pago);
        if (!(idp)) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoClienteQueries.percaPagoReno(idp);
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, producto.id, idFolder);
        pago.archivo = await DriveApiService.subirArchivo(idFolderPago, file, producto.id + '-pago-1');
        const id = await PagosQueries.post(pago)
        // await AutorizacionRegistroQueries.post({idRegistro: producto.id});
        await SolicitudesInternoQueries.update({emitida: 1}, idp);
        await RegistroQueries.update({idEstadoPoliza: 4}, producto.id)
        return res.status(200).send({id});
    } catch (err) {
        next(err)
    }
});

PagoRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        await PagosQueries.post(req.body)
        return res.status(200).send();
        // return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

PagoRoutes.put('/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!(id)) return generales.manejoErrores('No se envio la informacion', res);
        await PagosQueries.update(req.body, id)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});
PagoRoutes.put('/update-archivo/:id/:archivo/:idEmpleadoModificante', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        const {archivo} = req.params;
        const {idEmpleadoModificante} = req.params;
        if (!(id && archivo && idEmpleadoModificante)) return generales.manejoErrores('No se envio la informacion', res);
        const pagosUpdate = await PagosQueries.getByIdEstado(id)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});
PagoRoutes.get('/recibo/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const byId = await PagosQueries.getByIdRecibo(id);
        res.status(200).send(byId);
    } catch (err) {
        next(err);
    }
})


export default PagoRoutes;
