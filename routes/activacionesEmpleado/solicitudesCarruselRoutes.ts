import {Request, Response, Router} from "express";
import SolicitudesCarruselQueries from "../../queries/activacionEmpleado/solicitudesCarruselQueries";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
const generales = require('../../general/function')

const SolicitudesCarruselRoutes = Router();

SolicitudesCarruselRoutes.post('', async (req: Request, res: Response, next) => {
   try {
       await SolicitudesCarruselQueries.post(req.body);
       res.send();
   } catch (e) {
       next(e)
   }
});

SolicitudesCarruselRoutes.get('/byIdEstado', async (req: Request, res: Response, next) => {
    try {
        const {id, idempleado} = req.headers;
        if (!(id && idempleado)) return generales.manejoErrores('No se envio el id', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idempleado);
        let query = '';
        switch (+permisos.idPermisoVisualizacion) {
            case 2:
                // if (+permisos.idPuesto === 7) {
                //     const hijos = await PlazaQueries.getPlazasHijo(permisos.id);
                //     const aux = [];
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idempleado)
                //     }
                //     query = `and empleado.id IN (${aux})`;
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
                    const aux = [];
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(permisos.idDepartamento)
                    }
                    query = `and departamento.id IN (${aux})`;
                // }
                break;
            case 3:
                query = `and empleado.id = ${idempleado}`
                break;
        }
        const datos = await SolicitudesCarruselQueries.getByIdEstadoSolicitud(id, query);
        res.status(200).send(datos);
    } catch (e) {
        next(e)
    }
});

SolicitudesCarruselRoutes.put('', async (req: Request, res: Response, next) => {
    try {
        const {id, ...data} = req.body;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        await SolicitudesCarruselQueries.update({...data, fechaAutorizacionCoordinador: new Date()}, id)
        res.status(200).send();
    } catch (e) {
        next(e)
    }
});

export default SolicitudesCarruselRoutes;
