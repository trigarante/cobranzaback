import {Router, Request, Response} from "express";
import ReciboQueries from "../../queries/recibo/reciboQueries";
import AsistenciasService from "../../queries/administracion-personal/asistencias/asistenciasService";
const generales = require('../../general/function');

const reciboRoutes = Router();

reciboRoutes.get('/registro/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        const {idrecibo} = req.headers;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const query = +idrecibo ? `and re.id = ${idrecibo}` : '';
        const recibo = await ReciboQueries.findAllByIdRegistroAndActivo(id, query);
        res.status(200).send( recibo );
    } catch (e) {
        res.status(500).send(e)
    }
})

reciboRoutes.get('/forPago/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const recibo = await ReciboQueries.forPagosByidRegistro(id);
        res.status(200).send( recibo );
    } catch (e) {
        res.status(500).send(e)
    }
})
reciboRoutes.get('/forPagoByid/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        console.log(id);
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const recibo = await ReciboQueries.forPagosByIdRecibo(id);
        console.log(recibo)
        res.status(200).send( recibo );
    } catch (e) {
        res.status(500).send(e)
    }
})
reciboRoutes.put('/bajaRecibos', async(req:Request, res: Response, next) => {
    try {
        const {idRegistro} = req.body;
        if (!idRegistro ) return generales.manejoErrores('No se envio el id Registro', res);
        const baja = await ReciboQueries.updateBaja(idRegistro, {activo: 0});
        res.status(200).send( baja );
    } catch (e) {
        res.sendStatus(500).send(e)
    }
})
reciboRoutes.post('/recibos', async(req:Request, res: Response, next) => {
    try {
        for (const recibo of req.body) {
            await ReciboQueries.post(recibo);
        }
        res.status(200).send();
    } catch (e) {
        res.sendStatus(500).send(e)
    }
})
export default reciboRoutes;
