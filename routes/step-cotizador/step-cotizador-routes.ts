import {Request, Response, Router} from "express";
import StepCotizadorQueries from "../../queries/step-cotizador/step-cotizador-queries";
const stepCotizadorRoutes = Router();
// lo usa cotizador  online
stepCotizadorRoutes.get('/:idSolicitud', async (req: Request, res: Response, next) => {
    try {
        const idSolicitud = req.params.idSolicitud;
        const solicitudesUltimoStep = await StepCotizadorQueries.getSolicitudes(idSolicitud);
        res.status(200).send(solicitudesUltimoStep);
    } catch (err) {
        next(err);
    }
});

stepCotizadorRoutes.get('/registro/:idRegistro', async (req: Request, res: Response, next) => {
    try {
        const idRegistro = req.params.idRegistro;
        const registroUltimoStep = await StepCotizadorQueries.getRegistro(idRegistro);
        res.status(200).send(registroUltimoStep);
    } catch (err) {
        next(err);
    }
});

export default stepCotizadorRoutes;
