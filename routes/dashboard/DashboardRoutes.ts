import {Request, Response, Router} from "express";
import DashboardQueries from "../../queries/dashboard/dashboardQueries";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');
const switchPermisosVisualizacion = require('../../general/permisosVisualizacion')

const DashboardRoutes = Router();


// Dashboard's routes
DashboardRoutes.get('/departamentos', async (req: Request, res: Response, next) => {
    try {
        const deptos = await DashboardQueries.getDepartamentos();
        res.status(200).send(deptos);
    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-dashboard-pjsip/:idEmpleado/:fechaInicio/:fechaFin/:idSubarea', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin, idSubarea} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin && idSubarea)) return generales.manejoErrores('No se envio algun paramtero', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let dash;
        console.log(idSubarea)
        console.log(permisos)
        // dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin).catch(e => console.log(e));

        if(+idSubarea === 0){
            dash = await switchPermisosVisualizacion.switchPermisosVisualizacion(permisos, fechaInicio, fechaFin, idEmpleado)
        } else {
            dash = await switchPermisosVisualizacion.switchPermisosVisualizacionByArea(permisos, fechaInicio, fechaFin, idEmpleado, idSubarea)
        }
        res.status(200).send(dash);

    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-leads/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun paramtero', res);
        let dash;
        dash = await DashboardQueries.descargarLeads(fechaInicio, fechaFin).catch(e => console.log(e));
        res.status(200).send(dash);
    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-llamadas/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun paramtero', res);
        let dash;
        dash = await DashboardQueries.descargarLlamadas(fechaInicio, fechaFin).catch(e => console.log(e));
        res.status(200).send(dash);
    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-dash/:idEmpleado/:fechaInicio/:fechaFin/:tipo', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin, tipo} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin && tipo)) return generales.manejoErrores('No se envio algun paramtero', res);
        let dash;
        switch (+tipo){
            case 1:
                dash = await DashboardQueries.descargaColumnaError(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
            case 2:
                dash = await DashboardQueries.descargaColumnaNoContactados(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
            case 3:
                dash = await DashboardQueries.descargaColumnaMarcacion1a4(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
            case 4:
                dash = await DashboardQueries.descargaColumnaMarcacion5(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
            case 5:
                dash = await DashboardQueries.descargaColumnaContactados(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
            default:
                dash = await DashboardQueries.descargaColumnaRegistro(fechaInicio, fechaFin, idEmpleado).catch(e => console.log(e));
                break;
        }
        res.status(200).send(dash);
    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-descarga-llamadas/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun paramtero', res);
        let dash;
        dash = await DashboardQueries.descargarLlamadas(fechaInicio, fechaFin).catch(e => console.log(e));
        res.status(200).send(dash);
    } catch (err) {
        next(err);
    }
});
DashboardRoutes.get('/get-dashboard-filter-pjsip/:idEmpleado/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio algun paramtero', res);
        let dash;
        dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin, ``,``).catch(e => console.log(e));
        res.status(200).send(dash);
    } catch (err) {
        next(err);
    }
});
export default DashboardRoutes;
