import {Router, Request, Response} from "express";
import TipoEndosoQueries from "../../queries/tipoEndoso/TipoEndosoQueries";
import SubsecuentesQueries from "../../queries/subsecuentes/subsecuentesQueries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');

const tipoEndosoRoutes = Router();

tipoEndosoRoutes.get('/', async(req:Request, res: Response, next) => {
    try {
        const tipoEndoso = await TipoEndosoQueries.getAll();
        res.status(200).send( tipoEndoso );
    } catch (e) {
        res.sendStatus(500);
    }
})

tipoEndosoRoutes.get('/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empelado', res);
        const permisos = await SolicitudesInternoQueries.getPermisosVisualizacion(+idEmpleado);
        let subsecuentes;
        switch (+permisos.idPermisoVisualizacion) {
            case 1:
                subsecuentes = await SubsecuentesQueries.findAll(fechaInicio, fechaFin);
                break;
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
                const aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                subsecuentes = await SubsecuentesQueries.subsecuentesDepartamentos(fechaInicio, fechaFin, aux);
                break;
            case 3:
                subsecuentes = await SubsecuentesQueries.subsecuentesEmpleado(permisos.idDepartamento, idEmpleado, fechaInicio, fechaFin);
                break;
        }
        res.status(200).send(subsecuentes);
    } catch (e) {
        res.sendStatus(500);
    }
})


export default tipoEndosoRoutes;
