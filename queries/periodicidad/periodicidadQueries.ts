import PeriodicidadModel from "../../models/venta-nueva/periodicidad/periodicidadModel";

export default class PeriodicidadQueries {
    static async getActivos() {
        return await PeriodicidadModel.findAll({
            where: {activo: 1},
        })
    }
}
