import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import AutorizacionRegistroModel
    from "../../../models/venta-nueva/autorizacionErrores/autorizacionRegistro/autorizacionRegistroModel";

export default class AutorizacionRegistroQueries {
    static async getById(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            DISTINCT ON (r.id)
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                pg.id AS "idPago",
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."autorizacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            WHERE ar.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async getByIdRegistro(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            DISTINCT ON (r.id)
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                pg.id AS "idPago",
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."autorizacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            WHERE ar."idRegistro" = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async getByIdEstado(id): Promise<any> {
        return await AutorizacionRegistroModel.findOne({
            where: {id},
            raw: true
        })
    }

    static async getAll(aux2, fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."autorizacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
                where
                 rb.numero = 1
                 and
                 rb.activo = 1
                 AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                 and
                 r."idEstadoPoliza" = 4
                 and
                 r."fechaCierre" is null
                 and
                 ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                  AND
                r.emitida = 1 and pc.archivo IS NOT NULL
                 AND
                 ${aux2}
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async getAllSubsecuentes(aux2, fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."autorizacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
                where
                 rb.numero = 1
                 and
                 rb.activo = 1
                 and
                 r."idFlujoPoliza" = 7
                 and
                 r."idEstadoPoliza" = 4
                 and
                 r."fechaCierre" is null
                 and
                 ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                  AND
                r.emitida = 1 and pc.archivo IS NOT NULL
                 AND
                 ${aux2}
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async getAllENR(aux2, fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ar."idEmpleado" AS "idEmpleadoAutorizacion",
                r."idEmpleado",
                r."idProducto",
                r.poliza,
                pc."idCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                i.archivo AS "archivoInspeccion",
                r."idFlujoPoliza" AS "flujoPoliza",
                dp.descripcion,
                s."nombreComercial",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago"
                FROM
                operaciones."autorizacionRegistro" ar
                INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones.socios s ON r."idSocio" = s.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
                where
                 rb.numero = 1
                 and
                 rb.activo = 1
                 and
                 r."idFlujoPoliza" = 58
                 and
                 r."idEstadoPoliza" = 4
                 and
                 r."fechaCierre" is null
                 and
                 ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                  AND
                r.emitida = 1 and pc.archivo IS NOT NULL
                 AND
                 ${aux2}
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async getAllByDepartamentos(aux2, fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
            ar.id,
            ar."idRegistro",
            ar."idEstadoVerificacion",
            ev.estado AS "estadoVerificacion",
            ar.numero,
            ar."verificadoCliente",
            ar."verificadoProducto",
            ar."verificadoRegistro",
            ar."verificadoPago",
            ar."verificadoInspeccion",
            ar."fechaCreacion",
            ar."idEmpleado" AS "idEmpleadoAutorizacion",
            r."idEmpleado",
            r."idProducto",
            r.poliza,
            pc."idCliente",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
            pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
            pc.archivo as "carpetaCliente",
            r.archivo AS "carpetaRegistro",
            i.archivo AS "archivoInspeccion",
            r."idFlujoPoliza" AS "flujoPoliza",
            dp.descripcion,
            s."nombreComercial",
            pg.id AS idPago,
            pg.archivo AS "archivoPago",
            pg."fechaPago"
            FROM
            operaciones."autorizacionRegistro" ar
            INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
            INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
            INNER JOIN operaciones.socios s ON r."idSocio" = s.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
            INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
            INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
            LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            where
            rb.numero = 1
            and
            rb.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
            and
            r."idEstadoPoliza" = 4
            and
            r."fechaCierre" is null
            and
            ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            and r."idDepartamento" IN (${departamentos})
            AND
            r.emitida = 1 and pc.archivo IS NOT NULL
            AND
            ${aux2}
            ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentosSubs(aux2, fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
            ar.id,
            ar."idRegistro",
            ar."idEstadoVerificacion",
            ev.estado AS "estadoVerificacion",
            ar.numero,
            ar."verificadoCliente",
            ar."verificadoProducto",
            ar."verificadoRegistro",
            ar."verificadoPago",
            ar."verificadoInspeccion",
            ar."fechaCreacion",
            ar."idEmpleado" AS "idEmpleadoAutorizacion",
            r."idEmpleado",
            r."idProducto",
            r.poliza,
            pc."idCliente",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
            pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
            pc.archivo as "carpetaCliente",
            r.archivo AS "carpetaRegistro",
            i.archivo AS "archivoInspeccion",
            r."idFlujoPoliza" AS "flujoPoliza",
            dp.descripcion,
            s."nombreComercial",
            pg.id AS idPago,
            pg.archivo AS "archivoPago",
            pg."fechaPago"
            FROM
            operaciones."autorizacionRegistro" ar
            INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
            INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
            INNER JOIN operaciones.socios s ON r."idSocio" = s.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
            INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
            INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
            LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            where
            rb.numero = 1
            and
            rb.activo = 1
            and
            r."idFlujoPoliza" = 7
            and
            r."idEstadoPoliza" = 4
            and
            r."fechaCierre" is null
            and
            ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            and r."idDepartamento" IN (${departamentos})
            AND
            r.emitida = 1 and pc.archivo IS NOT NULL
            AND
            ${aux2}
            ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentosENR(aux2, fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
            ar.id,
            ar."idRegistro",
            ar."idEstadoVerificacion",
            ev.estado AS "estadoVerificacion",
            ar.numero,
            ar."verificadoCliente",
            ar."verificadoProducto",
            ar."verificadoRegistro",
            ar."verificadoPago",
            ar."verificadoInspeccion",
            ar."fechaCreacion",
            ar."idEmpleado" AS "idEmpleadoAutorizacion",
            r."idEmpleado",
            r."idProducto",
            r.poliza,
            pc."idCliente",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
            pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
            pc.archivo as "carpetaCliente",
            r.archivo AS "carpetaRegistro",
            i.archivo AS "archivoInspeccion",
            r."idFlujoPoliza" AS "flujoPoliza",
            dp.descripcion,
            s."nombreComercial",
            pg.id AS idPago,
            pg.archivo AS "archivoPago",
            pg."fechaPago"
            FROM
            operaciones."autorizacionRegistro" ar
            INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
            INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
            INNER JOIN operaciones.socios s ON r."idSocio" = s.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
            INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
            INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
            LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            where
            rb.numero = 1
            and
            rb.activo = 1
            and
            r."idFlujoPoliza" = 58
            and
            r."idEstadoPoliza" = 4
            and
            r."fechaCierre" is null
            and
            ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            and r."idDepartamento" IN (${departamentos})
            AND
            r.emitida = 1 and pc.archivo IS NOT NULL
            AND
            ${aux2}
            ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByHijos(aux2, fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
            ar.id,
            ar."idRegistro",
            ar."idEstadoVerificacion",
            ev.estado AS "estadoVerificacion",
            ar.numero,
            ar."verificadoCliente",
            ar."verificadoProducto",
            ar."verificadoRegistro",
            ar."verificadoPago",
            ar."verificadoInspeccion",
            ar."fechaCreacion",
            ar."idEmpleado" AS "idEmpleadoAutorizacion",
            r."idEmpleado",
            r."idProducto",
            r.poliza,
            pc."idCliente",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
            pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
            pc.archivo as "carpetaCliente",
            r.archivo AS "carpetaRegistro",
            i.archivo AS "archivoInspeccion",
            r."idFlujoPoliza" AS "flujoPoliza",
            dp.descripcion,
            s."nombreComercial",
            pg.id AS idPago,
            pg.archivo AS "archivoPago",
            pg."fechaPago"
            FROM
            operaciones."autorizacionRegistro" ar
            INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
            INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
            INNER JOIN operaciones.socios s ON r."idSocio" = s.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
            INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
            INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
            LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            where
            rb.numero = 1
            and
            rb.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
            and
            r."idEstadoPoliza" = 4
            and
            r."fechaCierre" is null
            and
            ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            and r."idEmpleado" IN (${departamentos})
            AND
            r.emitida = 1 and pc.archivo IS NOT NULL
            AND
            ${aux2}
            ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, aux2, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
            ar.id,
            ar."idRegistro",
            ar."idEstadoVerificacion",
            ev.estado AS "estadoVerificacion",
            ar.numero,
            ar."verificadoCliente",
            ar."verificadoProducto",
            ar."verificadoRegistro",
            ar."verificadoPago",
            ar."verificadoInspeccion",
            ar."fechaCreacion",
            ar."idEmpleado" AS "idEmpleadoAutorizacion",
            r."idEmpleado",
            r."idProducto",
            r.poliza,
            pc."idCliente",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno  AS "nombreCliente",
            pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
            pc.archivo as "carpetaCliente",
            r.archivo AS "carpetaRegistro",
            i.archivo AS "archivoInspeccion",
            r."idFlujoPoliza" AS "flujoPoliza",
            dp.descripcion,
            s."nombreComercial",
            pg.id AS idPago,
            pg.archivo AS "archivoPago",
            pg."fechaPago"
            FROM
            operaciones."autorizacionRegistro" ar
            INNER JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
            INNER JOIN operaciones.registro r ON ar."idRegistro" = r.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
            INNER JOIN operaciones.socios s ON r."idSocio" = s.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
            INNER JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
            INNER JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
            LEFT JOIN operaciones.inspecciones i ON r.id = i."idRegistro"
            where
            rb.numero = 1
            and
            rb.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
            and
            r."idEstadoPoliza" = 4
            and
            r."fechaCierre" is null
            and
            ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            -- and dp.id = ${idDepartamento}
            AND
            r.emitida = 1 and pc.archivo IS NOT NULL
            AND
            ${aux2}
            ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async update(data, id) {
        return await AutorizacionRegistroModel.update(data,{
            where: {id}
        })
    }

    static async updateByIdRegistro(data, idRegistro) {
        return await AutorizacionRegistroModel.update(data,{
            where: {idRegistro}
        })
    }

    static async updateEstado(id: number, data: AutorizacionRegistroModel): Promise<any> {
        return await AutorizacionRegistroModel.update({...data}, {
            where: {
                id,
            }
        });
    }

    static async post(data) {
        return await  AutorizacionRegistroModel.create(data);
    }
}
