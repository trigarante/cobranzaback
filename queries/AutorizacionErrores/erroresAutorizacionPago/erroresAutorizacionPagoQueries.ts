import ErroresAutorizacionPagoModel
    from "../../../models/venta-nueva/autorizacionErrores/erroresAutorizacionPago/erroresAutorizacionPago";
import AutorizacionPagoModel from "../../../models/venta-nueva/autorizacionErrores/autorizacionPago/autorizacionPago";

export default class ErroresAutorizacionPagoQueries {
    static async getByIdAutorizacionAndDocumento(idAutorizacionPago) {
        return await ErroresAutorizacionPagoModel.findOne({
            where: {
                idAutorizacionPago,
            },
        })
    }

    static async post(data) {
        return await ErroresAutorizacionPagoModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async postPago(data) {
        return await ErroresAutorizacionPagoModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async update(data, id) {
        return await ErroresAutorizacionPagoModel.update(data, {
            where: {id},
        })
    }

    static async updateEstado(idAutorizacionPago: number, data: AutorizacionPagoModel): Promise<any> {
        return await AutorizacionPagoModel.update({...data}, {
            where: {
                id: idAutorizacionPago,
            }
        });
    }
}
