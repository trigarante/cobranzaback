import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class PlazaQueries {
    static async getPlazasHijo(id) {
        return await dbPostgres.query(`
        SELECT empleado.id
        FROM "recursosHumanos".empleado
        JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
        WHERE plaza."idPadre" = ${id}`, {
            type: QueryTypes.SELECT

        })
    }
}
