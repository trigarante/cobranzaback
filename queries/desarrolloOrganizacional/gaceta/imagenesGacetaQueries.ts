import ImagenesGacetaModel from "../../../models/desarrollo-organizacional/trigaranteComunica/imagenesGacetaModel";
import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

export default class ImagenesGacetaQueries {
    static async post(gaceta) {
        return await ImagenesGacetaModel.create(gaceta);
    }

    static async getById(idGaceta: number) {
        return await ImagenesGacetaModel.findAll({
            where: {
                idGaceta
            }
        });
    }

    static async getGacetaActual() {
        return await dbPostgres.query(`
        SELECT *
        FROM "recursosHumanos"."imagenesGaceta" ig
        WHERE ig."idGaceta" = (
            SELECT gaceta.id
            FROM "recursosHumanos".gaceta
            WHERE CURRENT_DATE >= gaceta."fechaLanzamiento"
            AND activo = 1
            ORDER BY gaceta."fechaLanzamiento" DESC
            LIMIT 1
        )
        ORDER BY ig.numero ASC
        `,
            {
                type: QueryTypes.SELECT
            })
    }
}
