import TrigaranteEscucha from "../../models/desarrollo-organizacional/trigarante-escucha/trigaranteEscucha";
import TemaTrigaranteEscucha from "../../models/desarrollo-organizacional/tema-trigaranteEscucha/temaTrigaranteEscucha";
import MotivoTrigaranteEscucha from "../../models/desarrollo-organizacional/motivo-trigaranteEscucha/motivoTrigaranteEscucha";

export default class TrigaranteEscuchaQueries {

    static async getActivos(activo: number) {
        return await TemaTrigaranteEscucha.findAll({
            where: {
                activo
            }
        });
    }
    static async getByIdTema(idTemaTrigaranteEscucha: number) {
        return await MotivoTrigaranteEscucha.findAll({
            where: {
                idTemaTrigaranteEscucha,
                activo: 1
            }
        });
    }
    static async post(data: TrigaranteEscucha) {
        return await TrigaranteEscucha.create(data);
    }

}
