import {Op} from "sequelize";
import EstadoAsistenciaModel from "../../../models/administracion-personal/asistencias/estadoAsistenciaService";

export default class EstadoAsistenciaService {
    static async getActivos() {
        return await EstadoAsistenciaModel.findAll({
            where: {
                activo: 1,
                id: {
                    [Op.not]: 9
                }
            }
        })
    }
    static async getDescripcionByid(id) {
        return await EstadoAsistenciaModel.findOne({
            where: {
                id
            }
        })
    }
}
