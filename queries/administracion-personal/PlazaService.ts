
import {Op, QueryTypes} from "sequelize";
import db, {dbPostgres} from "../../configs/connection";
import Plaza from "../../models/RRHH/plazaModel";
import PlazaModel from "../../models/RRHH/plazaModel";
import Departamento from "../../models/RRHH/Departamento";
import TipoPuesto from "../../models/RRHH/TipoPuesto";


export default class PlazaService {
    static async getIdPadre(idEmpleado: number) {
        return await dbPostgres.query(`
        select
            pl."idPadre" as "idPadre"
        from
            "recursosHumanos".empleado e
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
        where
            e.id = ${idEmpleado}
        `, {
            plain: true,
            type: QueryTypes.SELECT
        })
    }
    static async getById(idPlaza: number) {
        return await Plaza.findOne({
            where: {
                id: idPlaza
            },
            raw: true
        });
    }

    static async create(data: Plaza) {
        return await Plaza.create(data).then(resultEntity => {
            return resultEntity.get({plain:true})
        })
    }

    static async update(idPlaza: number, data: any) {
        return await PlazaModel.update(data, {
            where: {
                id: idPlaza
            },
            returning: true,
            plain: true
        }).then(data => {
            console.log(data[1])
            return data[1].dataValues
        })
    }

    static async get() {
        return await Plaza.findAll({
            include: [
                Departamento,
                TipoPuesto
            ],
            where: {
                [Op.and]: [
                    { idEstadoPlaza: {
                            [Op.not]: 4
                        }
                    },
                    { activo: 1 }
                ]
            },
        });
    }

    static async getPlazas(tipo) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza."idPadre",
            sede.nombre AS sede,
            area.nombre AS area,
            plaza."idDepartamento",
            departamento.descripcion AS departamento,
            plaza."idPuesto",
            puesto.nombre AS puesto,
            plaza."puestoEspecifico",
            plaza."idTipoPlaza",
            "tipoPlaza".nombre AS "tipoPlaza",
            segmento.segmento,
            plaza."idEmpresaPagadora",
            empresas.nombre AS "empresaPagadora",
            plaza."idEmpresaEmpleadora",
            "empresaEmpleadora".nombre AS "empresaEmpleadora",
            "turnoEmpleado".turno,
            plaza."sueldoDiario",
            plaza."sueldoMensual",
            kpi.cantidad || ' - ' || "periodicidadKpi".descripcion as kpi
        FROM
            "recursosHumanos".plaza
        INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
        INNER JOIN "recursosHumanos"."departamento" ON plaza."idDepartamento" = departamento.id
        INNER JOIN "recursosHumanos".area ON area.id = departamento."idArea"
        INNER JOIN "recursosHumanos"."sede" ON plaza."idSede" = "sede".id
        INNER JOIN "recursosHumanos".puesto ON puesto.id = plaza."idPuesto"
        INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
        INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
        LEFT JOIN "recursosHumanos".empresas "empresaEmpleadora" ON "empresaEmpleadora".id = plaza."idEmpresaEmpleadora"
        INNER JOIN "recursosHumanos"."turnoEmpleado" ON "turnoEmpleado".id = plaza."idTurnoEmpleado"
        INNER JOIN "recursosHumanos".kpi ON plaza."idKpi" = kpi."id"
        INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi"."id"
        WHERE
            plaza.activo = 1
            AND "idEstadoPlaza" IN  (${tipo})
        ORDER BY
	        plaza.id DESC`, {
            type: QueryTypes.SELECT
        })
    }

    static async getPlazasEstado() {
        return await db.query(`
        SELECT
            plaza.id,
            plaza.idDepartamento,
            plaza.idTipoPuesto,
            plaza.activo,
            plaza.idTipoPlaza,
            plaza.idTurnoEmpleado,
            plaza.idSueldo,
            plaza.idEmpresaContratante,
            plaza.idEmpresaEmpleadora,
            tipoPlaza.tipoPlaza,
            es.nombre AS empresaEmpleadora,
            empresa.nombre AS empresaContratante,
            tipoPuesto.nombre AS tipoPuesto,
            departamento.descripcion AS subarea,
            dept.descripcion AS area,
            turnoEmpleado.turno,
            turnoEmpleado.horario,
            esquemaVacaciones.descripcion AS esquemaVacaciones,
            esquemaVacaciones.json AS jsonEsquema,
            sueldos.cantidad,
            sueldos.kpi
        FROM
            plaza
        INNER JOIN tipoPlaza ON tipoPlaza.id = plaza.idTipoPlaza
        INNER JOIN departamento ON plaza.idDepartamento = departamento.id
        LEFT JOIN departamento dept ON dept.id = departamento.idPadre
        INNER JOIN tipoPuesto ON plaza.idTipoPuesto = tipoPuesto.id
        INNER JOIN esquemaVacaciones ON tipoPuesto.idEsquemaVacaciones= esquemaVacaciones.id
        INNER JOIN empresa ON empresa.id = plaza.idEmpresaContratante
        LEFT JOIN empresa AS es ON es.id = plaza.idEmpresaEmpleadora
        INNER JOIN turnoEmpleado ON plaza.idTurnoEmpleado = turnoEmpleado.id
        INNER JOIN sueldos ON plaza.idSueldo = sueldos.id
        WHERE
            aprobadoADP = 1
            AND aprobadoDireccion = 1
            AND aprobadoFinanzas = 1
            AND plaza.activo = 1
            AND idEstadoPlaza = 3
            AND tipoPuesto.id = 7
            AND plaza.idTipoPlaza = 2
            ORDER BY plaza.id DESC`, {
            type: QueryTypes.SELECT
        })
    }

    static async getPlazasPendientes() {
        return await db.query(`
        SELECT
            plaza.id,
            plaza.idDepartamento,
            plaza.idTipoPuesto,
            plaza.activo,
            plaza.idTipoPlaza,
            plaza.idTurnoEmpleado,
            plaza.idSueldo,
            plaza.idEmpresaContratante,
            plaza.idEmpresaEmpleadora,
            tipoPlaza.tipoPlaza,
            es.nombre AS empresaEmpleadora,
            empresa.nombre AS empresaContratante,
            tipoPuesto.nombre AS tipoPuesto,
            departamento.descripcion AS subarea,
            dept.descripcion AS area,
            turnoEmpleado.turno,
            turnoEmpleado.horario,
            esquemaVacaciones.descripcion AS esquemaVacaciones,
            esquemaVacaciones.json AS jsonEsquema,
            sueldos.cantidad,
            sueldos.kpi
        FROM
            plaza
        INNER JOIN tipoPlaza ON tipoPlaza.id = plaza.idTipoPlaza
        INNER JOIN departamento ON plaza.idDepartamento = departamento.id
        LEFT JOIN departamento dept ON dept.id = departamento.idPadre
        INNER JOIN tipoPuesto ON plaza.idTipoPuesto = tipoPuesto.id
        INNER JOIN esquemaVacaciones ON tipoPuesto.idEsquemaVacaciones= esquemaVacaciones.id
        INNER JOIN empresa ON empresa.id = plaza.idEmpresaContratante
        LEFT JOIN empresa AS es ON es.id = plaza.idEmpresaEmpleadora
        INNER JOIN turnoEmpleado ON plaza.idTurnoEmpleado = turnoEmpleado.id
        INNER JOIN sueldos ON plaza.idSueldo = sueldos.id
        WHERE
            plaza.activo = 1
            AND idEstadoPlaza = 2
            ORDER BY plaza.id DESC`, {
            type: QueryTypes.SELECT
        })
    }

    static async getArea(iddpto) {
        return await dbPostgres.query(`
        select
            departamento."idArea"
        from
            "recursosHumanos".departamento
        where
            departamento.id = ${iddpto}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async infoPlazas(idPlaza) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza.activo,
            plaza."idPadre",
            plaza."idEstadoPlaza",
            plaza."idSede",
            sede.nombre AS sede,
            plaza."idArea",
            area.nombre AS area,
            departamento."idSegmento",
            segmento.segmento,
            plaza."idDepartamento",
            departamento.descripcion AS departamento,
            plaza."idPuesto",
            puesto.nombre AS puesto,
            plaza."puestoEspecifico",
            plaza."idTipoPlaza",
            "tipoPlaza".nombre AS "tipoPlaza",
            plaza."idEmpresaPagadora",
            empresas.nombre AS "empresaPagadora",
            plaza."idEmpresaEmpleadora",
            es.nombre AS "empresaEmpleadora",
            plaza."idTurnoEmpleado",
            "turnoEmpleado".turno,
            "turnoEmpleado".horario,
            plaza."sueldoDiario",
            plaza."sueldoMensual",
            plaza."idKpi",
            kpi.cantidad,
            kpi."idPeriodicidad",
            "periodicidadKpi".descripcion,
            CONCAT (area.codigo, segmento.codigo, departamento.codigo) AS cc
        FROM
            "recursosHumanos".plaza
            INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
            INNER JOIN "recursosHumanos"."departamento" ON plaza."idDepartamento" = departamento.id
            INNER JOIN "recursosHumanos".area ON area.id = departamento."idArea"
            INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
            LEFT JOIN "recursosHumanos".empresas AS es ON es.id = plaza."idEmpresaEmpleadora"
            LEFT JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
            INNER JOIN "recursosHumanos"."sede" ON plaza."idSede" = "sede".id
            INNER JOIN "recursosHumanos"."estadoPlaza" ON "estadoPlaza".id  = plaza."idEstadoPlaza"
            INNER JOIN "recursosHumanos".puesto ON puesto.id = plaza."idPuesto"
            INNER JOIN "recursosHumanos".kpi ON kpi.id = plaza."idKpi"
            INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi".id
            INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
        WHERE plaza.activo = 1
            AND plaza.id = ${idPlaza}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async publicarPlaza(idPlaza: number, data: Plaza) {
        return await Plaza.update(data, {
            where: {
                id: idPlaza
            }
        })
    }

    // static async getByAprobacion(aprobado, array) {
    //     return await db.query(`
    //     SELECT
    //         plaza.id,
    //         plaza.idDepartamento,
    //         plaza.idTipoPuesto,
    //         plaza.activo,
    //         plaza.idTipoPlaza,
    //         plaza.idTurnoEmpleado,
    //         plaza.idSueldo,
    //         plaza.idEmpresaContratante,
    //         plaza.idEmpresaEmpleadora,
    //         tipoPlaza.tipoPlaza,
    //         es.nombre AS empresaEmpleadora,
    //         empresa.nombre AS empresaContratante,
    //         tipoPuesto.nombre AS tipoPuesto,
    //         departamento.descripcion AS subarea,
    //         dept.descripcion AS area,
    //         turnoEmpleado.turno,
    //         turnoEmpleado.horario,
    //         esquemaVacaciones.descripcion AS esquemaVacaciones,
    //         esquemaVacaciones.json AS jsonEsquema,
    //         sueldos.cantidad,
    //         sueldos.kpi
    //     FROM
    //         plaza
    //     INNER JOIN tipoPlaza ON tipoPlaza.id = plaza.idTipoPlaza
    //     INNER JOIN departamento ON plaza.idDepartamento = departamento.id
    //     LEFT JOIN departamento dept ON dept.id = departamento.idPadre
    //     INNER JOIN tipoPuesto ON plaza.idTipoPuesto = tipoPuesto.id
    //     INNER JOIN esquemaVacaciones ON tipoPuesto.idEsquemaVacaciones= esquemaVacaciones.id
    //     INNER JOIN empresa ON empresa.id = plaza.idEmpresaContratante
    //     LEFT JOIN empresa AS es ON es.id = plaza.idEmpresaEmpleadora
    //     INNER JOIN turnoEmpleado ON plaza.idTurnoEmpleado = turnoEmpleado.id
    //     INNER JOIN sueldos ON plaza.idSueldo = sueldos.id
    //     WHERE
    //         aprobadoAdp = ${array[0]}
    //         AND aprobadoFinanzas = ${array[1]}
    //         AND aprobadoDireccion = ${array[2]}
    //         AND plaza.activo = 1
    //         AND idEstadoPlaza = 2
    //         ORDER BY plaza.id DESC
    //     `,
    //         {
    //             type: QueryTypes.SELECT
    //         })
    // }

    // static async getFiltroByIdArea(idArea) {
    //     return await dbPostgres.query(`
    //     SELECT
    //         plaza.id,
    //         plaza."idPadre",
    //         CASE WHEN empleado.id IS NOT NULL THEN  precandidato.nombre || ' ' || precandidato."apellidoPaterno" ELSE NULL END as nombre,
    //         plaza."idEstadoPlaza",
    //         plaza."idSede",
    //         sede.nombre AS sede,
    //         plaza."idArea",
    //         area.nombre AS area,
    //         departamento."idSegmento",
    //         segmento.segmento,
    //         plaza."idDepartamento",
    //         departamento.descripcion AS departamento,
    //         plaza."idPuesto",
    //         puesto.nombre AS puesto,
    //         plaza."puestoEspecifico",
    //         plaza."idTipoPlaza",
    //         "tipoPlaza".nombre AS "tipoPlaza",
    //         plaza."idEmpresaPagadora",
    //         empresas.nombre AS "empresaPagadora",
    //         plaza."idEmpresaEmpleadora",
    //         es.nombre AS "empresaEmpleadora",
    //         plaza."idTurnoEmpleado",
    //         "turnoEmpleado".turno,
    //         "turnoEmpleado".horario,
    //         plaza."sueldoDiario",
    //         plaza."sueldoMensual",
    //         plaza."idKpi",
    //         kpi.cantidad,
    //         kpi."idPeriodicidad",
    //         "periodicidadKpi".descripcion
    //     FROM
    //         "recursosHumanos".plaza
    //         INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento.id
    //         INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto.id
    //         INNER JOIN "recursosHumanos".sede ON plaza."idSede" = sede.id
    //         INNER JOIN "recursosHumanos".area ON plaza."idArea" = area.id
    //         LEFT JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
    //         LEFT JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
    //         LEFT JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
    //         INNER JOIN "recursosHumanos"."estadoPlaza" ON "estadoPlaza".id  = plaza."idEstadoPlaza"
    //         INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
    //         INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
    //         INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
    //         LEFT JOIN "recursosHumanos".empresas AS es ON es.id = plaza."idEmpresaEmpleadora"
    //         INNER JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
    //         INNER JOIN "recursosHumanos".kpi ON kpi.id = plaza."idKpi"
    //         INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi".id
    //     WHERE plaza.activo = 1
    //         AND plaza."idArea" = ${idArea} OR plaza."idPadre" IS NULL
    //     `, {
    //         type: QueryTypes.SELECT
    //     })
    // }
    static async getFiltroByIdArea(idArea) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza."idPadre" AS "parentId"
        FROM
            "recursosHumanos".plaza
            INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento.id
            INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto.id
            INNER JOIN "recursosHumanos".sede ON plaza."idSede" = sede.id
            INNER JOIN "recursosHumanos".area ON plaza."idArea" = area.id
            LEFT JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
            LEFT JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
            LEFT JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
            INNER JOIN "recursosHumanos"."estadoPlaza" ON "estadoPlaza".id  = plaza."idEstadoPlaza"
            INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
            INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
            INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
            LEFT JOIN "recursosHumanos".empresas AS es ON es.id = plaza."idEmpresaEmpleadora"
            INNER JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
            INNER JOIN "recursosHumanos".kpi ON kpi.id = plaza."idKpi"
            INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi".id
        WHERE
            area.id = ${idArea}
        `, {
            type: QueryTypes.SELECT
        })
    }
    static async getFiltroByIdAusente(id) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza."idPadre" AS "parentId"
        FROM
            "recursosHumanos".plaza
            INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento.id
            INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto.id
            INNER JOIN "recursosHumanos".sede ON plaza."idSede" = sede.id
            INNER JOIN "recursosHumanos".area ON plaza."idArea" = area.id
            LEFT JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
            LEFT JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
            LEFT JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
            INNER JOIN "recursosHumanos"."estadoPlaza" ON "estadoPlaza".id  = plaza."idEstadoPlaza"
            INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
            INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
            INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
            LEFT JOIN "recursosHumanos".empresas AS es ON es.id = plaza."idEmpresaEmpleadora"
            INNER JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
            INNER JOIN "recursosHumanos".kpi ON kpi.id = plaza."idKpi"
            INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi".id
        WHERE
            plaza.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain:true
        })
    }
    static async getOrganigrama(ids) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza."idPadre" AS "parentId",
            CASE WHEN empleado.id IS NOT NULL THEN  precandidato.nombre || ' ' || precandidato."apellidoPaterno" ELSE NULL END as nombre,
            plaza."idEstadoPlaza",
            plaza."idSede",
            sede.nombre AS sede,
            plaza."idArea",
            area.nombre AS area,
            departamento."idSegmento",
            segmento.segmento,
            plaza."idDepartamento",
            departamento.descripcion AS departamento,
            plaza."idPuesto",
            puesto.nombre AS puesto,
            plaza."puestoEspecifico",
            plaza."idTipoPlaza",
            "tipoPlaza".nombre AS "tipoPlaza",
            plaza."idEmpresaPagadora",
            empresas.nombre AS "empresaPagadora",
            plaza."idEmpresaEmpleadora",
            es.nombre AS "empresaEmpleadora",
            plaza."idTurnoEmpleado",
            "turnoEmpleado".turno,
            "turnoEmpleado".horario,
            plaza."sueldoDiario",
            plaza."sueldoMensual",
            plaza."idKpi",
            kpi.cantidad,
            kpi."idPeriodicidad",
            "periodicidadKpi".descripcion
        FROM
            "recursosHumanos".plaza
        INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento.id
        INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto.id
        INNER JOIN "recursosHumanos".sede ON plaza."idSede" = sede.id
        INNER JOIN "recursosHumanos".area ON plaza."idArea" = area.id
        LEFT JOIN "recursosHumanos".empleado ON plaza.id = empleado."idPlaza"
        LEFT JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato.id
        LEFT JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato.id
        INNER JOIN "recursosHumanos"."estadoPlaza" ON "estadoPlaza".id  = plaza."idEstadoPlaza"
        INNER JOIN "recursosHumanos".segmento ON segmento.id = departamento."idSegmento"
        INNER JOIN "recursosHumanos"."tipoPlaza" ON "tipoPlaza".id = plaza."idTipoPlaza"
        INNER JOIN "recursosHumanos".empresas ON empresas.id = plaza."idEmpresaPagadora"
        LEFT JOIN "recursosHumanos".empresas AS es ON es.id = plaza."idEmpresaEmpleadora"
        LEFT JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
        INNER JOIN "recursosHumanos".kpi ON kpi.id = plaza."idKpi"
        INNER JOIN "recursosHumanos"."periodicidadKpi" ON kpi."idPeriodicidad" = "periodicidadKpi".id
        WHERE plaza."idEstadoPlaza" <> 7 and plaza.id in (${ids})
        ORDER BY ID asc
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async getPlazasHijo(id) {
        return await dbPostgres.query(`
        SELECT empleado.id
        FROM "recursosHumanos".empleado
        JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
        WHERE plaza."idPadre" = ${id}`, {
            type: QueryTypes.SELECT
        })
    }

    static async getByIdEmpleado(idEmpleado) {
        return await dbPostgres.query(`
        SELECT
            plaza.id,
            plaza."idDepartamento",
            plaza."idEstadoPlaza"
        FROM
            "recursosHumanos".plaza
            INNER JOIN "recursosHumanos".empleado ON empleado."idPlaza" = plaza."id"
        WHERE empleado.id = ${idEmpleado}`,
            {
                type: QueryTypes.SELECT,
                plain: true
            })
    }
}
