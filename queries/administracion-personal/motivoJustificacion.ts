import MotivoJustificacion from "../../models/administracion-personal/motivoJustificacion";

export default class MotivoJustificacionService {
    static async create(data) {
        return await MotivoJustificacion.create({...data});
    }

    static async getAll() {
        return await MotivoJustificacion.findAll({
            where: {activo: 1}
        });
    }

    static async updateMotivoJustificacion(idMotivoJustificacion: number, data) {
        return await MotivoJustificacion.update({ ...data }, {
            where: {
                id: idMotivoJustificacion
            }
        });
    }

    static async getMotivoJustificacionById(id: number) {
        return await MotivoJustificacion.findOne({
            where: { id }
        });
    }

    static async getByEstadoAsistencia(id: number) {
        return await MotivoJustificacion.findAll({
            where: { idEstadoAsistencia: id }
        });
    }
}
