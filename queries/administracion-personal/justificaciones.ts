import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import Justificaciones from "../../models/administracion-personal/justificaciones";

const { Op } = require("sequelize");
export default class JustificacionesService {
    static async create(data) {
        return await Justificaciones.create(data);
    }

    static async get() {
        return await Justificaciones.findAll();
    }

    // Si tienen permiso es un supervisor, si no es un ejecutivo
    static async getByEstadoAdp(idEstadoJustificaciones: number) {
        const query = idEstadoJustificaciones === 3 ? ' justificaciones."idEstadoJustificacion" = 3' : 'justificaciones."idEstadoJustificacion" != 3';
        return await dbPostgres.query(`
            SELECT
            justificaciones."id",
            justificaciones."idEmpleado",
            precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombre,
            justificaciones."idEmpleadoSolicitante",
            "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "nombreSolicitante",
            justificaciones."idEmpleadoAutorizo",
            "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" AS "nombreAutorizo",
            justificaciones."fechaRegistro",
            justificaciones."fechaSolicitud",
            justificaciones.documento,
            justificaciones."idEstadoJustificacion",
            "estadoJustificacion".descripcion,
            justificaciones."comentariosAdp",
            justificaciones.comentarios,
            justificaciones."idMotivoJustificacion",
            "motivoJustificacion".motivo,
            justificaciones.folio,
            justificaciones."idTipoJustificacion",
            "tipoJustificacion".tipo,
            justificaciones."folioUnico"
            FROM
            "recursosHumanos".justificaciones
            INNER JOIN "recursosHumanos".empleado ON justificaciones."idEmpleado" = empleado."id"
            INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
            LEFT JOIN "recursosHumanos".empleado AS "empleadoSolicitante" ON justificaciones."idEmpleadoSolicitante" = "empleadoSolicitante"."id"
            LEFT JOIN "recursosHumanos".candidato AS "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante"."id"
            LEFT JOIN "recursosHumanos".precandidato AS "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante"."id"
            LEFT JOIN "recursosHumanos".empleado AS "empleadoAutorizo" ON justificaciones."idEmpleadoAutorizo" = "empleadoAutorizo"."id"
            LEFT JOIN "recursosHumanos".candidato AS "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
            LEFT JOIN "recursosHumanos".precandidato AS "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
            INNER JOIN "recursosHumanos"."estadoJustificacion" ON justificaciones."idEstadoJustificacion" = "estadoJustificacion"."id"
            INNER JOIN "recursosHumanos"."motivoJustificacion" ON justificaciones."idMotivoJustificacion" = "motivoJustificacion"."id"
            INNER JOIN "recursosHumanos"."tipoJustificacion" ON justificaciones."idTipoJustificacion" = "tipoJustificacion"."id"
            AND ${query}
       `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getByMismoFolio(id, folio) {
        return await dbPostgres.query(`
            SELECT
            justificaciones."id",
            justificaciones."idEmpleado",
            precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombre,
            justificaciones."idEmpleadoSolicitante",
            "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "nombreSolicitante",
            justificaciones."idEmpleadoAutorizo",
            "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" AS "nombreAutorizo",
            justificaciones."fechaRegistro",
            justificaciones."fechaSolicitud",
            justificaciones.documento,
            justificaciones."idEstadoJustificacion",
            "estadoJustificacion".descripcion,
            justificaciones."comentariosAdp",
            justificaciones.comentarios,
            justificaciones."idMotivoJustificacion",
            "motivoJustificacion".motivo,
            justificaciones.folio,
            justificaciones."idTipoJustificacion",
            "tipoJustificacion".tipo,
            justificaciones."folioUnico"
            FROM
            "recursosHumanos".justificaciones
            INNER JOIN "recursosHumanos".empleado ON justificaciones."idEmpleado" = empleado."id"
            INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
            LEFT JOIN "recursosHumanos".empleado AS "empleadoSolicitante" ON justificaciones."idEmpleadoSolicitante" = "empleadoSolicitante"."id"
            LEFT JOIN "recursosHumanos".candidato AS "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante"."id"
            LEFT JOIN "recursosHumanos".precandidato AS "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante"."id"
            LEFT JOIN "recursosHumanos".empleado AS "empleadoAutorizo" ON justificaciones."idEmpleadoAutorizo" = "empleadoAutorizo"."id"
            LEFT JOIN "recursosHumanos".candidato AS "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
            LEFT JOIN "recursosHumanos".precandidato AS "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
            INNER JOIN "recursosHumanos"."estadoJustificacion" ON justificaciones."idEstadoJustificacion" = "estadoJustificacion"."id"
            INNER JOIN "recursosHumanos"."motivoJustificacion" ON justificaciones."idMotivoJustificacion" = "motivoJustificacion"."id"
            INNER JOIN "recursosHumanos"."tipoJustificacion" ON justificaciones."idTipoJustificacion" = "tipoJustificacion"."id"
            WHERE folio = '${folio}' AND justificaciones.id != ${id} LIMIT 1
       `, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }

    static async getByEstado(idEmpleado: number, idEstadoJustificaciones: number, permiso) {
        // const query = idEstadoJustificaciones === 3 ? { idEstadoJustificacion: 3} : { [Op.not]: [{ idEstadoJustificacion: 3 }] };
        // const queryEmp = permiso === 1 ? {idEmpleadoSolicitante: idEmpleado} : {idEmpleado}
        // return await Justificaciones.findAll({
        //     where: { ...query, ...queryEmp }
        // });
        let query = idEstadoJustificaciones === 3 ? ' justificaciones."idEstadoJustificacion" = 3' : 'justificaciones."idEstadoJustificacion" != 3';
        query += permiso === 1 ? ' AND justificaciones."idEmpleadoSolicitante" = ' : ' AND justificaciones."idEmpleado" = ';
        return await dbPostgres.query(`
            SELECT
                justificaciones."id",
                justificaciones."idEmpleado",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS nombre,
                justificaciones."idEmpleadoSolicitante",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "nombreSolicitante",
                justificaciones."idEmpleadoAutorizo",
                "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" AS "nombreAutorizo",
                justificaciones."fechaRegistro",
                justificaciones.documento,
                justificaciones."idEstadoJustificacion",
                "estadoJustificacion".descripcion,
                justificaciones."comentariosAdp",
                justificaciones.comentarios,
                justificaciones."idMotivoJustificacion",
                "motivoJustificacion".motivo,
                justificaciones."fechaInicio",
                justificaciones."fechaFin",
                area."nombre" area,
                departamento.descripcion departamento
            FROM
                "recursosHumanos".justificaciones
                INNER JOIN "recursosHumanos".empleado ON justificaciones."idEmpleado" = empleado."id"
                INNER JOIN "recursosHumanos".candidato ON empleado."idCandidato" = candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON candidato."idPrecandidato" = precandidato."id"
                LEFT JOIN "recursosHumanos".empleado AS "empleadoSolicitante" ON justificaciones."idEmpleadoSolicitante" = "empleadoSolicitante"."id"
                INNER JOIN "recursosHumanos".plaza ON  "empleadoSolicitante"."idPlaza" = plaza."id"
                INNER JOIN "recursosHumanos".area ON plaza."idArea" = area."id"
                INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento."id"
                LEFT JOIN "recursosHumanos".candidato AS "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante"."id"
                LEFT JOIN "recursosHumanos".precandidato AS "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante"."id"
                LEFT JOIN "recursosHumanos".empleado AS "empleadoAutorizo" ON justificaciones."idEmpleadoAutorizo" = "empleadoAutorizo"."id"
                LEFT JOIN "recursosHumanos".candidato AS "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
                LEFT JOIN "recursosHumanos".precandidato AS "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
                INNER JOIN "recursosHumanos"."estadoJustificacion" ON justificaciones."idEstadoJustificacion" = "estadoJustificacion"."id"
                INNER JOIN "recursosHumanos"."motivoJustificacion" ON justificaciones."idMotivoJustificacion" = "motivoJustificacion"."id"
                AND ${query}${idEmpleado}
            ORDER by ID DESC
       `, {
            type: QueryTypes.SELECT,
        })
    }

    static async updateEstatus( id: number, data: any) {
        return await Justificaciones.update({...data}, { where: { id } });
    }

    static async update(idJustificaciones: number, data: Justificaciones) {
        return await Justificaciones.update({ ...data }, {
            where: {
                id: idJustificaciones
            }
        });
    }

    static async getJustificacionesById(id: number) {
        return await Justificaciones.findOne({
            where: { id }
        });
    }

    static async folioUnico(folio) {
        return await dbPostgres.query(`
        SELECT id FROM "recursosHumanos".justificaciones WhERE folio = 'AAD82JDJSA' LIMIT 1`, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }
}
