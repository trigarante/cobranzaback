import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import VacacionesModel from "../../../models/administracion-personal/vacaciones/VacacionesModel";

export default class SolicitudVacacionesService {
    static async getPasadas(idEmpleado, fechaInicio, fechaFinal) {
        return (await dbPostgres.query(`
            SELECT
        *
        FROM
        "recursosHumanos"."solicitudVacaciones"
        WHERE

        ("fechaInicio" between Date('${fechaInicio}') and Date('${fechaFinal}') or
        "fechaFinal" between Date('${fechaInicio}') and Date('${fechaFinal}'))  and
        "idEmpleado"=${idEmpleado} and
        "idEstadoVacacion" in (1,2)
        `,
            {
                type: QueryTypes.SELECT,
                // plain: true,
            }));
    }
    // NEW
    static async getCantidadVacacionesByIdEmpleado(idEmpleado) {
        return (await dbPostgres.query(`
           SELECT
            empleado."id",
            precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" as "nombreEmpleado",
            empleado.anios,
            empleado."cantidadVacaciones",
            area.nombre area,
            empleado."fechaIngreso",
            departamento.descripcion departamento,
            departamento.id as "idDepartamento",
            "asistenciaSabado"."idAsistenciaFinSemana",
            puesto.nombre puesto,
            "esquemaVacaciones".json,
            CASE WHEN "asistenciaSabado".id IS NOT NULL AND "asistenciaSabado".activo = 1 THEN "asistenciaFinSemana".id ELSE 0 END AS "trabajaSabado"
            FROM "recursosHumanos".empleado
            INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
            INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza."id"
            INNER JOIN "recursosHumanos".area ON "recursosHumanos".plaza."idArea" = "recursosHumanos".area."id"
            INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
            INNER JOIN "recursosHumanos".puesto ON "recursosHumanos".plaza."idPuesto" = "recursosHumanos".puesto."id"
            INNER JOIN "recursosHumanos"."esquemaVacaciones" ON "recursosHumanos".plaza."idEsquema" = "recursosHumanos"."esquemaVacaciones"."id"
            LEFT JOIN "recursosHumanos"."asistenciaSabado" ON "asistenciaSabado"."idDepartamento" = plaza."idDepartamento"
            LEFT JOIN "recursosHumanos"."asistenciaFinSemana" ON "asistenciaSabado"."idAsistenciaFinSemana" = "asistenciaFinSemana"."id"
            where empleado.id = ${idEmpleado}
            -- and
            -- empleado."cantidadVacaciones" > 0
            ORDER BY
            empleado.id DESC

        `, {
            type: QueryTypes.SELECT,
            plain: true
        }));
    }
    static async byIdEmpleado(idEmpleado) {
        return (await dbPostgres.query(`
            SELECT
                "solicitudVacaciones"."id",
                "solicitudVacaciones"."idEmpleado",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" nombre,
                "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" "nombreAutorizo",
                "solicitudVacaciones"."idEmpleadoSupervisor",
                "estadoSolicitudVacaciones".descripcion,
                "solicitudVacaciones"."fechaRegistro",
                "solicitudVacaciones"."fechaInicio",
                "solicitudVacaciones"."fechaFinal",
                "solicitudVacaciones"."cantidadDias",
                "solicitudVacaciones".comentarios,
                "solicitudVacaciones"."comentariosADP",
                "solicitudVacaciones"."idEstadoVacacion"
            FROM
                "recursosHumanos"."solicitudVacaciones"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos"."solicitudVacaciones"."idEmpleado" = "recursosHumanos".empleado."id"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
                LEFT JOIN "recursosHumanos".empleado "empleadoAutorizo" ON "recursosHumanos"."solicitudVacaciones"."idEmpleadoSupervisor" = "empleadoAutorizo"."id"
                LEFT JOIN "recursosHumanos".candidato "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
                INNER JOIN "recursosHumanos"."estadoSolicitudVacaciones" ON "recursosHumanos"."solicitudVacaciones"."idEstadoVacacion" = "recursosHumanos"."estadoSolicitudVacaciones"."id"
            WHERE
                "idEmpleado" = ${idEmpleado}
            ORDER BY
                ID DESC
        `,
            {
                type: QueryTypes.SELECT
            }));
    }

    static async byDepartamentos(idEstadoVacacion: number, idEmpleado: number, aux) {
        const query = idEstadoVacacion === 2 ? ' "solicitudVacaciones"."idEstadoVacacion" = 2 AND "solicitudVacaciones"."fechaInicio" > NOW()' :
            idEstadoVacacion === 1 ? `empleado.id = ${idEmpleado}`  : `empleado.id IN (${aux})`;

        return (await dbPostgres.query(`
            SELECT
                "solicitudVacaciones"."id",
                "solicitudVacaciones"."idEmpleado",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" nombre,
                "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" "nombreAutorizo",
                "solicitudVacaciones"."idEmpleadoSupervisor",
                "estadoSolicitudVacaciones".descripcion,
                "solicitudVacaciones"."fechaRegistro",
                "solicitudVacaciones"."fechaInicio",
                "solicitudVacaciones"."fechaFinal",
                "solicitudVacaciones"."cantidadDias",
                "solicitudVacaciones".comentarios,
                "solicitudVacaciones"."comentariosADP",
                "solicitudVacaciones"."idEstadoVacacion"
            FROM
                "recursosHumanos"."solicitudVacaciones"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos"."solicitudVacaciones"."idEmpleado" = "recursosHumanos".empleado."id"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
                LEFT JOIN "recursosHumanos".empleado "empleadoAutorizo" ON "recursosHumanos"."solicitudVacaciones"."idEmpleadoSupervisor" = "empleadoAutorizo"."id"
                LEFT JOIN "recursosHumanos".candidato "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
                INNER JOIN "recursosHumanos"."estadoSolicitudVacaciones" ON "recursosHumanos"."solicitudVacaciones"."idEstadoVacacion" = "recursosHumanos"."estadoSolicitudVacaciones"."id"
                INNER JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
                AND ${query}
            ORDER BY
                ID DESC
        `,
            {
                type: QueryTypes.SELECT
            }));
    }
    static async byDepartamentos2(idEstadoVacacion: number, idEmpleado: number, aux) {
        const query = idEstadoVacacion === 2 ? ' "solicitudVacaciones"."idEstadoVacacion" = 2 AND "solicitudVacaciones"."fechaInicio" > NOW()' :
            idEstadoVacacion === 1 ? `empleado.id = ${idEmpleado}`  : `empleado.id IN (${aux})`;

        return (await dbPostgres.query(`
            SELECT
                "solicitudVacaciones"."id",
                "solicitudVacaciones"."idEmpleado",
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" nombre,
                "precandidatoAutorizo".nombre || ' ' || "precandidatoAutorizo"."apellidoPaterno" || ' ' || "precandidatoAutorizo"."apellidoMaterno" "nombreAutorizo",
                "solicitudVacaciones"."idEmpleadoSupervisor",
                "estadoSolicitudVacaciones".descripcion,
                "solicitudVacaciones"."fechaRegistro",
                "solicitudVacaciones"."fechaInicio",
                "solicitudVacaciones"."fechaFinal",
                "solicitudVacaciones"."cantidadDias",
                "solicitudVacaciones".comentarios,
                "solicitudVacaciones"."comentariosADP",
                "solicitudVacaciones"."comentariosEjecutivo",
                "solicitudVacaciones"."idEstadoVacacion",
                "recursosHumanos".area.nombre AS area,
                "recursosHumanos".departamento.descripcion AS departamento,
                "recursosHumanos"."turnoEmpleado".turno AS "turnoEmpleado"
            FROM
                "recursosHumanos"."solicitudVacaciones"
                INNER JOIN "recursosHumanos".empleado ON "recursosHumanos"."solicitudVacaciones"."idEmpleado" = "recursosHumanos".empleado."id"
                INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
                INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
                LEFT JOIN "recursosHumanos".empleado "empleadoAutorizo" ON "recursosHumanos"."solicitudVacaciones"."idEmpleadoSupervisor" = "empleadoAutorizo"."id"
                LEFT JOIN "recursosHumanos".candidato "candidatoAutorizo" ON "empleadoAutorizo"."idCandidato" = "candidatoAutorizo"."id"
                LEFT JOIN "recursosHumanos".precandidato "precandidatoAutorizo" ON "candidatoAutorizo"."idPrecandidato" = "precandidatoAutorizo"."id"
                INNER JOIN "recursosHumanos"."estadoSolicitudVacaciones" ON "recursosHumanos"."solicitudVacaciones"."idEstadoVacacion" = "recursosHumanos"."estadoSolicitudVacaciones"."id"
                INNER JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
                INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
                INNER JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area."id"
                INNER JOIN "recursosHumanos"."turnoEmpleado" ON plaza."idTurnoEmpleado" = "turnoEmpleado".id
                AND ${query}
            ORDER BY
                ID DESC
        `,
            {
                type: QueryTypes.SELECT
            }));
    }



    static async create(data) {
        return await VacacionesModel.create({...data});
    }

    static async updateSolicitudVacaciones(id, data) {
        return await VacacionesModel.update({ ...data }, {
            where: {
                id
            }
        });
    }

    static async getById(id: number) {
        return await VacacionesModel.findOne({
            where: { id },
            raw: true
        });
    }

    // FIN NEW
    //
    // static async getAll() {
    //     return await SolicitudVacaciones.findAll();
    // }
    //
    // static async getCompleto() {
    //     return (await db.query(`
    //         SELECT
    //               precandidato.id AS idPrecandidato,
    //               precandidato.nombre,
    //               precandidato.apellidoPaterno,
    //               precandidato.apellidoMaterno,
    //               precandidato.idProspectoRRHH,
    //               empleado.idCantidadVacaciones,
    //               solicitudVacaciones.id AS idSolicitudVacaciones,
    //               solicitudVacaciones.idEmpleado,
    //               solicitudVacaciones.idFechaSolicitud,
    //               solicitudVacaciones.solicitud,
    //               solicitudVacaciones.idEstadoSolicitudVacaciones,
    //               solicitudVacaciones.idEmpleadoAutorizado,
    //               estadoSolicitudVacaciones.descripcion AS estadoSolicitudVacaciones
    //         FROM
    //               precandidato
    //               INNER JOIN candidato ON precandidato.id = candidato.idPrecandidato
    //               INNER JOIN empleado ON candidato.id = empleado.idCandidato
    //               INNER JOIN solicitudVacaciones ON empleado.id = solicitudVacaciones.idEmpleado
    //               INNER JOIN estadoSolicitudVacaciones ON solicitudVacaciones.idEstadoSolicitudVacaciones = estadoSolicitudVacaciones.id
    //         WHERE
    //               solicitudVacaciones.idEstadoSolicitudVacaciones = 1
    //
    //     `))[0];
    // }
    //
    // static async getCompletoAutorizar() {
    //     return (await db.query(`
    //         SELECT
    //               precandidato.id AS idPrecandidato,
    //               precandidato.nombre,
    //               precandidato.apellidoPaterno,
    //               precandidato.apellidoMaterno,
    //               precandidato.idProspectoRRHH,
    //               empleado.idCantidadVacaciones,
    //               cantidadVacaciones.cantidadDias AS diasVacaciones,
    //               solicitudVacaciones.id AS idSolicitudVacaciones,
    //               solicitudVacaciones.idEmpleado,
    //               solicitudVacaciones.idFechaSolicitud,
    //               solicitudVacaciones.solicitud,
    //               solicitudVacaciones.idEstadoSolicitudVacaciones,
    //               solicitudVacaciones.idEmpleadoAutorizado,
    //               estadoSolicitudVacaciones.descripcion AS estadoSolicitudVacaciones
    //         FROM
    //               precandidato
    //               INNER JOIN candidato ON precandidato.id = candidato.idPrecandidato
    //               INNER JOIN empleado ON candidato.id = empleado.idCandidato
    //               INNER JOIN solicitudVacaciones ON empleado.id = solicitudVacaciones.idEmpleado
    //               INNER JOIN estadoSolicitudVacaciones ON solicitudVacaciones.idEstadoSolicitudVacaciones = estadoSolicitudVacaciones.id
    //               INNER JOIN cantidadVacaciones ON empleado.idCantidadVacaciones = cantidadVacaciones.id
    //         WHERE
    //               solicitudVacaciones.idEstadoSolicitudVacaciones = 1
    //
    //     `))[0];
    // }

}
