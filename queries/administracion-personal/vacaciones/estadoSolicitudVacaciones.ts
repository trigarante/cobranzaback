import EstadoSolicitudVacaciones from "../../../models/administracion-personal/estadoSolicitudVacaciones";

export default class EstadoSolicitudVacacionesService {
    static async create(data: EstadoSolicitudVacaciones) {
        return await EstadoSolicitudVacaciones.create({...data});
    }

    static async getAll() {
        return await EstadoSolicitudVacaciones.findAll();
    }

    static async updateEstadoSolicitudVacaciones(idEstadoSolicitudVacaciones: number, data: EstadoSolicitudVacaciones) {
        return await EstadoSolicitudVacaciones.update({ ...data }, {
            where: {
                id: idEstadoSolicitudVacaciones
            }
        });
    }

    static async getEstadoSolicitudVacacionesById(id: number) {
        return await EstadoSolicitudVacaciones.findOne({
            where: { id }
        });
    }
}
