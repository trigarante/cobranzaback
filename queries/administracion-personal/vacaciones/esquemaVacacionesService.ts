import EsquemaVacacionesModel from "../../../models/administracion-personal/vacaciones/esquemaVacacionesModel";

export default class EsquemaVacacionesService {
    static async getActivos() {
        return await EsquemaVacacionesModel.findAll({
            where: {activo: 1}
        })
    }
}
