import solicitudesIVRModel from "../../models/venta-nueva/solicitudesIVR/solicitudesIVRModel";

export default class SolicitudesIVRQueries {
    static async getAll(): Promise<any> {
        return await solicitudesIVRModel.findAll()
    }
}
