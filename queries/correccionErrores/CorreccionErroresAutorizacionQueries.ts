import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class CorreccionErroresAutorizacionQueries {

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario"
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

// AutorizacionerroresAnexosAView
    static async getAll(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAnexosA" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 2
                 OR
                 ar."verificadoProducto" = 2
                 OR
                 ar."verificadoRegistro" = 2
                 OR
                 ar."verificadoPago" = 2
                 OR
                 ar."verificadoInspeccion" = 2
                )
                AND
                rb.numero =1
                AND
                ar."idEstadoVerificacion" IN (1,2)
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AutorizacionerroresAnexosAView
    static async getAllByDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAnexosA" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN  "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 2
                 OR
                 ar."verificadoProducto" = 2
                 OR
                 ar."verificadoRegistro" = 2
                 OR
                 ar."verificadoPago" = 2
                 OR
                 ar."verificadoInspeccion" = 2
                )
                AND
                rb.numero =1
                AND
                ar."idEstadoVerificacion" IN (1,2)
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AutorizacionerroresAnexosAView
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAnexosA" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 2
                 OR
                 ar."verificadoProducto" = 2
                 OR
                 ar."verificadoRegistro" = 2
                 OR
                 ar."verificadoPago" = 2
                 OR
                 ar."verificadoInspeccion" = 2
                )
                AND
                rb.numero =1
                AND
                ar."idEstadoVerificacion" IN (1,2)
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                pg."idEmpleado" = ${idEmpleado}
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // AutorizacionerroresView
    static async getAllDatos(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAutorizacion",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAutorizacion" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 5
                 OR
                 ar."verificadoProducto" = 5
                 OR
                 ar."verificadoRegistro" = 5
                 OR
                 ar."verificadoPago" = 5
                 OR
                 ar."verificadoInspeccion" = 5
                )
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AutorizacionerroresView
    static async getAllByDepartamentosDatos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAutorizacion",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAutorizacion" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 5
                 OR
                 ar."verificadoProducto" = 5
                 OR
                 ar."verificadoRegistro" = 5
                 OR
                 ar."verificadoPago" = 5
                 OR
                 ar."verificadoInspeccion" = 5
                )
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // AutorizacionerroresView
    static async getAllByDepartamentoAndIdEmpleadoDatos(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (ar."id")
                ar.id,
                ar."idRegistro",
                ar."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                ar.numero,
                ar."verificadoCliente",
                ar."verificadoProducto",
                ar."verificadoRegistro",
                ar."verificadoPago",
                ar."verificadoInspeccion",
                ar."fechaCreacion",
                ea.id AS "idErrorAutorizacion",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                prmc.nombre || ' ' || prmc."apellidoPaterno" || ' ' || prmc."apellidoMaterno" as "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."autorizacionRegistro" ar
                JOIN operaciones."erroresAutorizacion" ea ON ea."idAutorizacionRegistro" = ar.id
                JOIN operaciones.registro r ON ar."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                LEFT JOIN "recursosHumanos".empleado emc ON ea."idEmpleado" = emc.id
                LEFT JOIN "recursosHumanos".candidato cmc ON emc."idCandidato" = cmc.id
                LEFT JOIN "recursosHumanos".precandidato prmc ON cmc."idPrecandidato" = prmc.id
                JOIN operaciones."estadoVerificacion" ev ON ar."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (ar."verificadoCliente" = 5
                 OR
                 ar."verificadoProducto" = 5
                 OR
                 ar."verificadoRegistro" = 5
                 OR
                 ar."verificadoPago" = 5
                 OR
                 ar."verificadoInspeccion" = 5
                )
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" = 4
                AND
                rb."fechaCierre" IS null
                AND
                ar."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                pg."idEmpleado" = ${idEmpleado}
                ORDER by ar.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
}
