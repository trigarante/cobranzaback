import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class CorreccionErroresVerificacionQueries {

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario"
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

// VerificacionErroresVNAView
    static async getAll(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
           SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllENR(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
           SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresVNAView
    static async getAllSubs(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
           SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
// VerificacionErroresVNAView
    static async getAllByDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
               DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresVNAView
    static async getAllByDepartamentosSubs(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
               DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentosENR(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
               DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // VerificacionErroresVNAView
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresVNAView
    static async getAllByDepartamentoAndIdEmpleadoSubs(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentoAndIdEmpleadoENR(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                ea.id AS "idErrorAnexo",
                ea."idEstadoCorreccion",
                ea."idEmpleadoCorreccion",
                ea."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" as nombre,
                "precandidatoMesa".nombre || ' ' || "precandidatoMesa"."apellidoPaterno" || ' '  || "precandidatoMesa"."apellidoMaterno" AS "nombreMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre || ' ' || cl.paterno ||  ' ' || cl.materno AS "nombreCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresAnexosV" ea ON ea."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON ea."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 2
                OR
                vr."verificadoProducto" = 2
                OR
                vr."verificadoRegistro" = 2
                OR
                vr."verificadoPago" = 2
                OR
                vr."verificadoInspeccion" = 2
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                ea."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllDatosENR(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllDatosSubs(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllDatos(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllByDepartamentosDatos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllByDepartamentosDatosSubs(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentosDatosENR(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllByDepartamentoAndIdEmpleadoDatos(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    // VerificacionErroresView
    static async getAllByDepartamentoAndIdEmpleadoDatosSubs(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" in (1, 7, 4, 9)
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentoAndIdEmpleadoDatosENR(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                DISTINCT ON (vr."id")
                vr.id,
                vr."idRegistro",
                vr."idEstadoVerificacion",
                ev.estado AS "estadoVerificacion",
                vr.numero,
                vr."verificadoCliente",
                vr."verificadoProducto",
                vr."verificadoRegistro",
                vr."verificadoPago",
                vr."verificadoInspeccion",
                vr."fechaCreacion",
                erv.id AS "idErrorAutorizacion",
                erv."idEstadoCorreccion",
                erv."idEmpleadoCorreccion",
                erv."fechaCreacion" AS "fechaCreacionError",
                r."idEmpleado",
                r."idDepartamento",
                dp.descripcion,
                s."nombreComercial",
                pr.nombre,
                pr."apellidoPaterno",
                pr."apellidoMaterno",
                "precandidatoMesa".nombre AS "nombreMesa",
                "precandidatoMesa"."apellidoPaterno" AS "apellidoPaternoMesa",
                "precandidatoMesa"."apellidoMaterno" AS "apellidoMaternoMesa",
                r.poliza,
                cl.id AS "idCliente",
                cl.nombre AS "nombreCliente",
                cl.paterno AS "apellidoPaternoCliente",
                cl.materno AS "apellidoMaternoCliente",
                pc.archivo as "carpetaCliente",
                r.archivo AS "carpetaRegistro",
                r."idFlujoPoliza" AS "flujoPoliza",
                pg.id AS idPago,
                pg.archivo AS "archivoPago",
                pg."fechaPago",
                r."fechaInicio"
                FROM operaciones."verificacionRegistro" vr
                JOIN operaciones."erroresVerificacion" erv ON erv."idVerificacionRegistro" = vr.id
                JOIN operaciones.registro r ON vr."idRegistro" = r.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoMesa" ON erv."idEmpleado" = "empleadoMesa".id
                JOIN "recursosHumanos".candidato "candidatoMesa" ON "empleadoMesa"."idCandidato" = "candidatoMesa".id
                JOIN "recursosHumanos".precandidato "precandidatoMesa" ON "candidatoMesa"."idPrecandidato" = "precandidatoMesa".id
                JOIN operaciones."estadoVerificacion" ev ON vr."idEstadoVerificacion" = ev.id
                JOIN "operaciones".recibos rb ON r."id" = rb."idRegistro"
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones.socios s ON r."idSocio" = s.id
                LEFT JOIN "operaciones".pagos pg ON rb.id = pg."idRecibo"
                WHERE
                (vr."verificadoCliente" = 5
                OR
                vr."verificadoProducto" = 5
                OR
                vr."verificadoRegistro" = 5
                OR
                vr."verificadoPago" = 5
                OR
                vr."verificadoInspeccion" = 5
                )
                AND
                vr."verificadoCliente" <> 1
                AND
                vr."verificadoProducto" <> 1
                AND
                vr."verificadoRegistro" <> 1
                AND
                vr."verificadoPago" <> 1
                AND
                rb.numero =1
                AND
                erv."idEstadoCorreccion" = 1
                AND
                r."idEstadoPoliza" <> 9
                AND
                r."idFlujoPoliza" = 58
                AND
                rb."fechaCierre" IS null
                AND
                vr."fechaCreacion" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id = ${idDepartamento}
                AND
                r."idEmpleado" = ${idEmpleado}
                ORDER by vr.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

}
