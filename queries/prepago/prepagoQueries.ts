import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";
import PrepagoModel from "../../models/venta-nueva/prepago/prepagoModel";

export default class PrepagoQueries {
    static async get(query) {
        return await dbPostgres.query(`
          SELECT
            DISTINCT ON (operaciones.registro.id)
            operaciones.registro.id,
            operaciones.recibos."idEmpleado",
            operaciones.registro.poliza,
            operaciones.registro."fechaRegistro",
            "recursosHumanos".precandidato.nombre || ' ' ||
            "recursosHumanos".precandidato."apellidoPaterno" || ' ' ||
            "recursosHumanos".precandidato."apellidoMaterno" AS "nombreEmpleado",
            operaciones.cliente.nombre || ' ' ||
            operaciones.cliente.paterno || ' ' ||
            operaciones.cliente.materno AS "nombreCliente",
            operaciones.socios."nombreComercial",
            operaciones."tipoSubRamo".tipo as descripcion,
            operaciones."prepagoPay".titular,
            operaciones."prepagoPay"."fechaPago",
            operaciones."prepagoPay"."numeroTarjeta",
            operaciones."prepagoPay".csv,
            operaciones."prepagoPay"."mes" as "mesVencimiento",
            operaciones."prepagoPay"."anio" as "anioVencimiento",
            operaciones."msiAhorraPay".nombre as "tipoPago",
            operaciones."metodoPagoAhorraPay".nombre  as "metodoPago",
            operaciones."bancoAhorraPay".nombre as "banco",
            operaciones.socios."alias" as "nombreComercial",
            operaciones."prepagoPay"."tarjetaDigital"
        FROM
            operaciones.registro
        INNER JOIN operaciones.recibos ON registro.id = recibos."idRegistro"
        INNER JOIN operaciones."prepagoPay" ON recibos.id = "prepagoPay"."idRecibo"
        INNER JOIN operaciones."msiAhorraPay" ON "prepagoPay"."idMsiAhorraPay" = "msiAhorraPay".id
        INNER JOIN operaciones."bancoAhorraPay" ON "msiAhorraPay"."idBancoAhorraPay" = "bancoAhorraPay".id
        INNER JOIN operaciones."metodoPagoAhorraPay" ON "bancoAhorraPay"."idMetodoPagoAhorraPay" = "metodoPagoAhorraPay".id
        INNER JOIN operaciones.socios ON "metodoPagoAhorraPay"."idSocio" = socios.id
        INNER JOIN "recursosHumanos".empleado ON recibos."idEmpleado" = "recursosHumanos".empleado.id
        INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
        INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
        INNER JOIN operaciones."productoCliente" ON operaciones.registro."idProducto" = operaciones."productoCliente".id
        INNER JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
        INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productoCliente"."idSubRamo"
        INNER JOIN operaciones."tipoSubRamo" ON operaciones."tipoSubRamo".id = operaciones."subRamo"."idTipoSubRamo"
        LEFT JOIN operaciones.pagos ON recibos.id = pagos."idRecibo"
        WHERE pagos.id IS NULL
        AND operaciones."prepagoPay".titular IS NOT NULL
        ${query}
        `, {
            type: QueryTypes.SELECT
        })
    }
    static async getCobranza(empleado) {
        return await dbPostgres.query(`
        SELECT
            prepago."idRegistro",
            prepago.titular,
            prepago.poliza,
            prepago."fechaCreacion",
            prepago."idRecibo",
            "medioContacto".nombre
        FROM operaciones.prepago
        INNER JOIN operaciones."medioContacto" on prepago."idMedioContacto" = "medioContacto".id
        INNER JOIN operaciones.registro ON prepago."idRegistro" = registro.id
        WHERE prepago."fechaCompra" IS NOT NULL AND "idEmpleado" = ${empleado}
        AND operaciones.registro."idFlujoPoliza" = 4
        ORDER BY prepago."fechaCreacion" DESC`, {
            type: QueryTypes.SELECT
        })
    }
    static async getENR(empleado) {
        return await dbPostgres.query(`
        SELECT
            prepago."idRegistro",
            prepago.titular,
            prepago.poliza,
            prepago."fechaCreacion",
            prepago."idRecibo",
            "medioContacto".nombre
        FROM operaciones.prepago
        INNER JOIN operaciones."medioContacto" on prepago."idMedioContacto" = "medioContacto".id
        INNER JOIN operaciones.registro ON prepago."idRegistro" = registro.id
        WHERE prepago."fechaCompra" IS NOT NULL AND "idEmpleado" = ${empleado}
        AND operaciones.registro."idFlujoPoliza" = 58
        AND operaciones.registro."idEstadoPoliza" <> 9
        ORDER BY prepago."fechaCreacion" DESC`, {
            type: QueryTypes.SELECT
        })
    }
    static async getSubsecuentes(empleado) {
        return await dbPostgres.query(`
        SELECT
            prepago."idRegistro",
            prepago.titular,
            prepago.poliza,
            prepago."fechaCreacion",
            prepago."idRecibo",
            "medioContacto".nombre
        FROM operaciones.prepago
        INNER JOIN operaciones."medioContacto" on prepago."idMedioContacto" = "medioContacto".id
        INNER JOIN operaciones.registro ON prepago."idRegistro" = registro.id
        WHERE prepago."fechaCompra" IS NOT NULL AND "idEmpleado" = ${empleado}
        AND operaciones.registro."idFlujoPoliza" = 7
        ORDER BY prepago."fechaCreacion" DESC`, {
            type: QueryTypes.SELECT
        })
    }

    static async getById(idRegistro) {
        return await PrepagoModel.findOne({
            where: {idRegistro}
        })
    }

    static async getInfoCliente(idRegistro, idRecibo) {
        return await dbPostgres.query(`
        SELECT
            cliente.nombre || ' ' || cliente.paterno || ' ' || cliente.materno nombre,
            registro.poliza
        FROM operaciones.registro
        INNER JOIN operaciones."productoCliente" ON registro."idProducto" = "productoCliente".id
        INNER JOIN operaciones.cliente ON "productoCliente"."idCliente" = cliente.id
        INNER JOIN operaciones.recibos ON registro.id = recibos."idRegistro"
        WHERE
            registro.id = ${idRegistro} and recibos.id = ${idRecibo}`, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async post(data) {
        return PrepagoModel.create(data).then( data => {
            return data['id'];
        }).catch(err => console.log(err))
    }

    static async update(data, id) {
        return PrepagoModel.update(data, {
            where: {id}
        })
    }
}
