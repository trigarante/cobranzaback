import {dbPostgres} from "../../configs/connection";
import usuariosDepartamentoModel from "../../models/venta-nueva/usuariosDepartamento/usuariosDepartamentoModel";
import {QueryTypes} from "sequelize";

export default class UsuariosDepartamentoQueries{

    static async getDepartamentoByIdUsuario(idUsuario){
        return await dbPostgres.query(`
        SELECT ud."idDepartamento", "recursosHumanos".departamento.descripcion
        FROM generales."usuariosDepartamento" ud
        INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = ud."idDepartamento"
        WHERE ud."idUsuario" = ${idUsuario}`,
            {
                type: QueryTypes.SELECT
            })
    }
}
