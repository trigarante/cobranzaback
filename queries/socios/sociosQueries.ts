import SociosModel from "../../models/venta-nueva/socios/sociosModel";

export default class SociosQueries {
    static async getByIdPaisStep(idPais) {
        return await SociosModel.findAll({
            attributes: ['id', 'nombreComercial', 'expresionRegular', 'ejemploExpresionRegular'],
            where: {idPais, activo: 1}
        })
    }
}
