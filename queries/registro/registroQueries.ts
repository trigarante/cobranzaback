import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import RegistroModel from "../../models/venta-nueva/registro/registroModel";
import PagosModel from "../../models/venta-nueva/pagos/pagosModel";
import RegistroOnlineModel from "../../models/venta-nueva/registro/registroOnlineModel";

export default class RegistroQueries {

    static async detallesPoliza(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                registro."id",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaRegistro",
                operaciones.registro."fechaInicio",
                operaciones."formaPago".descripcion AS "tipoPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones."estadoPago".descripcion AS estado,
                operaciones.cliente."telefonoMovil",
                operaciones.cliente.correo,
                operaciones.cliente.genero,
                operaciones.cliente."razonSocial",
                operaciones.cliente."fechaNacimiento",
                operaciones.cliente.cp,
                operaciones.cliente.nombre || ' ' || operaciones.cliente.paterno || ' ' || operaciones.cliente.materno AS "nombreCliente",
                case when operaciones.pagos.id is not null then operaciones."estadoPago".descripcion else 'SIN PAGO' end AS "estadoPago",
                "productoCliente".datos,
                operaciones.socios."nombreComercial" as "aseguradora"
                FROM
                operaciones.registro
                INNER JOIN
                operaciones.recibos
                ON
                operaciones.registro."id" = operaciones.recibos."idRegistro"
                LEFT JOIN
                operaciones.pagos
                ON
                operaciones.recibos."id" = operaciones.pagos."idRecibo"
                LEFT JOIN
                operaciones."formaPago"
                ON
                operaciones.pagos."idFormaPago" = operaciones."formaPago"."id"
                LEFT JOIN
                operaciones."estadoPago"
                ON
                operaciones.pagos."idEstadoPago" = operaciones."estadoPago"."id"
                INNER JOIN
                operaciones."productoCliente"
                ON
                operaciones.registro."idProducto" = operaciones."productoCliente"."id"
                INNER JOIN
                operaciones.cliente
                ON
                operaciones."productoCliente"."idCliente" = operaciones.cliente."id"
                LEFT JOIN
                operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                LEFT JOIN
                operaciones.socios ON operaciones.registro."idSocio" = operaciones.socios."id"
                Where
                registro."id" = ${id} and recibos.numero = 1
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async findAllByIdRegistro(id): Promise<any> {
        return await dbPostgres.query(`
          SELECT
                DISTINCT ON (r.id)
                r.id,
                r."idEmpleado",
                r."idProducto",
                r."idTipoPago",
                r."idProductoSocio",
                r."idFlujoPoliza",
                fp.estado,
                r.poliza,
                r."fechaInicio",
                r.archivo,
                r."idPeriodicidad",
                r."idSocio",
                per.nombre AS periodicidad,
                tp."cantidadPagos",
                tp."tipoPago",
                ps.nombre AS "productoSocio",
                ps."idSubRamo",
                tsr.tipo AS "tipoSubramo",
                sr."idRamo",
                pc.datos,
                case r."idEstadoPoliza" when 9 then 'cancelado' when 10 then 'Por corregir' else pc."noSerie" end AS "numSerie",
                r."idEstadoPoliza",
                ep.estado,
                dp."idArea",
                preca.nombre,
                preca."apellidoPaterno",
                preca."apellidoMaterno",
                pz."idSede",
                pz."idPuesto",
                sed."idEmpresa",
                dp.id AS "idDepartamento",
                dp.descripcion AS departamento,
                pc."idCliente",
                cl."archivoSubido",
                tr.tipo as "tipoRamo",
                so.alias,
                cl."idPais",
                r.oficina,
                pc."idSubRamo" AS "productoClienteSubRamo",
                pc."idSolicitud",
                r."primaNeta"
                FROM operaciones.registro r
                JOIN operaciones."tipoPago" tp ON r."idTipoPago" = tp.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                JOIN operaciones."tipoSubRamo" tsr ON sr."idTipoSubRamo" = tsr.id
                JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                JOIN operaciones."tipoRamo" tr ON ra."idTipoRamo" = tr.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato ca ON e."idCandidato" = ca.id
                JOIN "recursosHumanos".precandidato preca ON ca."idPrecandidato" = preca.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN operaciones."flujoPoliza" fp ON r."idFlujoPoliza" = fp.id
                JOIN operaciones.socios so ON ra."idSocio" = so.id
                JOIN operaciones.periodicidad per ON r."idPeriodicidad" = per.id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN "recursosHumanos".area ar ON dp."idArea" = ar.id
                JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz."id"
                JOIN "recursosHumanos".sede sed ON pz."idSede" = sed.id
                JOIN "recursosHumanos".empresas emp ON sed."idEmpresa" = emp.id
                WHERE
                r."idEstadoPoliza" <> 9 AND r.id = ${id}
                ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })

    }

    static async findAllByIdRegistroOnline(id): Promise<any> {
        return await dbPostgres.query(`
          SELECT
                DISTINCT ON (r.id)
                r.id,
                r."idEmpleado",
                r."idProducto",
                r."idTipoPago",
                r."idProductoSocio",
                r."idFlujoPoliza",
                fp.estado,
                r.poliza,
                r."fechaInicio",
                r.archivo,
                r."idPeriodicidad",
                r."idSocio",
                per.nombre AS periodicidad,
                tp."cantidadPagos",
                tp."tipoPago",
                ps.nombre AS "productoSocio",
                ps."idSubRamo",
                tsr.tipo AS "tipoSubramo",
                sr."idRamo",
                pc.datos,
                case r."idEstadoPoliza" when 9 then 'cancelado' when 10 then 'Por corregir' else pc."noSerie" end AS "numSerie",
                r."idEstadoPoliza",
                ep.estado,
                dp."idArea",
                preca.nombre,
                preca."apellidoPaterno",
                preca."apellidoMaterno",
                pz."idSede",
                pz."idPuesto",
                sed."idEmpresa",
                dp.id AS "idDepartamento",
                dp.descripcion AS departamento,
                pc."idCliente",
                cl."archivoSubido",
                tr.tipo as "tipoRamo",
                so.alias,
                cl."idPais",
                r.oficina,
                pc."idSubRamo" AS "productoClienteSubRamo",
                pc."idSolicitud",
                r."primaNeta"
                FROM operaciones.registro r
                JOIN operaciones."tipoPago" tp ON r."idTipoPago" = tp.id
                JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
                JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                JOIN operaciones."tipoSubRamo" tsr ON sr."idTipoSubRamo" = tsr.id
                JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                JOIN operaciones."tipoRamo" tr ON ra."idTipoRamo" = tr.id
                JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
                JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato ca ON e."idCandidato" = ca.id
                JOIN "recursosHumanos".precandidato preca ON ca."idPrecandidato" = preca.id
                JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                JOIN operaciones."flujoPoliza" fp ON r."idFlujoPoliza" = fp.id
                JOIN operaciones.socios so ON ra."idSocio" = so.id
                JOIN operaciones.periodicidad per ON r."idPeriodicidad" = per.id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN "recursosHumanos".area ar ON dp."idArea" = ar.id
                JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz."id"
                JOIN "recursosHumanos".sede sed ON pz."idSede" = sed.id
                JOIN "recursosHumanos".empresas emp ON sed."idEmpresa" = emp.id
                WHERE
                r."idEstadoPoliza" <> 9 AND r.id = ${id}
                ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })

    }
    static async postOnline(data) {
        return await RegistroOnlineModel.create(data).then(data => {
            return data.id;
        }).catch(err => console.log(err))
    }

    static async updateOnline(data, id) {
        return await RegistroOnlineModel.update(data, {
            where: {id},
        })
    }


    static async findAllByIdRegistroViewer(id): Promise<any> {
        return await dbPostgres.query(`
         SELECT
            DISTINCT ON (recibos.id)
            recibos.id AS "idRecibo",
            registro.id,
            registro.archivo AS "archivoPoliza",
            CASE WHEN "productoCliente".archivo IS NOT NULL THEN "productoCliente".archivo ELSE "cliente".archivo END as "carpetaCliente",
            inspecciones.archivo AS "archivoInspeccion",
            pagos.archivo AS "archivoPago"
            FROM operaciones.registro
            LEFT JOIN operaciones."productoCliente"  ON registro."idProducto"="productoCliente".id
            LEFT JOIN operaciones.cliente  ON "productoCliente"."idCliente"=cliente.id
            LEFT JOIN operaciones.recibos  ON registro.id = recibos."idRegistro"
            LEFT JOIN operaciones.pagos  ON recibos.id = pagos."idRecibo"
            LEFT JOIN operaciones.inspecciones ON registro.id = inspecciones."idRegistro"
            WHERE registro.id = ${id}
            AND recibos.activo = 1
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getPolizaExiste(poliza, idSocio, fecha): Promise<any> {
        return await dbPostgres.query(`
         SELECT * FROM operaciones.registro
        where "idSocio" = ${idSocio}
        and extract(year from "fechaInicio") = extract(year from '${fecha}'::date)
        and extract (MONTH from"fechaInicio") = extract(month from '${fecha}'::date)
        and "idEstadoPoliza" <> 9 and poliza like '${poliza}'
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async post(data) {
        return await RegistroModel.create(data).then(data => {
            return data.id;
        }).catch(err => console.log(err))
    }

    static async update(data, id) {
        return await RegistroModel.update(data, {
            where: {id},
        })
    }
    static async getForRegistroById(id) {
        return await dbPostgres.query(`
        SELECT
        registro.*,
        ramo."id" AS "idRamo",
        "subRamo"."id" AS "idSubRamo"
        FROM
        operaciones.registro
        INNER JOIN operaciones."productosSocio" ON registro."idProductoSocio" = "productosSocio".id
        INNER JOIN operaciones."subRamo" ON "productosSocio"."idSubRamo" = "subRamo"."id"
        INNER JOIN operaciones.ramo ON "subRamo"."idRamo" = ramo."id"
        WHERE registro.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async getAllIdsSteps(id) {
        return await dbPostgres.query(`
        SELECT
        DISTINCT ON (registro."id")
        operaciones.registro."id",
        operaciones."productoCliente"."id" AS "idProducto",
        operaciones."productoCliente"."idCliente",
        operaciones.recibos."id" AS "idRecibo",
        operaciones.pagos."id" AS "idPago",
        operaciones.inspecciones."id" AS "idInspeccion",
        cliente.id as "idCliente"
        FROM
        operaciones.registro
        INNER JOIN
        operaciones."productoCliente"
        ON
        operaciones.registro."idProducto" = operaciones."productoCliente"."id"
        LEFT JOIN
        operaciones.inspecciones
        ON
        operaciones.registro."id" = operaciones.inspecciones."idRegistro"
        INNER JOIN
        operaciones.recibos
        ON
        operaciones.registro."id" = operaciones.recibos."idRegistro"
        LEFT JOIN
        operaciones.pagos
        ON
        operaciones.recibos."id" = operaciones.pagos."idRecibo"
        INNER JOIN operaciones.cliente ON cliente.id = "productoCliente"."idCliente"
        WHERE registro.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    static async updateEstado(idRegistro: number, data): Promise<any> {
        return await PagosModel.update({...data}, {
            where: {
                id: idRegistro,
            }
        });
    }
    static async getDetallesRegistro(idRegistro) {
        return await dbPostgres.query(`
SELECT
                registro."id",
                operaciones.registro.poliza,
                operaciones.registro."primaNeta",
                operaciones.registro."fechaRegistro",
                operaciones.registro."fechaInicio",
                operaciones."tipoPago"."tipoPago" AS "tipoPago",
                operaciones."tipoPago"."cantidadPagos",
                operaciones."estadoPoliza".estado AS estado,
                operaciones.cliente."telefonoMovil",
                operaciones.cliente.correo,
                operaciones.cliente.genero,
                operaciones.cliente."razonSocial",
                operaciones.cliente."fechaNacimiento",
                operaciones.cliente.cp,
                operaciones.cliente.nombre || ' ' || operaciones.cliente.paterno || ' ' || operaciones.cliente.materno AS "nombreCliente",
                case when operaciones.pagos.id is not null then operaciones."estadoPago".descripcion else 'SIN PAGO' end AS "estadoPago",
                "productoCliente".datos
                FROM
                operaciones.registro
                INNER JOIN
                operaciones.recibos
                ON
                operaciones.registro."id" = operaciones.recibos."idRegistro"
                LEFT JOIN
                operaciones.pagos
                ON
                operaciones.recibos."id" = operaciones.pagos."idRecibo"
                LEFT JOIN
                operaciones."formaPago"
                ON
                operaciones.pagos."idFormaPago" = operaciones."formaPago"."id"
                LEFT JOIN
                operaciones."estadoPago"
                ON
                operaciones.pagos."idEstadoPago" = operaciones."estadoPago"."id"
                LEFT JOIN
                operaciones."productoCliente"
                ON
                operaciones.registro."idProducto" = operaciones."productoCliente"."id"
                LEFT JOIN
                operaciones.cliente
                ON
                operaciones."productoCliente"."idCliente" = operaciones.cliente."id"
                LEFT JOIN
                operaciones."tipoPago" ON operaciones."tipoPago".id = operaciones.registro."idTipoPago"
                LEFT JOIN
                operaciones."estadoPoliza" ON operaciones.registro."idEstadoPoliza" = operaciones."estadoPoliza"."id"
                Where
                registro."id" = ${idRegistro}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    static async getRegistroById(id){
        return await RegistroModel.findOne({
            where: {id},
            raw: true
        })
    }
}



