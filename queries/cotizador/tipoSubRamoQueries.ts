import {Op} from "sequelize";
import tipoSubRamoModel from "../../models/venta-nueva/cotizador/tipo-subramo/tipoSubRamoModel";

export default class TipoSubRamoQueries {
    static async getFiltrados() {
        return await tipoSubRamoModel.findAll({
            where: {id: {
                [Op.in]: [1,2,9]
                }}
        })
    }
}
