import ProductosSocioModel from "../../models/venta-nueva/productosSocio/productosSocioModel";
import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class ProductosSocioQueries {
    static async getByIdSubRamo(idSubRamo) {
        return await  dbPostgres.query(`
        SELECT operaciones."productosSocio".id, operaciones."productosSocio"."idSubRamo", operaciones."tipoProducto".tipo AS "tipoProducto" FROM operaciones."productosSocio"
        INNER JOIN operaciones."tipoProducto" ON operaciones."tipoProducto".id = operaciones."productosSocio"."idTipoProducto"
        WHERE operaciones."productosSocio"."idSubRamo" = '${idSubRamo}' AND operaciones."productosSocio".activo =1`, {
            type: QueryTypes.SELECT,
        })
    }
    // lo usa cotizador  online
    static async getByIdSubRamoNombre(idSubRamo, nombre) {
        return await ProductosSocioModel.findOne({
            where: {idSubRamo, activo: 1, nombre}
        })
    }
}
