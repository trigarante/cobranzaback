import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SubsecuentesQueries {
    static async findAll(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
           SELECT
           DISTINCT(rg.id),
            rg.poliza, cl."telefonoMovil",
            cl.nombre as "nombreCliente", cl.paterno as "apellidoPaternoCliente", cl.materno as "apellidoMaternoCliente",precand.nombre, precand."apellidoPaterno", precand."apellidoMaterno", soc."nombreComercial", dpto.descripcion as departamento,
            ep.estado, er."estadoDescripcion" as "estadoRecibo", rg."fechaInicio",
            json_extract_path_text(pgs.datos, 'cargoRecurrente') as "cargoRecurrente",
            pc."idSolicitud"
            FROM operaciones.registro rg
            JOIN operaciones."productoCliente" pc ON rg."idProducto" = pc."id"
            JOIN operaciones.cliente cl ON pc."idCliente" = cl."id"
            LEFT JOIN "recursosHumanos".empleado emp ON rg."idEmpleado" = emp."id"
            LEFT JOIN "recursosHumanos".candidato cand ON emp."idCandidato" = cand."id"
            LEFT JOIN "recursosHumanos".precandidato precand ON cand."idPrecandidato" = precand."id"
            JOIN operaciones."productosSocio" ps ON rg."idProductoSocio" = ps."id"
            JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
            JOIN operaciones.ramo rm ON sr."idRamo" = rm."id"
            JOIN operaciones.socios soc ON rm."idSocio" = soc."id"
            JOIN "recursosHumanos".departamento dpto ON rg."idDepartamento" = dpto."id"
            JOIN operaciones."estadoPoliza" ep ON rg."idEstadoPoliza" = ep."id"
            JOIN operaciones.recibos rb ON rg.id = rb."idRegistro"
            JOIN operaciones."estadoRecibo" er ON rb."idEstadoRecibos" = er."id"
            LEFT JOIN operaciones.pagos pgs ON pgs."idRecibo" = rb."id"
            JOIN operaciones."tipoPago" tp ON rg."idTipoPago" = tp."id"
            WHERE rb.activo = 1
            AND tp."cantidadPagos" > 1
            AND rg."fechaInicio" BETWEEN '${fechaInicio}' AND '${fechaFin}'
            ORDER BY rg."id" DESC
        `, {
            type: QueryTypes.SELECT,
            plain: false,
        });
    }

    static async subsecuentesDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
           SELECT
           DISTINCT(rg.id),
            rg.poliza,
            cl."telefonoMovil",
            cl.nombre as "nombreCliente", cl.paterno as "apellidoPaternoCliente", cl.materno as "apellidoMaternoCliente",precand.nombre, precand."apellidoPaterno", precand."apellidoMaterno", soc."nombreComercial", dpto.descripcion as departamento,
            ep.estado, er."estadoDescripcion" as "estadoRecibo", rg."fechaInicio",
            json_extract_path_text(pgs.datos, 'cargoRecurrente') as "cargoRecurrente",
            pc."idSolicitud"
            FROM operaciones.registro rg
            JOIN operaciones."productoCliente" pc ON rg."idProducto" = pc."id"
            JOIN operaciones.cliente cl ON pc."idCliente" = cl."id"
            LEFT JOIN "recursosHumanos".empleado emp ON rg."idEmpleado" = emp."id"
            LEFT JOIN "recursosHumanos".candidato cand ON emp."idCandidato" = cand."id"
            LEFT JOIN "recursosHumanos".precandidato precand ON cand."idPrecandidato" = precand."id"
            JOIN operaciones."productosSocio" ps ON rg."idProductoSocio" = ps."id"
            JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
            JOIN operaciones.ramo rm ON sr."idRamo" = rm."id"
            JOIN operaciones.socios soc ON rm."idSocio" = soc."id"
            JOIN "recursosHumanos".departamento dpto ON rg."idDepartamento" = dpto."id"
            JOIN operaciones."estadoPoliza" ep ON rg."idEstadoPoliza" = ep."id"
            JOIN operaciones.recibos rb ON rg.id = rb."idRegistro"
            JOIN operaciones."estadoRecibo" er ON rb."idEstadoRecibos" = er."id"
            LEFT JOIN operaciones.pagos pgs ON pgs."idRecibo" = rb."id"
            JOIN operaciones."tipoPago" tp ON rg."idTipoPago" = tp."id"
            WHERE rb.activo = 1
            AND tp."cantidadPagos" > 1
            --AND rg."idDepartamento" IN (${departamentos})
            AND rg."fechaInicio" BETWEEN '${fechaInicio}' AND '${fechaFin}'
            ORDER BY rg."id" DESC
        `, {
            type: QueryTypes.SELECT,
            plain: false,
        });
    }

    static async subsecuentesEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
           SELECT
           DISTINCT(rg.id),
            rg.poliza,
            cl."telefonoMovil",
            cl.nombre as "nombreCliente", cl.paterno as "apellidoPaternoCliente", cl.materno as "apellidoMaternoCliente",precand.nombre, precand."apellidoPaterno", precand."apellidoMaterno", soc."nombreComercial", dpto.descripcion as departamento,
            ep.estado, er."estadoDescripcion" as "estadoRecibo", rg."fechaInicio",
            json_extract_path_text(pgs.datos, 'cargoRecurrente') as "cargoRecurrente",
            pc."idSolicitud"
            FROM operaciones.registro rg
            JOIN operaciones."productoCliente" pc ON rg."idProducto" = pc."id"
            JOIN operaciones.cliente cl ON pc."idCliente" = cl."id"
            LEFT JOIN "recursosHumanos".empleado emp ON rg."idEmpleado" = emp."id"
            LEFT JOIN "recursosHumanos".candidato cand ON emp."idCandidato" = cand."id"
            LEFT JOIN "recursosHumanos".precandidato precand ON cand."idPrecandidato" = precand."id"
            JOIN operaciones."productosSocio" ps ON rg."idProductoSocio" = ps."id"
            JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr."id"
            JOIN operaciones.ramo rm ON sr."idRamo" = rm."id"
            JOIN operaciones.socios soc ON rm."idSocio" = soc."id"
            JOIN "recursosHumanos".departamento dpto ON rg."idDepartamento" = dpto."id"
            JOIN operaciones."estadoPoliza" ep ON rg."idEstadoPoliza" = ep."id"
            JOIN operaciones.recibos rb ON rg.id = rb."idRegistro"
            JOIN operaciones."estadoRecibo" er ON rb."idEstadoRecibos" = er."id"
            LEFT JOIN operaciones.pagos pgs ON pgs."idRecibo" = rb."id"
            JOIN operaciones."tipoPago" tp ON rg."idTipoPago" = tp."id"
            WHERE rb.activo = 1
            AND tp."cantidadPagos" > 1
            -- AND rg."idDepartamento" = ${idDepartamento}
            AND rg."idEmpleado" = ${idEmpleado}
            AND rg."fechaInicio" BETWEEN '${fechaInicio}' AND '${fechaFin}'
            ORDER BY rg."id" DESC
        `, {
            type: QueryTypes.SELECT,
            plain: false,
        });
    }
}
