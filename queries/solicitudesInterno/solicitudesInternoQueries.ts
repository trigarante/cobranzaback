import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";
import solicitudesInternoModel from "../../models/venta-nueva/solicitudesInterno/solicitudesInternoModel";
import solicitudesInternoOnlineModel from "../../models/venta-nueva/solicitudesInterno/solicitudesInternoOnline";

export default class SolicitudesInternoQueries {
    static async getSolicitudes(idEmpleado): Promise<any> {
        return await dbPostgres.query(`
            SELECT *
                FROM operaciones.solicitudes
                WHERE operaciones.solicitudes."idEmpleado" = ${idEmpleado}
                    ORDER BY
                    id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario"
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }

    static async getAll(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                operaciones."estadoSolicitud".estado,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                CASE WHEN "operaciones"."cliente".nombre || "operaciones"."cliente"."paterno" || "operaciones"."cliente"."materno" IS NOT NULL THEN "operaciones"."cliente".nombre || ' ' || "operaciones"."cliente"."paterno" || ' ' || "operaciones"."cliente"."materno" ELSE prospecto.nombre END AS "nombreCliente",
                "operaciones"."productoSolicitud".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            solicitudes.emitida = 0
            AND
            cotizaciones."idTipoContacto" = ${idTipoContacto} and solicitudes."idEstadoSolicitud" = ${idEstadoSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentos(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                operaciones."estadoSolicitud".estado,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                CASE WHEN "operaciones"."cliente".nombre || "operaciones"."cliente"."paterno" || "operaciones"."cliente"."materno" IS NOT NULL THEN "operaciones"."cliente".nombre || ' ' || "operaciones"."cliente"."paterno" || ' ' || "operaciones"."cliente"."materno" ELSE prospecto.nombre END AS "nombreCliente",
                "operaciones"."productoSolicitud".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            operaciones.solicitudes."idDepartamento" IN (${departamentos})
            AND
            solicitudes.emitida = 0
            AND
            cotizaciones."idTipoContacto" = ${idTipoContacto} and solicitudes."idEstadoSolicitud" = ${idEstadoSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByHijos(fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                operaciones."estadoSolicitud".estado,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                CASE WHEN "operaciones"."cliente".nombre || "operaciones"."cliente"."paterno" || "operaciones"."cliente"."materno" IS NOT NULL THEN "operaciones"."cliente".nombre || ' ' || "operaciones"."cliente"."paterno" || ' ' || "operaciones"."cliente"."materno" ELSE prospecto.nombre END AS "nombreCliente",
                "operaciones"."productoSolicitud".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            operaciones.solicitudes."idEmpleado" IN (${departamentos})
            AND
            solicitudes.emitida = 0
            AND
            cotizaciones."idTipoContacto" = ${idTipoContacto} and solicitudes."idEstadoSolicitud" = ${idEstadoSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin, idTipoContacto, idEstadoSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                operaciones."estadoSolicitud".estado,
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                CASE WHEN "operaciones"."cliente".nombre || "operaciones"."cliente"."paterno" || "operaciones"."cliente"."materno" IS NOT NULL THEN "operaciones"."cliente".nombre || ' ' || "operaciones"."cliente"."paterno" || ' ' || "operaciones"."cliente"."materno" ELSE prospecto.nombre END AS "nombreCliente",
                "operaciones"."productoSolicitud".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
                "operaciones"."prospecto".numero
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                INNER JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
                INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            WHERE operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            --AND
            --"recursosHumanos".departamento.id = ${idDepartamento}
            AND
            operaciones.solicitudes."idEmpleado" = ${idEmpleado}
            AND
            solicitudes.emitida = 0
            AND
            cotizaciones."idTipoContacto" = ${idTipoContacto} and solicitudes."idEstadoSolicitud" = ${idEstadoSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getById(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "operaciones"."cliente".nombre "nombreCliente",
                "operaciones"."cliente"."paterno" "apellidoPaternoCliente",
                "operaciones"."cliente"."materno" "apellidoMaternoCliente",
                "operaciones"."productoCliente".id "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre "nombreEmpleado",
                "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaternoEmpleado",
                "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaternoEmpleado",
                "operaciones"."prospecto".numero,
                "operaciones"."cotizacionesAli".peticion,
                "operaciones"."cotizacionesAli".respuesta
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                LEFT JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
            WHERE operaciones.solicitudes.id = ${idSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    // se cambio "productoCliente".id "idProducto", por c`cotizaciones`.`idProducto` AS `idProducto`,
    // lo usa cotizador  online
    static async getByIdOnline(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones.solicitudes."idCotizacionAli",
                operaciones.solicitudes."idEmpleado",
                operaciones.solicitudes."fechaSolicitud",
                "recursosHumanos".departamento.id "idDepartamento",
                "recursosHumanos".departamento.descripcion "descripcionDepartemento",
                "operaciones"."cliente".nombre "nombreCliente",
                "operaciones"."cliente"."paterno" "apellidoPaternoCliente",
                "operaciones"."cliente"."materno" "apellidoMaternoCliente",
                "operaciones"."cotizaciones"."idProducto" "idProducto",
                "operaciones"."prospecto".id "idProspecto",
                "recursosHumanos".precandidato.nombre "nombreEmpleado",
                "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaternoEmpleado",
                "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaternoEmpleado",
                "operaciones"."prospecto".numero,
                "operaciones"."cotizacionesAli".peticion,
                "operaciones"."cotizacionesAli".respuesta,
                "operaciones"."cotizacionesAli"."idSubRamo",
                "operaciones"."cotizacionesAli"."nuevoJson"
            FROM operaciones.solicitudes
                LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
                LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
                LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
                LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
                LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
                LEFT JOIN "operaciones"."productoCliente" ON "operaciones"."productoCliente"."idSolicitud" = operaciones.solicitudes.id
                LEFT JOIN "operaciones"."cliente" ON "operaciones"."cliente"."id" = "operaciones"."productoCliente"."idCliente"
                LEFT JOIN "operaciones"."cotizacionesAli" ON "operaciones"."cotizacionesAli"."id" = operaciones.solicitudes."idCotizacionAli"
                INNER JOIN "operaciones"."cotizaciones" ON "operaciones"."cotizaciones"."id" = "operaciones"."cotizacionesAli"."idCotizacion"
                INNER JOIN "operaciones"."productoSolicitud" ON "operaciones"."productoSolicitud"."id" = "operaciones"."cotizaciones"."idProducto"
                INNER JOIN "operaciones"."prospecto" ON "operaciones"."prospecto"."id" = "operaciones"."productoSolicitud"."idProspecto"
            WHERE operaciones.solicitudes.id = ${idSolicitud}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }
    static async getStepsById(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
SELECT
                DISTINCT ON (registro."id")
                solicitudes."id",
                "cotizacionesAli".peticion,
                "cotizacionesAli".respuesta,
                "productoCliente"."idCliente",
                "productoCliente"."id" AS "idProducto",
                registro.id as "idRegistro",
                registro.id AS "idRecibo",
                pagos.id AS "idPago",
                "cotizacionesAli"."idSubRamo"
            FROM
                operaciones.solicitudes
            INNER JOIN
                operaciones."cotizacionesAli"
                ON
                solicitudes."idCotizacionAli" = "cotizacionesAli"."id"
            LEFT JOIN
                operaciones."productoCliente"
                ON
                solicitudes."id" = "productoCliente"."idSolicitud"
            LEFT JOIN
                operaciones.registro
                ON
                "productoCliente"."id" = registro."idProducto"
            LEFT JOIN
                operaciones.recibos
                ON
                registro."id" = recibos."idRegistro"
            LEFT JOIN
                operaciones.pagos
                ON
                recibos."id" = pagos."idRecibo"
            WHERE solicitudes.id = ${idSolicitud}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getByIdEstadoContacto(idEstadoContacto): Promise<any> {
        return await dbPostgres.query(`
            SELECT *
                FROM operaciones.solicitudes
                WHERE operaciones.solicitudes."idEstadoContacto" = ${idEstadoContacto}
                    ORDER BY
                    id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async post(data) {
        return await solicitudesInternoModel.create(data).then(data => {
           return data.id;
        });
    }
    static async postOnline(data) {
        return await solicitudesInternoOnlineModel.create(data).then(data => {
            return data.id;
        });
    }

    static async update(data, id) {
        return await solicitudesInternoModel.update(data, {
            where: {id}
        });
    }

    static async getInfoProspectoStepCliente(id) {
        return dbPostgres.query(`
        SELECT
            prospecto.numero,
            prospecto.nombre,
            prospecto.correo
        FROM
            operaciones.solicitudes
        INNER JOIN
            operaciones."cotizacionesAli"
            ON
            solicitudes."idCotizacionAli" = "cotizacionesAli"."id"
        INNER JOIN
            operaciones.cotizaciones
            ON
            "cotizacionesAli"."idCotizacion" = cotizaciones."id"
        INNER JOIN
            operaciones."productoSolicitud"
            ON
            cotizaciones."idProducto" = "productoSolicitud"."id"
        INNER JOIN
            operaciones.prospecto
            ON
            "productoSolicitud"."idProspecto" = prospecto."id"
        WHERE solicitudes."id" = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        })
    }
// SOLICITUDESVNVIEW
    static async getAllTipo(idEstado, idTipoContacto, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartemento",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            operaciones."estadoSolicitud".id = ${idEstado}
            AND
            operaciones.cotizaciones."idTipoContacto" = ${idTipoContacto}
            ORDER BY
            operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
// SOLICITUDESVNVIEW
    static async getAllByDepartamentosTipo(idEstado, idTipoContacto, fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartemento",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            operaciones.solicitudes."idDepartamento" IN (${departamentos})
            AND
            operaciones."estadoSolicitud".id = ${idEstado}
            AND
            operaciones.cotizaciones."idTipoContacto" = ${idTipoContacto}
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
// SOLICITUDESVNVIEW
    static async getAllByDepartamentoAndIdEmpleadoTipo(idEstado, idTipoContacto, idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            operaciones.solicitudes.id,
            operaciones.solicitudes."idCotizacionAli",
            operaciones.solicitudes."idEmpleado",
            operaciones.solicitudes."idEstadoSolicitud",
            operaciones.solicitudes."fechaSolicitud",
            operaciones.solicitudes."comentarios",
            operaciones."cotizacionesAli"."idCotizacion",
            operaciones."cotizacionesAli"."fechaCotizacion",
            operaciones."estadoSolicitud".estado,
            operaciones."estadoSolicitud".activo,
            "recursosHumanos".empleado."idCandidato",
            "recursosHumanos".departamento.id "idDepartamento",
            "recursosHumanos".departamento.descripcion "descripcionDepartemento",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
            operaciones.cotizaciones."idProducto",
            operaciones.cotizaciones."idTipoContacto",
            operaciones."productoSolicitud"."idProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'nombre' is null then operaciones.prospecto.nombre else operaciones."productoSolicitud".datos::json ->> 'nombre' end AS "nombreProspecto",
            operaciones.prospecto.correo "correoProspecto",
            CASE WHEN operaciones."productoSolicitud".datos::json ->> 'telefono' is null then operaciones.prospecto.numero else operaciones."productoSolicitud".datos::json ->> 'telefono' end AS "numeroProspecto",
            operaciones.solicitudes."idFlujoSolicitud",
            operaciones.cotizaciones."idMedioDifusion"
            FROM operaciones.solicitudes
            INNER JOIN operaciones."cotizacionesAli" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAli".id
            LEFT JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones."solicitudes"."idEmpleado"
            LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos"."empleado"."idCandidato"
            LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos"."candidato"."idPrecandidato"
            LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".plaza.id = "recursosHumanos"."empleado"."idPlaza"
            LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento.id = "recursosHumanos"."plaza"."idDepartamento"
            INNER JOIN operaciones."estadoSolicitud" ON operaciones.solicitudes."idEstadoSolicitud" = operaciones."estadoSolicitud".id
            INNER JOIN operaciones.cotizaciones ON operaciones.cotizaciones."id" = operaciones."cotizacionesAli"."idCotizacion"
            INNER JOIN operaciones."productoSolicitud" ON operaciones."productoSolicitud"."id" = operaciones.cotizaciones."idProducto"
            INNER JOIN operaciones."prospecto" ON "operaciones"."prospecto"."id" = operaciones."productoSolicitud"."idProspecto"
            WHERE
            operaciones.solicitudes."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            AND
            "recursosHumanos".departamento.id = ${idDepartamento}
            AND
            operaciones.solicitudes."idEmpleado" = ${idEmpleado}
            AND
            operaciones."estadoSolicitud".id = ${idEstado}
            AND
            operaciones.cotizaciones."idTipoContacto" = ${idTipoContacto}

            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
}
