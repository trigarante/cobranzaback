import ErroresAnexosAModel from "../../models/venta-nueva/erroresAnexo/erroresAnexosAModel";
export default class ErroresAnexosAQueries {
    static async getByIdAutorizacionAndDocumento(idAutorizacionRegistro, idTipoDocumento) {
        return await ErroresAnexosAModel.findOne({
            where: {
                idAutorizacionRegistro,
                idTipoDocumento,
                idEstadoCorreccion:1
            },
        })
    }

    static async post(data){
        return await ErroresAnexosAModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async update(data, id){
        return await ErroresAnexosAModel.update(data, {
            where: {id}
        });
    }
}
