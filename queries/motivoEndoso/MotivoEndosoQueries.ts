import MotivoEndosoModel from "../../models/venta-nueva/motivoEndoso/MotivoEndosoModel";


export default class MotivoEndosoQueries {

    static async findAllByIdTipoEndoso(idTipoEndoso): Promise<any> {
        return await MotivoEndosoModel.findAll({
            where: {idTipoEndoso}
        })
    }

}



