import TipoEndosoModel from "../../models/venta-nueva/tipoEndoso/TipoEndosoModel";


export default class TipoEndosoQueries {

    static async getAll(): Promise<any> {
        return await TipoEndosoModel.findAll({
            where: {activo: 1}
        })
    }

}



