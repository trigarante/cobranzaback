import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class DashboardQueries {
    static async getDepartamentos() {
        return await dbPostgres.query(`
        SELECT
            departamento.id "idSubarea",
            departamento.descripcion subarea
        FROM "recursosHumanos".area INNER JOIN "recursosHumanos".departamento ON area."id" = departamento."idArea"
        WHERE "idArea" IN  (47, 48, 52, 53)
        ORDER BY descripcion ASC`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargaColumnaError(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    "llamadaSolicitud"."llamadasError" AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND ( "llamadaSolicitud"."llamadasError" >= 2
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL
    AND "llamadaSolicitud".llamadas < 1 )`, {
            type: QueryTypes.SELECT
        })
    }

    static async descargaColumnaNoContactados(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    CASE WHEN "llamadaSolicitud".llamadas IS NULL THEN 0 ELSE "llamadaSolicitud".llamadas END AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND ("llamadaSolicitud"."idSolicitud" IS NULL
    OR ( "llamadaSolicitud"."llamadasError" < 2 AND "llamadaSolicitud".llamadas < 1 AND "llamadaSolicitud".efectivas < 1 ))`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargaColumnaMarcacion1a4(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    "llamadaSolicitud".llamadas AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND ("llamadaSolicitud".llamadas >= 1
    AND "llamadaSolicitud".llamadas < 8
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL)`, {
            type: QueryTypes.SELECT
        })
    }

    static async descargaColumnaMarcacion5(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    "llamadaSolicitud".llamadas AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND"llamadaSolicitud".llamadas >= 8 AND registro.id IS NULL`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargaColumnaContactados(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    "llamadaSolicitud".llamadas AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND "llamadaSolicitud".efectivas >= 1
    AND registro.id IS NULL`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargaColumnaRegistro(fechaInicio, fechaFin, idEmpleado) {
        return await dbPostgres.query(`
        SELECT
    DISTINCT ON (operaciones.solicitudes.id)
    operaciones.solicitudes.id,
    "recursosHumanos".departamento.descripcion,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    "llamadaSolicitud".llamadas AS llamadas
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado"
) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}
    AND registro.id IS NOT NULL`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargarLlamadas(fechaInicio, fechaFin) {
        return await dbPostgres.query(`
        SELECT
        operaciones.llamadas."fechaRegistro",
    operaciones.llamadas."fechaInicio",
    cast( operaciones.llamadas."fechaInicio" AS time ) AS "horaInicio",
    "recursosHumanos".area.nombre AS area,
    "recursosHumanos".departamento.descripcion AS  departamento,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    operaciones.llamadas.numero AS numero,
(EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio"))
    AS duracion,
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones."subEtiquetaSolicitud".descripcion AS subetiqueta,
    operaciones."tipoLlamada".descripcion AS tipo,
    operaciones."estadoLlamada".descripcion AS estado
    FROM
    operaciones.llamadas
    JOIN "recursosHumanos".empleado ON operaciones.llamadas."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area.id
    JOIN operaciones."tipoLlamada" ON operaciones.llamadas."idTipoLlamada" = operaciones."tipoLlamada".id
    JOIN operaciones."estadoLlamada" ON operaciones.llamadas."idEstadoLlamada" = operaciones."estadoLlamada".id
    LEFT JOIN operaciones."subEtiquetaSolicitud" ON operaciones.llamadas."idSubEtiqueta" = operaciones."subEtiquetaSolicitud".id
    WHERE
    DATE( operaciones.llamadas."fechaRegistro" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')`, {
            type: QueryTypes.SELECT
        })
    }
    static async descargarLeads(fechaInicio, fechaFin) {
        return await dbPostgres.query(`
        SELECT
DISTINCT ON (operaciones.solicitudes.id)
operaciones.solicitudes.id AS "idSolicitud",
    "recursosHumanos".plaza."idDepartamento",
    operaciones.solicitudes."fechaSolicitud",
    "recursosHumanos".area.nombre AS area,
    "recursosHumanos".departamento.descripcion AS departamento,
    concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    operaciones."productoSolicitud".datos ->> 'telefono' AS numero,
    CASE WHEN "llamadaSolicitud".llamadas IS NULL THEN 0 ELSE "llamadaSolicitud".llamadas END AS llamadas,
    CASE WHEN "llamadaSolicitud".efectivas IS NULL THEN 0 ELSE "llamadaSolicitud".efectivas END  AS "llamadasEfectivas",
    "llamadaSolicitud"."callbackEfectivo",
    CASE WHEN (EXTRACT(MINUTE FROM "llamadaSolicitud"."primerContacto")-EXTRACT(MINUTE FROM operaciones.solicitudes."fechaSolicitud")) IS NULL THEN 0 ELSE (EXTRACT(MINUTE FROM "llamadaSolicitud"."primerContacto")-EXTRACT(MINUTE FROM operaciones.solicitudes."fechaSolicitud")) END AS minutosAtencion
FROM
operaciones.solicitudes
JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
JOIN "recursosHumanos".area ON "recursosHumanos".departamento."idArea" = "recursosHumanos".area.id
LEFT JOIN (
    SELECT
operaciones.llamadas."idSolicitud" AS "idSolicitud",
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
THEN 1 ELSE 0 END) AS llamadas,
    SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
    THEN 1 ELSE 0 END) AS "llamadasError",
    SUM(
        CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
THEN 1 ELSE 0 END) AS efectivas,
    CASE WHEN operaciones.llamadas."idTipoLlamada" = 2
AND  (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
THEN 'SI' ELSE 'NO' END AS "callbackEfectivo",
    MIN( operaciones.llamadas."fechaInicio") AS "primerContacto"
FROM
operaciones.llamadas
GROUP BY
operaciones.llamadas."idSolicitud",
    operaciones.llamadas."idEmpleado",
    "callbackEfectivo"
) AS "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
INNER JOIN operaciones."cotizacionesAliSnJson" ON operaciones.solicitudes."idCotizacionAli" = operaciones."cotizacionesAliSnJson".id
INNER JOIN operaciones.cotizaciones ON operaciones."cotizacionesAliSnJson"."idCotizacion" = operaciones.cotizaciones.id
INNER JOIN operaciones."productoSolicitud" ON operaciones.cotizaciones."idProducto" = operaciones."productoSolicitud".id
WHERE
DATE( operaciones.solicitudes."fechaSolicitud") BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)`, {
            type: QueryTypes.SELECT
        })
    }

    static async getDashboard(fechaInicio, fechaFin, aux2 , aux): Promise<any> {
        return await dbPostgres.query(`
            SELECT
    operaciones.solicitudes."idEmpleado",
        "recursosHumanos".plaza."idDepartamento",
        "recursosHumanos".departamento.descripcion,
        concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    count( * ) AS leads,
        SUM(CASE WHEN "llamadasEmpleados".llamadas IS NULL THEN 0 ELSE "llamadasEmpleados".llamadas END)  AS llamadas,
        SUM(CASE WHEN "llamadaSolicitud"."llamadasError" >= 2
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL
    AND "llamadaSolicitud".llamadas < 1
    THEN 1 ELSE 0 END) AS "contactoErroneo",
        SUM( CASE WHEN "llamadaSolicitud".efectivas >= 1
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS contactados,
        SUM(CASE WHEN "llamadaSolicitud".llamadas >= 8 AND registro.id IS NULL AND "llamadaSolicitud".efectivas < 1
    THEN 1 ELSE 0 END) AS "enMarcacion5",
        SUM(CASE WHEN "llamadaSolicitud"."idSolicitud" IS NULL
    OR ( "llamadaSolicitud"."llamadasError" < 2 AND "llamadaSolicitud".llamadas < 1 AND "llamadaSolicitud".efectivas < 1 )
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS "noContactados",
        SUM(CASE WHEN "llamadaSolicitud".llamadas >= 1
    AND "llamadaSolicitud".llamadas < 8
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS "enMarcacion1a4",
        SUM(CASE WHEN registro.id IS NOT NULL THEN 1 ELSE 0 END) AS "contactoEfectivo",
        SUM(CASE WHEN logueos."horaLogueo" IS NULL THEN 0 ELSE logueos."horaLogueo" END) AS "horaLogueo",
        SUM(CASE WHEN logueos."tiempoLogueadoMinMaxSeg" IS NULL THEN 0 ELSE logueos."tiempoLogueadoMinMaxSeg" END) AS "tiempoLogueo",
        SUM(CASE WHEN pausas."totalPausas" IS NULL THEN 0 ELSE pausas."totalPausas" END) AS pausas,
        SUM(CASE WHEN pausas."totalTiempoPausas" IS NULL THEN 0 ELSE pausas."totalTiempoPausas" END) AS "tiempoPausaa",
        SUM(CASE WHEN "llamadasEmpleados"."tiempoProduc" IS NULL THEN 0  ELSE  "llamadasEmpleados"."tiempoProduc" END) AS "tiempoProductivo"
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
        operaciones.llamadas."idEmpleado" AS "idEmpleado",
        SUM(
            CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
        SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
        THEN 1 ELSE 0 END) AS "llamadasError",
        SUM(
            CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
        operaciones.llamadas."idEmpleado"
    ) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
        SUM(
            CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL
    THEN 1 ELSE 0 END) AS llamadas,
        SUM(
            CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL AND operaciones.llamadas."fechaFinal" IS NOT NULL
    THEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio"))
    ELSE 0 END)
    AS "tiempoProduc"
    FROM
    operaciones.llamadas
    WHERE
    DATE( operaciones.llamadas."fechaInicio" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    GROUP BY
    operaciones.llamadas."idEmpleado"
    ) "llamadasEmpleados" ON "llamadasEmpleados"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    LEFT JOIN (
        SELECT
    logueosD."idEmpleado" AS "idEmpleado",
        EXTRACT(SECOND FROM min(logueosD.inicio)) AS "horaLogueo",
        CASE WHEN max( logueosD.Fin ) IS NULL
    THEN (EXTRACT(SECOND FROM NOW())-EXTRACT(SECOND FROM min(logueosD.inicio)))
    ELSE (EXTRACT(SECOND FROM max(logueosD.fin))-EXTRACT(SECOND FROM min(logueosD.inicio)))
    END AS "tiempoLogueadoMinMaxSeg"
    FROM
    (
        SELECT
    operaciones.usuarios."idEmpleado" AS "idEmpleado",
        generales."sesionUsuarios"."fechaInicio" AS inicio,
        generales."sesionUsuarios"."fechaFin" AS fin
    FROM
    generales."sesionUsuarios"
    JOIN operaciones.usuarios ON generales."sesionUsuarios"."idUsuario" = operaciones.usuarios.id
    WHERE  generales."sesionUsuarios"."fechaInicio" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    ORDER BY
    operaciones.usuarios."idEmpleado"
    ) logueosD
    GROUP BY
    logueosD."idEmpleado"
    ) logueos ON logueos."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN (
        SELECT
    pausas."idEmpleado" AS "idEmpleado",
        count( pausas."idPausa" ) AS "totalPausas",
        sum( pausas.tiempo ) AS "totalTiempoPausas"
    FROM
    (
        SELECT
    operaciones.usuarios."idEmpleado" AS "idEmpleado",
        operaciones."pausaUsuarios".id AS "idPausa",
        CASE WHEN
    EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaFin") - EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaInicio") > 0
    THEN EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaFin") - EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaInicio")
    ELSE 0 END AS tiempo
    FROM
    operaciones."pausaUsuarios"
    LEFT JOIN operaciones."motivosPausa" ON operaciones."pausaUsuarios"."idMotivosPausa" = operaciones."motivosPausa".id
    JOIN operaciones.usuarios ON operaciones."pausaUsuarios"."idUsuario"= operaciones.usuarios.id

    WHERE operaciones."pausaUsuarios"."fechaInicio" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    ORDER BY
    operaciones.usuarios."idEmpleado",
        operaciones."pausaUsuarios".id
    ) pausas
    GROUP BY
    pausas."idEmpleado"
    ORDER BY
    pausas."idEmpleado"
    ) pausas ON pausas."idEmpleado" = operaciones.solicitudes."idEmpleado"
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    ${aux2}
    GROUP BY operaciones.solicitudes."idEmpleado",
        "recursosHumanos".plaza."idDepartamento",
        "recursosHumanos".departamento.descripcion,
        ejecutivo`, {
            type: QueryTypes.SELECT
        })
    }
    static async getDashboardIdSubarea(fechaInicio, fechaFin, idSubarea, aux2, aux): Promise<any> {
        return await dbPostgres.query(`
            SELECT
    operaciones.solicitudes."idEmpleado",
        "recursosHumanos".plaza."idDepartamento",
        "recursosHumanos".departamento.descripcion,
        concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS ejecutivo,
    count( * ) AS leads,
        SUM(CASE WHEN "llamadasEmpleados".llamadas IS NULL THEN 0 ELSE "llamadasEmpleados".llamadas END)  AS llamadas,
        SUM(CASE WHEN "llamadaSolicitud"."llamadasError" >= 2
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL
    AND "llamadaSolicitud".llamadas < 1
    THEN 1 ELSE 0 END) AS "contactoErroneo",
        SUM( CASE WHEN "llamadaSolicitud".efectivas >= 1
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS contactados,
        SUM(CASE WHEN "llamadaSolicitud".llamadas >= 8 AND registro.id IS NULL AND "llamadaSolicitud".efectivas < 1
    THEN 1 ELSE 0 END) AS "enMarcacion5",
        SUM(CASE WHEN "llamadaSolicitud"."idSolicitud" IS NULL
    OR ( "llamadaSolicitud"."llamadasError" < 2 AND "llamadaSolicitud".llamadas < 1 AND "llamadaSolicitud".efectivas < 1 )
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS "noContactados",
        SUM(CASE WHEN "llamadaSolicitud".llamadas >= 1
    AND "llamadaSolicitud".llamadas < 8
    AND "llamadaSolicitud".efectivas < 1
    AND registro.id IS NULL
    THEN 1 ELSE 0 END) AS "enMarcacion1a4",
        SUM(CASE WHEN registro.id IS NOT NULL THEN 1 ELSE 0 END) AS "contactoEfectivo",
        SUM(CASE WHEN logueos."horaLogueo" IS NULL THEN 0 ELSE logueos."horaLogueo" END) AS "horaLogueo",
        SUM(CASE WHEN logueos."tiempoLogueadoMinMaxSeg" IS NULL THEN 0 ELSE logueos."tiempoLogueadoMinMaxSeg" END) AS "tiempoLogueo",
        SUM(CASE WHEN pausas."totalPausas" IS NULL THEN 0 ELSE pausas."totalPausas" END) AS pausas,
        SUM(CASE WHEN pausas."totalTiempoPausas" IS NULL THEN 0 ELSE pausas."totalTiempoPausas" END) AS "tiempoPausaa",
        SUM(CASE WHEN "llamadasEmpleados"."tiempoProduc" IS NULL THEN 0  ELSE  "llamadasEmpleados"."tiempoProduc" END) AS "tiempoProductivo"
    FROM
    operaciones.solicitudes
    JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza.id
    JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento.id
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idSolicitud" AS "idSolicitud",
        operaciones.llamadas."idEmpleado" AS "idEmpleado",
        SUM(
            CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 1
    THEN 1 ELSE 0 END) AS llamadas,
        SUM(CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) < 2
        THEN 1 ELSE 0 END) AS "llamadasError",
        SUM(
            CASE WHEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio")) > 30
    AND  operaciones.llamadas."idSubEtiqueta" NOT IN ( 72, 40, 30, 3 )
    THEN 1 ELSE 0 END) AS efectivas
    FROM
    operaciones.llamadas
    GROUP BY
    operaciones.llamadas."idSolicitud",
        operaciones.llamadas."idEmpleado"
    ) "llamadaSolicitud" ON "llamadaSolicitud"."idSolicitud" = operaciones.solicitudes.id
    AND "llamadaSolicitud"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN (
        SELECT
    operaciones.llamadas."idEmpleado" AS "idEmpleado",
        SUM(
            CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL
    THEN 1 ELSE 0 END) AS llamadas,
        SUM(
            CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL AND operaciones.llamadas."fechaFinal" IS NOT NULL
    THEN (EXTRACT(SECOND FROM operaciones.llamadas."fechaFinal")-EXTRACT(SECOND FROM operaciones.llamadas."fechaInicio"))
    ELSE 0 END)
    AS "tiempoProduc"
    FROM
    operaciones.llamadas
    WHERE
    DATE( operaciones.llamadas."fechaInicio" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    GROUP BY
    operaciones.llamadas."idEmpleado"
    ) "llamadasEmpleados" ON "llamadasEmpleados"."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
    LEFT JOIN operaciones.registro ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    LEFT JOIN (
        SELECT
    logueosD."idEmpleado" AS "idEmpleado",
        EXTRACT(SECOND FROM min(logueosD.inicio)) AS "horaLogueo",
        CASE WHEN max( logueosD.Fin ) IS NULL
    THEN (EXTRACT(SECOND FROM NOW())-EXTRACT(SECOND FROM min(logueosD.inicio)))
    ELSE (EXTRACT(SECOND FROM max(logueosD.fin))-EXTRACT(SECOND FROM min(logueosD.inicio)))
    END AS "tiempoLogueadoMinMaxSeg"
    FROM
    (
        SELECT
    operaciones.usuarios."idEmpleado" AS "idEmpleado",
        generales."sesionUsuarios"."fechaInicio" AS inicio,
        generales."sesionUsuarios"."fechaFin" AS fin
    FROM
    generales."sesionUsuarios"
    JOIN operaciones.usuarios ON generales."sesionUsuarios"."idUsuario" = operaciones.usuarios.id
    WHERE  generales."sesionUsuarios"."fechaInicio" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    ORDER BY
    operaciones.usuarios."idEmpleado"
    ) logueosD
    GROUP BY
    logueosD."idEmpleado"
    ) logueos ON logueos."idEmpleado" = operaciones.solicitudes."idEmpleado"
    LEFT JOIN (
        SELECT
    pausas."idEmpleado" AS "idEmpleado",
        count( pausas."idPausa" ) AS "totalPausas",
        sum( pausas.tiempo ) AS "totalTiempoPausas"
    FROM
    (
        SELECT
    operaciones.usuarios."idEmpleado" AS "idEmpleado",
        operaciones."pausaUsuarios".id AS "idPausa",
        CASE WHEN
    EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaFin") - EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaInicio") > 0
    THEN EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaFin") - EXTRACT(SECOND FROM operaciones."pausaUsuarios"."fechaInicio")
    ELSE 0 END AS tiempo
    FROM
    operaciones."pausaUsuarios"
    LEFT JOIN operaciones."motivosPausa" ON operaciones."pausaUsuarios"."idMotivosPausa" = operaciones."motivosPausa".id
    JOIN operaciones.usuarios ON operaciones."pausaUsuarios"."idUsuario"= operaciones.usuarios.id

    WHERE operaciones."pausaUsuarios"."fechaInicio" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    ORDER BY
    operaciones.usuarios."idEmpleado",
        operaciones."pausaUsuarios".id
    ) pausas
    GROUP BY
    pausas."idEmpleado"
    ORDER BY
    pausas."idEmpleado"
    ) pausas ON pausas."idEmpleado" = operaciones.solicitudes."idEmpleado"
    WHERE
    DATE( operaciones.solicitudes."fechaSolicitud" ) BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
    ${aux2}
    GROUP BY operaciones.solicitudes."idEmpleado",
        "recursosHumanos".plaza."idDepartamento",
        "recursosHumanos".departamento.descripcion,
        ejecutivo`, {
            type: QueryTypes.SELECT
        })
    }
}


