import EstadoInspeccionModel from "../../models/venta-nueva/estadoInspeccion/estadoInspeccionModel";


export default class EstadoInspeccionQueries {

    static async findAll(): Promise<any> {
        return await EstadoInspeccionModel.findAll({
            where: {activo: 1}
        })
    }

}



