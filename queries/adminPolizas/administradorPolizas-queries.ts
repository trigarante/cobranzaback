import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import historialCancelacionENRModel from "../../models/venta-nueva/ENR/historialCancelacionENRModel";

export default class AdministradorPolizasQueries {

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario",
            pl."idPuesto",
            pl.id
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    static async getAll(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo,
            prepago.url
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            LEFT JOIN operaciones."prepagoPay" prepago ON prepago."idRecibo" = re.id
            LEFT JOIN app."usuarioApp" ON cl."idUsuarioApp" = "usuarioApp".id
            where
            re.activo = 1
            and
            (r."idFlujoPoliza" = 4 or dp.id = 263)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByIdEmpleado(fechaInicio, fechaFin, idEmpleado): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo,
            prepago.url
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            LEFT JOIN operaciones."prepagoPay" prepago ON prepago."idRecibo" = re.id
            LEFT JOIN app."usuarioApp" ON cl."idUsuarioApp" = "usuarioApp".id
            where
            re.activo = 1
            and
            (r."idFlujoPoliza" = 4 or dp.id = 263)
            and
            r."idEmpleado" = '${idEmpleado}'
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllPolizasTotales(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllSinPagar(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            AND re."idEstadoRecibos" = 5
            and
            r."idEstadoPoliza" <> 9
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllSinAPlciar(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            AND re."idEstadoRecibos" <> 5
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByDepartamentos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4  OR r."idFlujoPoliza" = 1)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            )
            and r."idDepartamento" IN (${departamentos})
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllSinPagarByDepartamentos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            AND re."idEstadoRecibos" = 5
            and r."idDepartamento" IN (${departamentos})
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllSinAplicarByDepartamentos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            AND re."idEstadoRecibos" <> 5
            and r."idDepartamento" IN (${departamentos})
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByHijos(departamentos, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1 OR r."idFlujoPoliza" = 9)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            )
            and r."idEmpleado" IN (${departamentos})
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1 OR r."idFlujoPoliza" = 9)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            )
            -- and r."idDepartamento" = ${idDepartamento}
            and r."idEmpleado" = ${idEmpleado}
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }
    static async getAllSPByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            AND re."idEstadoRecibos" = 5
            -- and r."idDepartamento" = ${idDepartamento}
            and r."idEmpleado" = ${idEmpleado}
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }
    static async getAllSAByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            pz."idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR (r."idFlujoPoliza" = 1 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 15
                )
            ))
            OR (r."idFlujoPoliza" = 9 and
            (
                pa.id is not null
                    or
                        (pa.id is null
                        and
                    extract (day from (now() -r."fechaInicio")) > 25
                )
            )))
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            AND re."idEstadoRecibos" <> 5
            -- and r."idDepartamento" = ${idDepartamento}
            and r."idEmpleado" = ${idEmpleado}
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY r.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getPoliza(poliza): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
						r.poliza = '${poliza}'
						and
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1 OR r."idFlujoPoliza" = 9)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getSerie(numero): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
						pc."noSerie" = '${numero}'
						and
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1 OR r."idFlujoPoliza" = 9)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllInternas(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            re.numero = 1
            and
            r."idFlujoPoliza"=7
            and
            r."idEstadoPoliza" <> 14
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllSubsecuentes(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            tp."cantidadPagos" > 1
            and
            re.numero = 1
            and
            --r."idFlujoPoliza" in (1, 7, 4, 9)
            --and
            r."idEstadoPoliza" <> 14
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllPolizas(poliza, query): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 14
            and
            r.poliza = '${poliza}'
            ${query}
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllBySerie(numero, query: string): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                r.id,
                r.poliza,
                r."fechaInicio",
                r."primaNeta",
                r."fechaRegistro",
                r.archivo,
                tp."cantidadPagos",
                tp."tipoPago",
                ps.nombre AS "productoSocio",
                r."idEstadoPoliza",
                ep.estado AS "estadoPoliza",
                preca.nombre,
                preca."apellidoPaterno",
                preca."apellidoMaterno",
                pz."idDepartamento",
                pc.datos,
                CASE
                WHEN pc."noSerie" IS NOT NULL THEN
                pc."noSerie"
                WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN
                pc.datos :: json ->> 'numeroSerie' ELSE 'SIN NUMERO DE SERIE'
                END AS "numeroSerie",
                preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" AS "nombreEmpleado",
                cl.nombre || ' ' || cl.paterno || ' ' || cl.materno AS "nombreCliente",
                so."nombreComercial",
                dp.descripcion,
                ep.estado AS "estadoPoliza",
                er."estadoDescripcion" AS "estadoRecibo",
                CASE
                WHEN pa.id IS NOT NULL THEN
                espa.descripcion ELSE 'SIN PAGO'
                END AS "estadoPago",
                cl."telefonoMovil",
                r."fechaInicio",
                CASE
                WHEN re."fechaCierre" IS NOT NULL THEN
                1 ELSE 0
                END AS cierre,
                re.id AS "idRecibo",
                CASE
                WHEN pc.archivo IS NOT NULL THEN
                pc.archivo ELSE cl.archivo
                END AS "carpetaCliente",-- importante
                re."idEstadoRecibos",
                pa.id AS "idPago",
                pa."idEstadoPago",
                pc."idSolicitud",
                r.emitida,
                cl.id AS "idCliente"
                FROM
                operaciones.registro r
                INNER JOIN operaciones."productosSocio" ps ON r."idProductoSocio" = ps.id
                INNER JOIN operaciones."productoCliente" pc ON r."idProducto" = pc.id
                INNER JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
                INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
                INNER JOIN "recursosHumanos".plaza pz ON e."idPlaza" = pz.id
                INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" = preca.id
                INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
                INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
                INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
                INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
                INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
                INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
                LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
                LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
                LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
						pc."noSerie" = '${numero}'
						and
            re.activo = 1
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 14
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getAllPolizasTotalesSubsecuentes(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            r.emitida,
            cl.id as "idCliente",
            cl.correo
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            AND (r."idFlujoPoliza" = 4 OR r."idFlujoPoliza" = 1)
            and
            re.numero = 1
            and
            r."idEstadoPoliza" <> 9
            and r."fechaRegistro" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getAllENR(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."idProducto",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            espa.id AS "idEstadoP",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            pc."idEmision",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            re.activo = 1
            and
            r."idFlujoPoliza" = 58
            and
            re.numero = 1
            and
            (r."idEstadoPoliza" = 25 OR r."idEstadoPoliza" = 4)
            and r."fechaRegistro" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getENRBySerie(numero): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."idProducto",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            pc."idEmision",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            pc."noSerie" = '${numero}'
            and
            re.activo = 1
            and
            r."idFlujoPoliza" = 58
            and
            re.numero = 1
            and
            (r."idEstadoPoliza" = 25 or r."idEstadoPoliza" = 4)
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async getENRByPoliza(poliza): Promise<any> {
        return await dbPostgres.query(`
            SELECT
            r.id,
            r.poliza,
            r."fechaInicio",
            r."primaNeta",
            r."idProducto",
            r."fechaRegistro",
            r.archivo,
            tp."cantidadPagos",
            tp."tipoPago",
            ps.nombre AS "productoSocio",
            r."idEstadoPoliza",
            ep.estado AS "estadoPoliza",
            preca.nombre,
            preca."apellidoPaterno",
            preca."apellidoMaterno",
            dp.id as "idDepartamento",
            pc.datos,
            CASE WHEN pc."noSerie" IS NOT NULL THEN pc."noSerie" WHEN pc.datos :: json ->> 'numeroSerie' IS NOT NULL THEN pc.datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
            preca.nombre || ' ' || preca."apellidoPaterno" || ' ' || preca."apellidoMaterno" as "nombreEmpleado",
            case when cl.paterno is not null and cl.materno is not null then cl.nombre || ' ' || cl.paterno || ' ' || cl.materno when cl.paterno is null and cl.materno is null then cl.nombre end AS "nombreCliente",
            so."nombreComercial",
            dp.descripcion,
            ep.estado AS "estadoPoliza",
            er."estadoDescripcion" AS "estadoRecibo",
            case when pa.id is not null then espa.descripcion else 'SIN PAGO' end AS "estadoPago",
            cl."telefonoMovil",
            r."fechaInicio",
            case when re."fechaCierre" is not null then 1 else 0 end AS cierre,
            re.id AS "idRecibo",
            pc.archivo as "carpetaCliente",
            -- importante
            re."idEstadoRecibos",
            pa.id AS "idPago",
            pa."idEstadoPago",
            pc."idSolicitud",
            pc."idEmision",
            r.emitida,
            cl.id as "idCliente"
            FROM
            operaciones.registro r
            INNER JOIN operaciones."productosSocio" ps On r."idProductoSocio" = ps.id
            INNER JOIN operaciones."productoCliente" pc On r."idProducto" = pc.id
            INNER JOIN operaciones."estadoPoliza" ep On r."idEstadoPoliza" = ep.id
            INNER JOIN "recursosHumanos".empleado e ON r."idEmpleado" = e.id
            INNER JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
            INNER JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
            INNER JOIN "recursosHumanos"."precandidato" preca ON c."idPrecandidato" =preca.id
            INNER JOIN operaciones.cliente cl ON pc."idCliente" = cl.id
            INNER JOIN operaciones.recibos re ON r.id = re."idRegistro"
            INNER JOIN operaciones."estadoRecibo" er ON re."idEstadoRecibos" = er.id
            INNER JOIN operaciones."subRamo" sr ON ps."idSubRamo" = sr.id
            INNER JOIN operaciones.ramo ra ON sr."idRamo" = ra.id
            INNER JOIN operaciones.socios so ON ra."idSocio" = so.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN operaciones."estadoPago" espa ON pa."idEstadoPago" = espa.id
            LEFT JOIN operaciones."tipoPago" tp ON tp.id = r."idTipoPago"
            where
            r.poliza = '${poliza}'
            and
            re.activo = 1
            and
            r."idFlujoPoliza" = 58
            and
            re.numero = 1
            and
            (r."idEstadoPoliza" = 25 or r."idEstadoPoliza" = 4)
            ORDER BY
                r.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    static async postCancelacionENR(data) {
        return await historialCancelacionENRModel.create(data).then( data => {
            return data;
        })
    }
    static async getMotivosCancelacionENR(): Promise<any> {
        return await dbPostgres.query(`
        SELECT * FROM operaciones."catalogoENR"
        `, {
            type: QueryTypes.SELECT,
            plain: false,
        });
    }
    static async getAllPolizasCanceladasENR(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT operaciones."historialCancelacionENR"."idRegistro",
            operaciones."historialCancelacionENR".poliza,
            operaciones."historialCancelacionENR".fecha,
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" as "nombreEmpleado",
            operaciones."catalogoENR".descripcion
            FROM operaciones."historialCancelacionENR"
            INNER JOIN "recursosHumanos".empleado ON operaciones."historialCancelacionENR"."idEmpleadoBaja" = "recursosHumanos".empleado."id"
            INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
            INNER JOIN operaciones."catalogoENR" ON operaciones."historialCancelacionENR"."idMotivo" = operaciones."catalogoENR"."id"
            WHERE
            operaciones."historialCancelacionENR".fecha BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
        `, {
            type: QueryTypes.SELECT,
        });
    }
}
