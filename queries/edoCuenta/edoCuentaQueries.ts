import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class EdoCuentaQueries {
    static async edoCuentaRegistro(fechaInicio, fechaFin) {
        return await dbPostgres.query(`
            SELECT
    operaciones.registro.id,
        operaciones.registro.poliza,
        operaciones.registro."fechaInicio",
        operaciones.registro."fechaRegistro",
        operaciones.registro."idEmpleado",
        concat_ws( ' ', "recursosHumanos".precandidato."apellidoPaterno", "recursosHumanos".precandidato."apellidoMaterno", "recursosHumanos".precandidato.nombre ) AS "nombreEjecutivo",
        operaciones.socios."nombreComercial",
        operaciones.registro."idDepartamento",
        "recursosHumanos".departamento.descripcion AS departamento,
        CASE WHEN ( operaciones.pagos."idEstadoPago" = 1 ) AND ( operaciones.recibos."idEstadoRecibos" = 5 ) AND ( operaciones.registro."idEstadoPoliza" = 6 )
    THEN 1 ELSE 0 END AS comisionable,
        CASE WHEN operaciones.pagos."idEstadoPago" = 2
    THEN 0 ELSE 1 END AS pagado,
        CASE WHEN operaciones.registro."idEstadoPoliza" <= 4
    THEN 0 ELSE 1 END AS autorizado,
        CASE WHEN operaciones.registro."idEstadoPoliza" <= 5
    THEN 0 ELSE 1 END AS verificado,
        CASE WHEN operaciones."verificacionRegistro".id IS NULL
    THEN 0 ELSE operaciones."verificacionRegistro"."verificadoCliente" END AS "verificadoCliente",
        CASE WHEN operaciones."verificacionRegistro".id IS NULL
    THEN 0 ELSE operaciones."verificacionRegistro"."verificadoProducto" END AS "verificadoProducto",
        CASE WHEN operaciones."verificacionRegistro".id IS NULL
    THEN 0 ELSE operaciones."verificacionRegistro"."verificadoRegistro" END AS "verificadoRegistro",
        CASE WHEN operaciones."verificacionRegistro".id IS NULL
    THEN 0 ELSE operaciones."verificacionRegistro"."verificadoPago" END AS "verificadoPago",
        CASE WHEN operaciones.recibos."idEstadoRecibos" = 5 THEN 1 ELSE 0 END AS aplicado,
        operaciones.recibos."fechaLiquidacion" AS "fechaAplicacionAseg",
        CASE WHEN operaciones.recibos."fechaAplicacion" IS NULL
    THEN operaciones.recibos."fechaLiquidacion" ELSE operaciones.recibos."fechaAplicacion" END AS "fechaAplicacion",
        '2022-01-30' AS "fechaComision",
        operaciones.pagos."fechaPago" AS "fechaPago"
    FROM
    operaciones.registro
    JOIN operaciones.recibos ON operaciones.registro.id = operaciones.recibos."idRegistro"
    LEFT JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
    LEFT JOIN operaciones."verificacionRegistro" ON operaciones.registro.id = operaciones."verificacionRegistro"."idRegistro"
    JOIN "recursosHumanos".empleado ON operaciones.recibos."idEmpleado" = "recursosHumanos".empleado.id
    JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    JOIN "recursosHumanos".departamento ON operaciones.registro."idDepartamento" = "recursosHumanos".departamento.id
    JOIN operaciones.socios ON operaciones.socios.id = operaciones.registro."idSocio"
    WHERE
    operaciones.registro."idFlujoPoliza" = 1
    AND operaciones.registro."idEstadoPoliza" <> 9
    AND operaciones.recibos.numero = 1
    AND operaciones.registro."fechaInicio" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
    ORDER BY
    operaciones.registro."fechaRegistro" DESC`, {
            type: QueryTypes.SELECT
        })
    }
}
