import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";
// lo usa cotizador  online
export default class StepCotizadorQueries {
    static async getSolicitudes(idSolicitud): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones."productoCliente".id "idProductoCliente",
                operaciones."productoCliente"."idCliente",
                operaciones."registro".id "idRegistro",
                COUNT(DISTINCT operaciones."recibos".id) "Recibos",
                COUNT(DISTINCT operaciones."pagos".id) "pagos"
            FROM operaciones.solicitudes
                LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                LEFT JOIN operaciones."registro" ON operaciones.registro."idProducto" = operaciones."productoCliente".id
                LEFT JOIN operaciones."recibos" ON operaciones.recibos."idRegistro" = operaciones."registro".id
                LEFT JOIN operaciones."pagos" ON operaciones.pagos."idRecibo" = operaciones."recibos".id
            WHERE operaciones.solicitudes.id = ${idSolicitud}
            GROUP BY
                operaciones.solicitudes.id,
                operaciones."productoCliente".id,
                operaciones."registro".id
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getRegistro(idRegistro): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                operaciones.solicitudes.id,
                operaciones."productoCliente".id "idProductoCliente",
                operaciones."productoCliente"."idCliente",
                operaciones."registro".id "idRegistro",
                COUNT(DISTINCT operaciones."recibos".id) "Recibos",
                COUNT(DISTINCT operaciones."pagos".id) "pagos"
            FROM operaciones.solicitudes
                LEFT JOIN operaciones."productoCliente" ON operaciones.solicitudes.id = operaciones."productoCliente"."idSolicitud"
                LEFT JOIN operaciones."registro" ON operaciones.registro."idProducto" = operaciones."productoCliente".id
                LEFT JOIN operaciones."recibos" ON operaciones.recibos."idRegistro" = operaciones."registro".id
                LEFT JOIN operaciones."pagos" ON operaciones.pagos."idRecibo" = operaciones."recibos".id
            WHERE operaciones.registro.id = ${idRegistro}
            GROUP BY
                operaciones.solicitudes.id,
                operaciones."productoCliente".id,
                operaciones."registro".id
            ORDER BY operaciones.solicitudes.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
}
