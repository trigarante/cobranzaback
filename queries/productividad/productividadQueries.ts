import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class ProductividadQueries {
    static async getDepartamentos() {
        return await dbPostgres.query(`
        SELECT
            departamento.id "idSubarea",
            departamento.descripcion subarea
        FROM "recursosHumanos".area INNER JOIN "recursosHumanos".departamento ON area."id" = departamento."idArea"
        WHERE "idArea" IN  (47, 48, 52, 53)
        ORDER BY descripcion ASC`, {
            type: QueryTypes.SELECT
        })
    }
    static async productividad(fechaInicio, fechaFin) {
        return await dbPostgres.query(`
SELECT
DISTINCT ON(operaciones.solicitudes."idEmpleado",
"recursosHumanos".departamento.descripcion)
operaciones.solicitudes."idEmpleado",
"recursosHumanos".departamento.descripcion AS departamento,
concat_ws(' ',"recursosHumanos".precandidato.nombre, "recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno") AS "nombreEjecutivo",
"solicitudesEmpleado".contactos,
CURRENT_DATE-DATE("recursosHumanos".empleado."fechaIngreso") AS antiguedad,
CASE WHEN "pagosVN"."polizasEmitidas" IS NULL
THEN 0 ELSE "pagosVN"."polizasEmitidas" END AS "polizasEmitidas",
  CASE WHEN "pagosVN"."primaNetaEmitida" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaEmitida"  END AS "primaNetaEmitida",
  CASE WHEN "pagosVN"."polizasCobradas" IS NULL
THEN 0 ELSE "pagosVN"."polizasCobradas" END AS "polizasCobradas",
  CASE WHEN "pagosVN"."primaNetaCobrada" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaCobrada" END AS "primaNetaCobrada",
  CASE WHEN "pagosVN"."polizasAplicadas" IS NULL
THEN 0 ELSE "pagosVN"."polizasAplicadas" END AS "polizasAplicadas",
  CASE WHEN "pagosVN"."primaNetaAplicada" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaAplicada" END AS "primaNetaAplicada"
FROM
operaciones.solicitudes
INNER JOIN operaciones."cotizacionesAliSnJson" ON operaciones."cotizacionesAliSnJson".id = operaciones.solicitudes."idCotizacionAli"
INNER JOIN operaciones.cotizaciones ON operaciones."cotizacionesAliSnJson"."idCotizacion" = operaciones.cotizaciones.id
INNER JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza"="recursosHumanos".plaza.id
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento"="recursosHumanos".departamento.id
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato"="recursosHumanos".candidato.id
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato"="recursosHumanos".precandidato.id
LEFT JOIN "operaciones"."medioDifusion" ON operaciones.cotizaciones."idMedioDifusion" = operaciones."medioDifusion".id
LEFT JOIN (
SELECT
operaciones.recibos."idEmpleado" AS "idEmpleado",
COUNT(operaciones.recibos.id) AS "polizasEmitidas",
SUM(operaciones.recibos.cantidad ) AS "primaNetaEmitida",
SUM(CASE WHEN operaciones.pagos."idEstadoPago" = 1
THEN 1 ELSE 0 END) AS "polizasCobradas",
SUM(CASE WHEN operaciones.pagos."idEstadoPago" = 1
THEN operaciones.recibos.cantidad ELSE 0 END) AS "primaNetaCobrada",
SUM(CASE WHEN operaciones.recibos."idEstadoRecibos"=5
THEN 1 ELSE 0 END) AS "polizasAplicadas",
SUM(CASE WHEN operaciones.recibos."idEstadoRecibos" = 5
THEN operaciones.recibos.cantidad ELSE 0 END) AS "primaNetaAplicada"
FROM
operaciones.registro
INNER JOIN operaciones.recibos ON operaciones.registro.id = operaciones.recibos."idRegistro"
INNER JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
WHERE
operaciones.registro."idEstadoPoliza" <> 9
AND operaciones.recibos.activo = 1
AND operaciones.recibos.numero = 1
AND operaciones.pagos."fechaPago" BETWEEN DATE '${fechaInicio}' AND DATE '${fechaFin}'
GROUP BY operaciones.recibos."idEmpleado"
) AS "pagosVN" ON "pagosVN"."idEmpleado" = operaciones.solicitudes."idEmpleado"
LEFT JOIN (
SELECT
operaciones.solicitudes."idEmpleado",
COUNT(operaciones.solicitudes."id") as contactos
FROM operaciones.solicitudes
WHERE
DATE (operaciones.solicitudes."fechaSolicitud" ) BETWEEN DATE '${fechaInicio}'
AND DATE '${fechaFin}'
AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
GROUP BY 1
) "solicitudesEmpleado" ON "solicitudesEmpleado"."idEmpleado" = operaciones.solicitudes."idEmpleado"
WHERE
DATE (operaciones.solicitudes."fechaSolicitud" ) BETWEEN DATE '${fechaInicio}'
AND DATE '${fechaFin}'
AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
AND "recursosHumanos".departamento."idArea" IN (20,34,35,39,40,41,47,48,52,58)`, {
            type: QueryTypes.SELECT
        })
    }

    static async productividadDepartamento(idDepartamento,fechaInicio, fechaFin) {
        return await dbPostgres.query(`
SELECT
DISTINCT ON(operaciones.solicitudes."idEmpleado",
"recursosHumanos".departamento.descripcion)
operaciones.solicitudes."idEmpleado",
"recursosHumanos".departamento.descripcion AS departamento,
concat_ws(' ',"recursosHumanos".precandidato.nombre, "recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno") AS "nombreEjecutivo",
"solicitudesEmpleado".contactos,
CURRENT_DATE-DATE("recursosHumanos".empleado."fechaIngreso") AS antiguedad,
CASE WHEN "pagosVN"."polizasEmitidas" IS NULL
THEN 0 ELSE "pagosVN"."polizasEmitidas" END AS "polizasEmitidas",
  CASE WHEN "pagosVN"."primaNetaEmitida" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaEmitida"  END AS "primaNetaEmitida",
  CASE WHEN "pagosVN"."polizasCobradas" IS NULL
THEN 0 ELSE "pagosVN"."polizasCobradas" END AS "polizasCobradas",
  CASE WHEN "pagosVN"."primaNetaCobrada" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaCobrada" END AS "primaNetaCobrada",
  CASE WHEN "pagosVN"."polizasAplicadas" IS NULL
THEN 0 ELSE "pagosVN"."polizasAplicadas" END AS "polizasAplicadas",
  CASE WHEN "pagosVN"."primaNetaAplicada" IS NULL
THEN 0 ELSE "pagosVN"."primaNetaAplicada" END AS "primaNetaAplicada"
FROM
operaciones.solicitudes
INNER JOIN operaciones."cotizacionesAliSnJson" ON operaciones."cotizacionesAliSnJson".id = operaciones.solicitudes."idCotizacionAli"
INNER JOIN operaciones.cotizaciones ON operaciones."cotizacionesAliSnJson"."idCotizacion" = operaciones.cotizaciones.id
INNER JOIN "recursosHumanos".empleado ON operaciones.solicitudes."idEmpleado" = "recursosHumanos".empleado.id
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza"="recursosHumanos".plaza.id
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento"="recursosHumanos".departamento.id
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato"="recursosHumanos".candidato.id
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato"="recursosHumanos".precandidato.id
LEFT JOIN "operaciones"."medioDifusion" ON operaciones.cotizaciones."idMedioDifusion" = operaciones."medioDifusion".id
LEFT JOIN (
SELECT
operaciones.recibos."idEmpleado" AS "idEmpleado",
COUNT(operaciones.recibos.id) AS "polizasEmitidas",
SUM(operaciones.recibos.cantidad ) AS "primaNetaEmitida",
SUM(CASE WHEN operaciones.pagos."idEstadoPago" = 1
THEN 1 ELSE 0 END) AS "polizasCobradas",
SUM(CASE WHEN operaciones.pagos."idEstadoPago" = 1
THEN operaciones.recibos.cantidad ELSE 0 END) AS "primaNetaCobrada",
SUM(CASE WHEN operaciones.recibos."idEstadoRecibos"=5
THEN 1 ELSE 0 END) AS "polizasAplicadas",
SUM(CASE WHEN operaciones.recibos."idEstadoRecibos" = 5
THEN operaciones.recibos.cantidad ELSE 0 END) AS "primaNetaAplicada"
FROM
operaciones.registro
INNER JOIN operaciones.recibos ON operaciones.registro.id = operaciones.recibos."idRegistro"
INNER JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
WHERE
operaciones.registro."idEstadoPoliza" <> 9
AND operaciones.recibos.activo = 1
AND operaciones.recibos.numero = 1
AND operaciones.pagos."fechaPago" BETWEEN DATE '${fechaInicio}' AND DATE '${fechaFin}'
GROUP BY operaciones.recibos."idEmpleado"
) AS "pagosVN" ON "pagosVN"."idEmpleado" = operaciones.solicitudes."idEmpleado"
LEFT JOIN (
SELECT
operaciones.solicitudes."idEmpleado",
COUNT(operaciones.solicitudes."id") as contactos
FROM operaciones.solicitudes
WHERE
DATE (operaciones.solicitudes."fechaSolicitud" ) BETWEEN DATE '${fechaInicio}'
AND DATE '${fechaFin}'
AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
GROUP BY 1
) "solicitudesEmpleado" ON "solicitudesEmpleado"."idEmpleado" = operaciones.solicitudes."idEmpleado"
WHERE
DATE (operaciones.solicitudes."fechaSolicitud" ) BETWEEN DATE '${fechaInicio}'
AND DATE '${fechaFin}'
AND operaciones.solicitudes."idEstadoSolicitud" NOT IN (5,21)
AND "recursosHumanos".departamento."id" = ${idDepartamento}` , {
            type: QueryTypes.SELECT
        })
    }
}


