import EmpleadosModel from "../../models/venta-nueva/empleados/EmpleadosModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class EmpleadosQueries {
    static async getById(id) {
        return await EmpleadosModel.findOne({
            where: {
                id
            },
        })
    }


    // Queries de empleados desarrollo organizacional

    static async getCorreoPersonal(idEmpleado){
        return await dbPostgres.query(`
            SELECT p.email
            FROM "recursosHumanos".empleado e
                JOIN "recursosHumanos".candidato c on e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato p on c."idPrecandidato" = p.id
            WHERE
            e.id = ${idEmpleado}
            `,{
            type: QueryTypes.SELECT,
            plain: true
        })

    }

    // FIN

    static async getEmpleadosByIdDepartamento(...idDepartamento){
        return await dbPostgres.query(`
            SELECT
                empleado.id,
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" as nombre
            FROM
                "recursosHumanos".departamento
                INNER JOIN "recursosHumanos".plaza ON departamento.id = "plaza"."idDepartamento"
                INNER JOIN "recursosHumanos".empleado ON plaza."id" = "empleado"."idPlaza"
                INNER JOIN "recursosHumanos".candidato ON candidato.id = "empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON precandidato.id = "candidato"."idPrecandidato"
                INNER JOIN operaciones.usuarios ON departamento."id" = usuarios."idDepartamento" AND usuarios."idEmpleado" = empleado."id"
            WHERE
                departamento."id" IN (${idDepartamento})
                AND empleado.activo = 1
                AND plaza."idPuesto" = 7
                AND usuarios.usuario IS NOT NULL
                AND precandidato."idEstadoRH" = 1
            `,{
            type: QueryTypes.SELECT,
        })
    }

    static async getActivos(query){
        return await dbPostgres.query(`
           SELECT
            "recursosHumanos".empleado."id",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno" AS nombre,
            "recursosHumanos".empleado."fechaIngreso",
            "recursosHumanos".sede.nombre AS sede,
            "recursosHumanos".departamento.descripcion,
            "recursosHumanos".puesto.nombre AS puesto,
            MAX ( "carruselEmpleados".contador ) AS contador,
            CASE

            WHEN MAX ( "carruselEmpleados".permiso ) = 1 THEN
            'ACTIVO' ELSE'INACTIVO'
            END permiso,
            "solicitudesCarrusel".ID AS "idSolicitudesCarrusel",
            MAX ( "carruselEmpleados".permiso ) "permisoNuevo"
        FROM
            "recursosHumanos".empleado
            INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id"
            INNER JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza" = "recursosHumanos".plaza."id"
            INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento" = "recursosHumanos".departamento."id"
            INNER JOIN "recursosHumanos".puesto ON "recursosHumanos".plaza."idPuesto" = "recursosHumanos".puesto."id"
            INNER JOIN "recursosHumanos"."carruselEmpleados" ON "recursosHumanos".empleado."id" = "recursosHumanos"."carruselEmpleados"."idEmpleado"
            INNER JOIN "recursosHumanos".sede ON "recursosHumanos".plaza."idSede" = "recursosHumanos".sede."id"
            LEFT JOIN operaciones."solicitudesCarrusel" ON "recursosHumanos".empleado."id" = operaciones."solicitudesCarrusel"."idEmpleado"
            AND "solicitudesCarrusel"."idEstadoSolicitudCarrusel" < 3
            INNER JOIN operaciones.usuarios ON usuarios."idEmpleado" = empleado.ID
        WHERE
            usuarios.usuario IS NOT NULL
            AND precandidato."idEstadoRH" = 1 ${query}
        GROUP BY
            empleado."id",
            "recursosHumanos".precandidato.nombre || ' ' || "recursosHumanos".precandidato."apellidoPaterno" || ' ' || "recursosHumanos".precandidato."apellidoMaterno",
            sede.nombre,
            departamento.descripcion,
            puesto.nombre,
            "solicitudesCarrusel"."id"
        ORDER BY
            empleado."id" ASC
            `,{
            type: QueryTypes.SELECT,
        })

    }
}
