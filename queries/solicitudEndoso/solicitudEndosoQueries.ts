import SolicitudEndosoModel from "../../models/venta-nueva/solicitudEndoso/SolicitudEndosoModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SolicitudEndosoQueries {
    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario"
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    // solicitudEndoso
    static async getAll(fechaInicio, fechaFin,): Promise<any> {
        return await dbPostgres.query(`
           SELECT
                se.id,
                r.poliza,
                CASE when pr.nombre is not null then pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" else 'SIN EMPLEADO' end AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
            FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                LEFT JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                LEFT JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                LEFT JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleadoSolicitante" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."productoCliente" ON r."idProducto" = "productoCliente"."id"
                JOIN operaciones.solicitudes ON "productoCliente"."idSolicitud" = solicitudes."id"
                JOIN operaciones."estadoSolicitud" esol ON solicitudes."idEstadoSolicitud" = esol."id"
            WHERE
                se."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // solicitudEndoso
    static async getAllByDepartamentos(fechaInicio, fechaFin, ...departamentos): Promise<any> {
        return await dbPostgres.query(`
             SELECT
                se.id,
                r.poliza,
                CASE when pr.nombre is not null then pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" else 'SIN EMPLEADO' end AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
            FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                LEFT JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                LEFT JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                LEFT JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleadoSolicitante" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."productoCliente" ON r."idProducto" = "productoCliente"."id"
                JOIN operaciones.solicitudes ON "productoCliente"."idSolicitud" = solicitudes."id"
                JOIN operaciones."estadoSolicitud" esol ON solicitudes."idEstadoSolicitud" = esol."id"
            WHERE
                se."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                AND
                r."idDepartamento" IN (${departamentos})
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }
    // solicitudEndoso
    static async getAllByDepartamentoAndIdEmpleado(idDepartamento,idEmpleado, fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                se.id,
                r.poliza,
                CASE when pr.nombre is not null then pr.nombre || ' ' || pr."apellidoPaterno" || ' ' || pr."apellidoMaterno" else 'SIN EMPLEADO' end AS "Empleado",
                "precandidatoSolicitante".nombre || ' ' || "precandidatoSolicitante"."apellidoPaterno" || ' ' || "precandidatoSolicitante"."apellidoMaterno" AS "EmpleadoSolicitante",
                "precandidatoEmisor".nombre || ' ' || "precandidatoEmisor"."apellidoPaterno" || ' ' || "precandidatoEmisor"."apellidoMaterno" AS "emisorPoliza",
                esen.descripcion AS "estadoEndoso",
                esol.estado AS "estado",
                dp.descripcion AS departamento,
                se."fechaSolicitud"
            FROM generales."solicitudEndoso" se
                JOIN generales."estadoEndoso" esen ON se."idEstadoEndoso" = esen.id
                JOIN operaciones.registro r ON se."idRegistro" = r.id
                LEFT JOIN "recursosHumanos".empleado e ON se."idEmpleado" = e.id
                LEFT JOIN "recursosHumanos".candidato c ON e."idCandidato" = c.id
                LEFT JOIN "recursosHumanos".precandidato pr ON c."idPrecandidato" = pr.id
                JOIN "recursosHumanos".empleado "empleadoSolicitante" ON se."idEmpleadoSolicitante" = "empleadoSolicitante".id
                JOIN "recursosHumanos".candidato "candidatoSolicitante" ON "empleadoSolicitante"."idCandidato" = "candidatoSolicitante".id
                JOIN "recursosHumanos".precandidato "precandidatoSolicitante" ON "candidatoSolicitante"."idPrecandidato" = "precandidatoSolicitante".id
                JOIN "recursosHumanos".empleado "empleadoEmisor" ON r."idEmpleado" = "empleadoEmisor".id
                JOIN "recursosHumanos".candidato "candidatoEmisor" ON "empleadoEmisor"."idCandidato" = "candidatoEmisor".id
                JOIN "recursosHumanos".precandidato "precandidatoEmisor" ON "candidatoEmisor"."idPrecandidato" = "precandidatoEmisor".id
                JOIN "recursosHumanos".departamento dp ON r."idDepartamento" = dp.id
                JOIN operaciones."productoCliente" ON r."idProducto" = "productoCliente"."id"
                JOIN operaciones.solicitudes ON "productoCliente"."idSolicitud" = solicitudes."id"
                JOIN operaciones."estadoSolicitud" esol ON solicitudes."idEstadoSolicitud" = esol."id"
            WHERE
                se."fechaSolicitud" BETWEEN date('${fechaInicio}') AND date('${fechaFin}')
                -- AND dp.id IN (${idDepartamento})
                AND
                se."idEmpleado" = ${idEmpleado}
                ORDER by se.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async post(data){
        return await SolicitudEndosoModel.create(data).then(data => {
            return data['id'];
        })
    }
}
