import TipoPagoModel from "../../models/venta-nueva/solicitudes/steps/registro-poliza/tipoPagoModel";

export default class TipoPagoQueries {
    static async getActivos() {
        return await TipoPagoModel.findAll({
            where: {activo: 1},
        })
    }
}
