import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

export default class CrearClienteQueries {

    static async findByCurp(curp: string): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.cliente
            WHERE
                 operaciones.cliente.curp = '${curp}'
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findByrfc(rfc: string): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.cliente
            WHERE
                 operaciones.cliente.rfc = '${rfc}'
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getPaises(): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones.paises
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
// lo usa cotizador  online
    static async getColoniaByCp(cp: any): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones."cpSepomex"
            WHERE
                operaciones."cpSepomex".cp = ${cp}
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    // static async updateCliente(idCliente: number, data: ClienteModel): Promise<any> {
    //     return await ClienteModel.update({...data}, {
    //         where: {
    //             id: idCliente,
    //         }
    //     });
    // }
}
