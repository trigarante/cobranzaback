import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SubRamoQueries {
    static async getByIdRamo(idRamo) {
        return await  dbPostgres.query(`
        SELECT operaciones."subRamo".id, operaciones."subRamo"."idRamo", operaciones."tipoSubRamo".tipo AS "tipoSubRamo" FROM operaciones."subRamo"
        INNER JOIN operaciones."tipoSubRamo" ON operaciones."tipoSubRamo".id = operaciones."subRamo"."idTipoSubRamo"
        WHERE operaciones."subRamo"."idRamo" = '${idRamo}' AND operaciones."subRamo".activo =1`, {
            type: QueryTypes.SELECT,
        })
    }
// lo usa cotizador  online
//     static async getAlias(idTipoSubRamo, alias) {
//         return await SubRamoViewModel.findAll({
//             where: {idTipoSubRamo, alias,  activo: 1}
//         })
//     }

    static async getAlias(idTipoSubRamo, alias) : Promise<any> {
        return await dbPostgres.query(`
         SELECT "subRamo".id,
                "subRamo"."idRamo",
                "subRamo"."idTipoSubRamo",
                "socios"."alias"
                FROM operaciones."subRamo"
                JOIN operaciones."ramo" ON "subRamo"."idRamo" = "ramo".id
                JOIN operaciones."socios" ON ramo."idSocio" = socios.id
                WHERE "subRamo"."activo" = 1 AND "subRamo"."idTipoSubRamo" = ${idTipoSubRamo} AND "socios"."alias" = '${alias}'

               ORDER BY "subRamo".id DESC;
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })

    }
    // lo usa cotizador  online
    // static async getSubramobyId(id) {
    //     return await SubRamoViewModel.findOne({
    //         where: {id, activo: 1}
    //     })
    // }
    static async getSubramobyId(id) : Promise<any> {
        return await dbPostgres.query(`
         SELECT "subRamo".id,
       "subRamo"."idRamo",
       "subRamo"."idTipoSubRamo",
        "socios"."alias",
        "socios".id AS "idSocio",
       "tipoSubRamo".tipo AS "tipoSubRamo"
       FROM operaciones."subRamo"
        JOIN operaciones."ramo" ON "subRamo"."idRamo" = "ramo".id
        JOIN operaciones."socios" ON ramo."idSocio" = socios.id
        JOIN operaciones."tipoSubRamo" ON "subRamo"."idTipoSubRamo" = "tipoSubRamo".id

        WHERE "subRamo".activo = 1 AND "subRamo"."id" = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    // lo usa cotizador  online
    // static async getByIdRamoView(idRamo) {
    //     return await SubRamoViewModel.findAll({
    //         where: {idRamo, activo: 1}
    //     })
    // }
    static async getByIdRamoView(idRamo): Promise<any> {
        return await dbPostgres.query(`
         SELECT "subRamo".id,
       "subRamo"."idRamo",
       "subRamo"."idTipoSubRamo",
        "socios"."alias",
        "socios".id AS "idSocio",
       "tipoSubRamo".tipo AS "tipoSubRamo"
       FROM operaciones."subRamo"
        JOIN operaciones."ramo" ON "subRamo"."idRamo" = "ramo".id
        JOIN operaciones."socios" ON ramo."idSocio" = socios.id
        JOIN operaciones."tipoSubRamo" ON "subRamo"."idTipoSubRamo" = "tipoSubRamo".id

        WHERE "subRamo".activo = 1 AND "subRamo"."idRamo" = ${idRamo}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

}
