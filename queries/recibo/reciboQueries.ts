import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import RecibosModel from "../../models/venta-nueva/recibo/recibosModel";
import ProductoClienteModel from "../../models/venta-nueva/solicitudes/steps/producto-cliente/productoClienteModel";

export default class ReciboQueries {

    static async forPagosByIdRecibo(id): Promise<any> {
        return await dbPostgres.query(`
           	Select
                re.id,
                re.cantidad,
                re.numero
                FROM
                operaciones.recibos re
                Where
                re.activo = 1 and re.numero >= 1
                and
                re."id" = ${id}

        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    
    static async findAllByIdRegistroAndActivo(id, query): Promise<any> {
        return await dbPostgres.query(`
           Select
            re.numero,
            re.cantidad,
            re."fechaVigencia",
            ep.estado,
            r.poliza,
            pa.id AS "idPago",
            pa."fechaRegistro",
            "precandidatoPago".nombre AS "nombreEmpleadoPago",
            "precandidatoPago"."apellidoPaterno" AS "apellidoPaternoEmpleadoPago",
            "precandidatoPago"."apellidoMaterno" AS "apellidoMaternoEmpleadoPago",
            re."fechaLiquidacion",
            pa."fechaPago",
            re.id,
            r.id as "idRegistro",
            prepago.id "idPrepago",
            prepago.url
            FROM
            operaciones.recibos re
            INNER JOIN operaciones.registro r ON re."idRegistro" = r.id
            INNER JOIN operaciones."estadoPoliza" ep ON r."idEstadoPoliza" = ep.id
            LEFT JOIN operaciones.pagos pa ON re.id = pa."idRecibo"
            LEFT JOIN "recursosHumanos".empleado "empleadoPago" ON pa."idEmpleado" = "empleadoPago"."id"
            LEFT JOIN "recursosHumanos".candidato "candidatoPago" ON "empleadoPago"."idCandidato" = "candidatoPago"."id"
            LEFT JOIN "recursosHumanos".precandidato "precandidatoPago" ON "candidatoPago"."idPrecandidato" = "precandidatoPago"."id"
            LEFT JOIN operaciones.prepago on r.id = prepago."idRegistro"
            Where
            re.activo = 1
            and
            r.id = ${id}
            ${query}
            Order by re.id Asc

        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async post(data) {
        return await RecibosModel.create(data).catch(err => console.log(err))
    }

    static async forPagosByidRegistro(id): Promise<any> {
        return await dbPostgres.query(`
           	Select
                re.id,
                re.cantidad
                FROM
                operaciones.recibos re
                Where
                re.activo = 1 and re.numero = 1
                and
                re."idRegistro" = ${id}

        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    static async updateBaja(id, data) {
        return await RecibosModel.update({...data}, {
            where: {
                idRegistro: id,
            }
        })
    }
}
