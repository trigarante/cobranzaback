import SolicitudesCarruselModel from "../../models/venta-nueva/activacionEmpleado/solicitudesCarruselModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class SolicitudesCarruselQueries {
    static async post(data) {
        return await SolicitudesCarruselModel.create(data);
    }

    static async getByIdEstadoSolicitud(idEstado, query) {
        return await dbPostgres.query(`
        SELECT
            "solicitudesCarrusel"."id",
            "solicitudesCarrusel"."idMotivoDesactivacion",
            "solicitudesCarrusel"."idEstadoSolicitudCarrusel",
            "solicitudesCarrusel"."fechaRegistro",
            precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" AS ejecutivo,
            "precandidatoS".nombre || ' ' || "precandidatoS"."apellidoPaterno" || ' ' || "precandidatoS"."apellidoMaterno" AS supervisor,
            empleado."fechaIngreso",
            sede.nombre AS sede,
            departamento.descripcion AS departamento,
            puesto.nombre AS puesto,
            MAX ( "carruselEmpleados".contador ) AS contador,
            CASE

            WHEN MAX ( "carruselEmpleados".permiso ) = 1 THEN
            'ACTIVO' ELSE'INACTIVO'
            END permiso,
            "motivosDesactivacion".descripcion AS "motivoDesactivacion",
            "tipoMotivo".descripcion as motivo
        FROM
            operaciones."solicitudesCarrusel"
            INNER JOIN "recursosHumanos".empleado ON operaciones."solicitudesCarrusel"."idEmpleado" = "recursosHumanos".empleado."id"
            INNER JOIN generales."motivosDesactivacion" ON operaciones."solicitudesCarrusel"."idMotivoDesactivacion" = generales."motivosDesactivacion"."id"
            INNER JOIN generales."tipoMotivo" ON generales."motivosDesactivacion"."idTipoMotivo" = generales."tipoMotivo"."id"
            INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato."id"
            INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato."id" ----
            LEFT JOIN "recursosHumanos".empleado "empleadoS" ON operaciones."solicitudesCarrusel"."idEmpleadoSolicitante" = "empleadoS"."id"
            LEFT JOIN "recursosHumanos".candidato "candidatoS" ON "empleadoS"."idCandidato" = "candidatoS"."id"
            LEFT JOIN "recursosHumanos".precandidato "precandidatoS" ON "candidatoS"."idPrecandidato" = "precandidatoS"."id" ---
            INNER JOIN "recursosHumanos".plaza ON plaza.ID = empleado."idPlaza"
            INNER JOIN "recursosHumanos".sede ON sede.ID = plaza."idSede"
            INNER JOIN "recursosHumanos".departamento ON plaza."idDepartamento" = departamento."id"
            INNER JOIN "recursosHumanos".puesto ON plaza."idPuesto" = puesto."id"
            INNER JOIN "recursosHumanos"."carruselEmpleados" ON "solicitudesCarrusel"."idEmpleado" = "carruselEmpleados"."idEmpleado"
        WHERE
            "solicitudesCarrusel"."idEstadoSolicitudCarrusel" = 1 ${query}
        GROUP BY
            "solicitudesCarrusel"."id",
            "solicitudesCarrusel"."idMotivoDesactivacion",
            "solicitudesCarrusel"."idEstadoSolicitudCarrusel",
            precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno",
            "precandidatoS".nombre || ' ' || "precandidatoS"."apellidoPaterno" || ' ' || "precandidatoS"."apellidoMaterno",
            "solicitudesCarrusel"."fechaRegistro",
            empleado."fechaIngreso",
            sede.nombre,
            departamento.descripcion,
            puesto.nombre,
            "motivosDesactivacion".descripcion,
            "motivosDesactivacion"."idTipoMotivo",
            "tipoMotivo".descripcion`, {
            type: QueryTypes.SELECT
        })
    }

    static async update(data, id) {
        return await SolicitudesCarruselModel.update(data, {
            where: {id},
        })
    }
}
