import MotivosDesactivacionModel from "../../models/venta-nueva/activacionEmpleado/motivosDesactivacionModel";

export default class MotivosDesactivacionQueries {
    static async getActivosByIdTipo(idTipoMotivo) {
        return await MotivosDesactivacionModel.findAll({
            where: {
                activo: 1, idTipoMotivo
            }
        })
    }
}
