import LogQualitasEmisionModel from "../../models/venta-nueva/LogCotizador/LogQualitasEmisionModel";
import LogQualitasPagoModel from "../../models/venta-nueva/LogCotizador/LogQualitasPagoModel";
import LogGnpEmisionModel from "../../models/venta-nueva/LogCotizador/LogGnpEmisionModel";
import LogGnpPagoModel from "../../models/venta-nueva/LogCotizador/LogGnpPagoModel";

export default class LogCotizadorQueries {
    static async postQualitasEmision(data) {
        return await LogQualitasEmisionModel.create(data).then(data => {
            return data['id'];
        });
    }

    static async postQualitasPago(data) {
        return await LogQualitasPagoModel.create(data).then(data => {
            return data['id'];
        });
    }

    static async postGnpEmision(data) {
        return await LogGnpEmisionModel.create(data).then(data => {
            return data['id'];
        });
    }

    static async postGnpPago(data) {
        return await LogGnpPagoModel.create(data).then(data => {
            return data['id'];
        });
    }

}