import express, {Application} from 'express';
import cors from 'cors';
import routes from "../routes/index";
const morgan = require('morgan');
import helmet from 'helmet';
import {dbPostgres} from "../configs/connection";
const fileUpload = require('express-fileupload');
const swaggerUI = require('swagger-ui-express');
const swaggerFile = require('../swagger-output.json');

class Server {

    private app: Application;
    private port: string;

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8080';

        // Métodos iniciales
        this.dbConnection();
        this.middlewares();
        this.routes();
    }

    async dbConnection() {

        try {
            await dbPostgres.authenticate();
            console.log('Database postgres online');
            console.log('Database online');
        } catch (error) {
            throw new Error(error);
        }

    }

    middlewares() {
        // JWT
        this.app.set('llave', process.env.llave)

        // Morgan
        this.app.use(morgan('tiny'));

        // CORS
        this.app.use(cors());

        // Helmet
        this.app.use(helmet());

        // Disable header
        this.app.disable('x-powered-by');

        // FileUpload
        this.app.use(fileUpload());

        // Lectura del body
        this.app.use(express.json());

        // Carpeta pública
        this.app.use(express.static('public'));
    }


    routes() {
        this.app.use("/v1/", routes)
        this.app.use(
            '/api-docs',
            swaggerUI.serve,
            swaggerUI.setup(swaggerFile)
        );
    }


    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        })
    }

}

export default Server;
