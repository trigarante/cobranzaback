import {DataTypes, Model} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const AsistenciaSabatinaModel = dbPostgres.define('AsistenciaSabatinaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idArea: {
            type: DataTypes.INTEGER
        },
        idDepartamento: {
            type: DataTypes.INTEGER
        },
        activo: {
            type: DataTypes.INTEGER
        }
    },
    {
        tableName: "asistenciaSabado",
        schema: "recursosHumanos",
        timestamps: false,
    }
);

export default AsistenciaSabatinaModel;
