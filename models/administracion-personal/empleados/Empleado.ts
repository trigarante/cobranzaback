import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const Empleado = dbPostgres.define('EmpleadoModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idPlaza: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        idCandidato: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        fechaAltaIMSS: {
            type: DataTypes.DATE
        },
        documentos: {
            type: DataTypes.JSON,
        },
        fechaIngreso: {
            type: DataTypes.DATE
        },
        fechaRegistro: {
            type: DataTypes.DATE
        },
        fechaFiniquito: {
            type: DataTypes.DATE
        },
        idEstadoFiniquito: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        },
        montoFiniquito: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        },
        cantidadVacaciones: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        },
        activo: {
            type: DataTypes.SMALLINT
        },
        fechaBaja: {
            type: DataTypes.DATE
        },
        anios: {
            type: DataTypes.INTEGER
        },
        tarjetaSecundaria: {
            type: DataTypes.STRING
        },
    },
    {
        tableName: "empleado",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default Empleado;
