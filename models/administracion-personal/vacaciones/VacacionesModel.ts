import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const VacacionesModel = dbPostgres.define('vacaciones', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoSupervisor: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoADP: {
        type: DataTypes.INTEGER,
    },
    idEmpleadoEjecutivo: {
        type: DataTypes.INTEGER,
    },
    idEstadoVacacion: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    fechaFinal: {
        type: DataTypes.DATE
    },
    fechaFinalizacion: {
        type: DataTypes.DATE
    },
    fechaCancelacion: {
        type: DataTypes.DATE
    },
    // fechaReingreso: {
    //     type: DataTypes.DATE
    // },
    cantidadDias: {
        type: DataTypes.INTEGER,
    },
    comentarios: {
        type: DataTypes.STRING,
    },
    comentariosADP: {
        type: DataTypes.STRING,
    },
    diasTomados: {
        type: DataTypes.INTEGER,
    },
    comentariosEjecutivo: {
        type: DataTypes.STRING,
    },
}, {
    schema: "recursosHumanos",
    timestamps: false,
    tableName: "solicitudVacaciones",
});

export default VacacionesModel;
