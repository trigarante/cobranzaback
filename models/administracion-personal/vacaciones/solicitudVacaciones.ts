
import {Sequelize, Model, DataTypes} from "sequelize";
import {dbPostgres as db}  from "../../../configs/connection";
import EstadoSolicitudVacaciones from "../estadoSolicitudVacaciones";
import Empleado from "../empleados/Empleado";


const sequelize = db;

class SolicitudVacaciones extends Model {
    public id!: number; // Note that the `null assertion` `!` is required in strict mode.
    public idEmpleado!: number;
    public fechaRegistro!: string;
    public idFechaSolicitud!: number;
    public solicitud!: any;
    public idEstadoSolicitudVacaciones!: number;
    public idEmpleadoAutorizado!: string;
}

SolicitudVacaciones.init(
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idEmpleado: {
            type: DataTypes.INTEGER,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        idFechaSolicitud: {
            type: DataTypes.INTEGER,
        },
        solicitud: {
            type: DataTypes.JSON,
        },
        idEstadoSolicitudVacaciones: {
            type: DataTypes.INTEGER,
        },
        idEmpleadoAutorizado: {
            type: DataTypes.INTEGER,
        },
        idEmpleadoADP: {
            type: DataTypes.INTEGER,
        },
    },
    {
        tableName: "solicitudVacaciones",
        timestamps: false,
        sequelize, // passing the `sequelize` instance is required
    }
);

SolicitudVacaciones.belongsTo(EstadoSolicitudVacaciones, {targetKey: "id", foreignKey: "idEstadoSolicitudVacaciones"});
SolicitudVacaciones.belongsTo(Empleado, {targetKey: "id", foreignKey: "idEmpleado"});

export default SolicitudVacaciones;
