import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const RegistroAsistenciaModel  = dbPostgres.define('registroAsistenciaModel',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idEmpleado: {
            type: DataTypes.BIGINT,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        idEmpleadoSupervisor: {
            type: DataTypes.BIGINT,
        },
        idAsistencia: {
            type: DataTypes.BIGINT,
        },

    },
    {
        tableName: "registroAsistencia",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default RegistroAsistenciaModel;
