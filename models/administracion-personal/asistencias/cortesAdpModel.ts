import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const CortesAdpModel = dbPostgres.define('cortesAdp',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idEmpleado: {
            type: DataTypes.BIGINT,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        fechaInicial: {
            type: DataTypes.DATE,
        },
        fechaFinal: {
            type: DataTypes.DATE,
        },
        fechaCierre: {
            type: DataTypes.DATE,
        },
        idEmpleadoCierre: {
            type: DataTypes.BIGINT,
        },
        activo: {
            type: DataTypes.SMALLINT,
        },
    },
    {
        tableName: "cortesAdp",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default CortesAdpModel;

