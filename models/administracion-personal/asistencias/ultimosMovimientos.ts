import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const UltimosMovimientos = dbPostgres.define( 'ultimosMovimientos',
    {
        id: {
            type: DataTypes.SMALLINT,
            autoIncrement: true,
            primaryKey: true,
        },
        idCierre: {
            type: DataTypes.BIGINT,
        },
        idEmpleado: {
            type: DataTypes.BIGINT,
        },
        idEmpleadoSupervisor: {
            type: DataTypes.BIGINT,
        },
        idAsistencia: {
            type: DataTypes.BIGINT,
        },
        idEmpleadoADP: {
            type: DataTypes.BIGINT,
        },
        documento: {
            type: DataTypes.STRING,
        },
        idEstadoJustificacion: {
            type: DataTypes.BIGINT,
        },
        comentarios: {
            type: DataTypes.STRING,
        },
        comentariosADP: {
            type: DataTypes.STRING,
        },
        idMotivo: {
            type: DataTypes.BIGINT,
        },
        idEstadoAsistencia: {
            type: DataTypes.BIGINT,
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        fechaInicio: {
            type: DataTypes.DATE,
        },
        fechaFin: {
            type: DataTypes.DATE,
        },
        fechaADP: {
            type: DataTypes.DATE,
        },
        comentariosADP: {
            type: DataTypes.STRING,
        },
    },
    {
        tableName: "ultimosMovimientos",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default UltimosMovimientos;
