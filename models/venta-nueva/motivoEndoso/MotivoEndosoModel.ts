import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const MotivoEndosoModel = dbPostgres.define('motivoEndoso', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idTipoEndoso: {
        type: DataTypes.INTEGER
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "motivoEndoso",
    schema: "generales",
    timestamps: false
})

export default MotivoEndosoModel
