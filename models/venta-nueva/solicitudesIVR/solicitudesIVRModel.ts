import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const solicitudesIVRModel = dbPostgres.define('solicitudesIVR', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    poliza: {
        type: DataTypes.STRING
    },
    numeroRecibo: {
        type: DataTypes.STRING
    },
    titular: {
        type: DataTypes.STRING
    },
    numeroTarjeta: {
        type: DataTypes.STRING
    },
    fecha: {
        type: DataTypes.STRING
    },
    codigoVerificacion: {
        type: DataTypes.STRING
    },
    origen: {
        type: DataTypes.STRING
    },
    banco: {
        type: DataTypes.STRING
    },
    tipoTarjeta: {
        type: DataTypes.STRING
    },
    disenoTarjeta: {
        type: DataTypes.STRING
    },
    grabacion: {
        type: DataTypes.STRING
    },
    msi: {
        type: DataTypes.STRING
    }
}, {
    tableName: "prepagoTelefonia",
    schema: "operaciones",
    timestamps: false
})

export default solicitudesIVRModel
