import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../../configs/connection";

const ProductoClienteModel = dbPostgres.define('productoCliente', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idCliente: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER
    },
    idSubRamo: {
        type: DataTypes.INTEGER
    },
    datos: {
        type: DataTypes.JSON
    },
    idEmision: {
        type: DataTypes.INTEGER
    },
    noSerie: {
        type: DataTypes.STRING
    },
    archivo: {
        type: DataTypes.STRING,
    }
}, {
    tableName: "productoCliente",
    schema: "operaciones",
    timestamps: false
});
export default ProductoClienteModel;
