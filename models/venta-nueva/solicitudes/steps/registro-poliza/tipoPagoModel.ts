import { DataTypes } from "sequelize";
import {dbPostgres} from "../../../../../configs/connection";

const TipoPagoModel = dbPostgres.define('tipoPago', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    cantidadPagos: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    tipoPago: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: "tipoPago",
    schema: 'operaciones',
    timestamps: false
});

export default TipoPagoModel;
