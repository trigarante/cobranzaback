import {Sequelize, Model, DataTypes} from "sequelize";
import db from "../../../../../configs/connection";
import {dbPostgres} from "../../../../../configs/connection";

const sequelize = dbPostgres;
// lo usa cotizador  online
const ClienteModelOnline = dbPostgres.define('cliente', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idPais: {
        type: DataTypes.INTEGER
    },
    razonSocial: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    paterno: {
        type: DataTypes.STRING
    },
    materno: {
        type: DataTypes.STRING
    },
    cp: {
        type: DataTypes.STRING
    },
    calle: {
        type: DataTypes.STRING
    },
    numInt: {
        type: DataTypes.STRING
    },
    numExt: {
        type: DataTypes.STRING
    },
    idColonia: {
        type: DataTypes.INTEGER
    },
    genero: {
        type: DataTypes.STRING
    },
    telefonoFijo: {
        type: DataTypes.STRING
    },
    telefonoMovil: {
        type: DataTypes.STRING
    },
    correo: {
        type: DataTypes.STRING
    },
    fechaNacimiento: {
        type: DataTypes.DATE
    },
    curp: {
        type: DataTypes.STRING
    },
    rfc: {
        type: DataTypes.STRING
    },
    archivo: {
        type: DataTypes.STRING
    },
    archivoSubido: {
        type: DataTypes.INTEGER
    },
    ruc: {
        type: DataTypes.STRING
    },
    dni: {
        type: DataTypes.STRING
    },
    idColoniaPeru: {
        type: DataTypes.INTEGER
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "cliente",
});

// class ClienteModel extends Model {
//     public id!: number;
//     public idPais!: number;
//     public razonSocial!: number;
//     public nombre!: string;
//     public paterno!: string;
//     public materno!: string;
//     public cp!: number;
//     public calle!: string;
//     public numInt!: number;
//     public numExt!: number;
//     public idColonia!: number;
//     public genero!: string;
//     public telefonoFijo!: number;
//     public telefonoMovil!: number;
//     public correo!: string;
//     public fechaNacimiento!: Date;
//     public curp!: string;
//     public rfc!: string;
//     public archivo!: string;
//     public archivoSubido!: string;
//     public ruc!: string;
//     public dni!: string;
//     public idColoniaPeru!: number;
// }

// ClienteModel.init({
//     id: {
//         type: DataTypes.INTEGER.UNSIGNED,
//         autoIncrement: true,
//         primaryKey: true,
//     },
//     idPais: {
//         type: DataTypes.INTEGER
//     },
//     razonSocial: {
//         type: DataTypes.INTEGER
//     },
//     nombre: {
//         type: DataTypes.STRING
//     },
//     paterno: {
//         type: DataTypes.STRING
//     },
//     materno: {
//         type: DataTypes.STRING
//     },
//     cp: {
//         type: DataTypes.INTEGER
//     },
//     calle: {
//         type: DataTypes.STRING
//     },
//     numInt: {
//         type: DataTypes.INTEGER
//     },
//     numExt: {
//         type: DataTypes.INTEGER
//     },
//     idColonia: {
//         type: DataTypes.INTEGER
//     },
//     genero: {
//         type: DataTypes.STRING
//     },
//     telefonoFijo: {
//         type: DataTypes.INTEGER
//     },
//     telefonoMovil: {
//         type: DataTypes.INTEGER
//     },
//     correo: {
//         type: DataTypes.STRING
//     },
//     fechaNacimiento: {
//         type: DataTypes.DATE
//     },
//     curp: {
//         type: DataTypes.STRING
//     },
//     rfc: {
//         type: DataTypes.STRING
//     },
//     archivo: {
//         type: DataTypes.STRING
//     },
//     archivoSubido: {
//         type: DataTypes.STRING
//     },
//     ruc: {
//         type: DataTypes.STRING
//     },
//     dni: {
//         type: DataTypes.STRING
//     },
//     idColoniaPeru: {
//         type: DataTypes.INTEGER
//     },
// }, {
//     tableName: "cliente",
//     timestamps: false,
//     sequelize,
// });

export default ClienteModelOnline;
