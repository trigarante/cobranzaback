import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import erroresAnexosAModel from "../erroresAnexo/erroresAnexosAModel";

const LogQualitasEmisionModel = dbPostgres.define('logQualitasEmisionModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitudVn: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    request: {
        type: DataTypes.JSON
    },
    response: {
        type: DataTypes.JSON
    },
    error: {
        type: DataTypes.JSON
    },
    poliza: {
        type: DataTypes.STRING
    },
}, {
    tableName: "logQualitasEmision",
    schema: "generales",
    timestamps: false
})
export default LogQualitasEmisionModel