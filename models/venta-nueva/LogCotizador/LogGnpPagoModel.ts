import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import LogQualitasEmisionModel from "./LogQualitasEmisionModel";

const LogGnpPagoModel = dbPostgres.define('logGnpPagoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitudVn: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    request: {
        type: DataTypes.JSON
    },
    response: {
        type: DataTypes.JSON
    },
    poliza: {
        type: DataTypes.STRING
    },
    error: {
        type: DataTypes.JSON
    },
    banderaPoliza: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "logGnpPago",
    schema: "generales",
    timestamps: false
})

export default LogGnpPagoModel