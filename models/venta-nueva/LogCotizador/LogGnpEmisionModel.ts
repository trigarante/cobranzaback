import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import LogQualitasEmisionModel from "./LogQualitasEmisionModel";

const LogGnpEmisionModel = dbPostgres.define('logGnpEmisionModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitudVn: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    request: {
        type: DataTypes.JSON
    },
    response: {
        type: DataTypes.JSON
    },
    error: {
        type: DataTypes.JSON
    },
}, {
    tableName: "logGnpEmision",
    schema: "generales",
    timestamps: false
})

export default LogGnpEmisionModel