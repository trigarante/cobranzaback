import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import LogQualitasEmisionModel from "./LogQualitasEmisionModel";

const LogQualitasPagoModel = dbPostgres.define('logQualitasPagoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitudVn: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    request: {
        type: DataTypes.JSON
    },
    response: {
        type: DataTypes.JSON
    },
    error: {
        type: DataTypes.JSON
    },
    poliza: {
        type: DataTypes.STRING
    },
    banderaPagado: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "logQualitasPago",
    schema: "generales",
    timestamps: false
})
export default LogQualitasPagoModel