import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const RegistroOnlineModel = dbPostgres.define('registro', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idEmpleado: {
        type: DataTypes.BIGINT,
    },
    idProducto: {
        type: DataTypes.BIGINT
    },
    idTipoPago: {
        type: DataTypes.INTEGER
    },
    idEstadoPoliza: {
        type: DataTypes.INTEGER
    },
    idSocio: {
        type: DataTypes.INTEGER
    },
    idProductoSocio: {
        type: DataTypes.BIGINT
    },
    idFlujoPoliza: {
        type: DataTypes.INTEGER
    },
    idPeriodicidad: {
        type: DataTypes.INTEGER
    },
    poliza: {
        type: DataTypes.STRING
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    primaNeta: {
        type: DataTypes.DOUBLE
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    archivo: {
        type: DataTypes.STRING
    },
    fechaFin: {
        type: DataTypes.DATE
    },
    oficina: {
        type: DataTypes.STRING
    },
    fechaCierre: {
        type: DataTypes.DATE
    },
    fechaCobranza: {
        type: DataTypes.DATE
    },
    idDepartamento: {
        type: DataTypes.BIGINT
    },
    emitida: {
        type: DataTypes.SMALLINT
    },
    online: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "registro",
    schema: "operaciones",
    timestamps: false
});

export default RegistroOnlineModel;