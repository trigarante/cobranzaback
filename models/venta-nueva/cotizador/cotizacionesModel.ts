import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const CotizacionesModel = dbPostgres.define('cotizacionesModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idProducto: {
        type: DataTypes.INTEGER,
    },
    idPagina: {
        type: DataTypes.INTEGER
    },
    idMedioDifusion: {
        type: DataTypes.INTEGER
    },
    idTipoContacto: {
        type: DataTypes.INTEGER
    },
    idEstadoCotizacion: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
}, {
    tableName: "cotizaciones",
    schema: "operaciones",
    timestamps: false
})

export default CotizacionesModel;
