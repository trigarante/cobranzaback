import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";


const tipoSubRamoModel = dbPostgres.define('tipoSubRamo', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    tipo: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },

}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "tipoSubRamo",
});

export default tipoSubRamoModel;
