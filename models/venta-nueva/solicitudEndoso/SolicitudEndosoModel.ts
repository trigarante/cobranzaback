import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const SolicitudEndosoModel = dbPostgres.define('SolicitudEndosoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEmpleadoSolicitante: {
        type: DataTypes.INTEGER
    },
    idPenalizacionEndoso: {
        type: DataTypes.INTEGER
    },
    idMotivoEndoso: {
        type: DataTypes.INTEGER
    },
    idEstadoSolicitudEndoso: {
        type: DataTypes.INTEGER
    },
    idEstadoEndoso: {
        type: DataTypes.INTEGER
    },
    fechaSolicitud: {
        type: DataTypes.DATE
    },
    fechaResolucion: {
        type: DataTypes.DATE
    },
    comentarios: {
        type: DataTypes.STRING
    },
    prima: {
        type: DataTypes.FLOAT
    },
    activo: {
        type: DataTypes.INTEGER
    },
    fechaAsignacion: {
        type: DataTypes.DATE
    },
    fechaTramiteAseguradora: {
        type: DataTypes.DATE
    },
    fechaTermino: {
        type: DataTypes.DATE
    },
    fechaResolucionAseguradora: {
        type: DataTypes.DATE
    },
}, {
    timestamps: false,
    schema: "generales",
    tableName: "solicitudEndoso",
});

export default SolicitudEndosoModel

