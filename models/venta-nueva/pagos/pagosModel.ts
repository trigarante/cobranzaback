import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const PagosModel = dbPostgres.define('pagos', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRecibo: {
        type: DataTypes.INTEGER,
    },
    idFormaPago: {
        type: DataTypes.INTEGER
    },
    idEstadoPago: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.BIGINT
    },
    fechaPago: {
        type: DataTypes.DATE
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    cantidad: {
        type: DataTypes.INTEGER
    },
    archivo: {
        type: DataTypes.STRING
    },
    datos: {
        type: DataTypes.JSON
    },
}, {
    tableName: "pagos",
    schema: "operaciones",
    timestamps: false
});
export default PagosModel;
