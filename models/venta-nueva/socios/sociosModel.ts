import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const SociosModel = dbPostgres.define('socios', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idPais: {
        type: DataTypes.SMALLINT,
    },
    prioridad: {
        type: DataTypes.SMALLINT
    },
    nombreComercial: {
        type: DataTypes.STRING
    },
    rfc: {
        type: DataTypes.STRING
    },
    razonSocial: {
        type: DataTypes.STRING
    },
    alias: {
        type: DataTypes.STRING
    },
    idEstadoSocio: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
    expresionRegular: {
        type: DataTypes.STRING
    },
    imagenPlantilla: {
        type: DataTypes.STRING
    },
    ejemploExpresionRegular: {
        type: DataTypes.STRING
    },
}, {
    tableName: "socios",
    schema: 'operaciones',
    timestamps: false
});

export default SociosModel;
