import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const ProductoSolicitudModel = dbPostgres.define('productoSolicitudModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idProspecto: {
        type: DataTypes.INTEGER,
    },
    idTipoSubRamo: {
        type: DataTypes.INTEGER
    },
    datos: {
        type: DataTypes.JSON
    },
}, {
    tableName: "productoSolicitud",
    schema: "operaciones",
    timestamps: false
})

export default ProductoSolicitudModel;
