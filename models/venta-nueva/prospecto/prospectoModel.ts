import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const ProspectoModel = dbPostgres.define('prospectoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    numero: {
        type: DataTypes.STRING,
    },
    correo: {
        type: DataTypes.STRING
    },
    nombre: {
        type: DataTypes.STRING
    },
    sexo: {
        type: DataTypes.STRING
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    edad: {
        type: DataTypes.INTEGER
    },
    idUsuarioApp: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: "prospecto",
    schema: "operaciones",
    timestamps: false
})

export default ProspectoModel;
