import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const SolicitudesCarruselModel= dbPostgres.define('solicitudesCarruselModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idMotivoDesactivacion: {
        type: DataTypes.INTEGER,
    },
    idEstadoSolicitudCarrusel: {
        type: DataTypes.SMALLINT
    },
    idEmpleado: {
        type: DataTypes.BIGINT
    },
    idEmpleadoSolicitante: {
        type: DataTypes.BIGINT
    },
    idEmpleadoCoordinador: {
        type: DataTypes.BIGINT
    },
    idEmpleadoRRHH: {
        type: DataTypes.BIGINT
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    fechaAutorizacionCoordinador: {
        type: DataTypes.DATE
    },
    fechaAutorizacionRRHH: {
        type: DataTypes.DATE
    },
}, {
    tableName: "solicitudesCarrusel",
    schema: "operaciones",
    timestamps: false
});
export default SolicitudesCarruselModel;
