import { DataTypes } from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const CampoCorregirModel = dbPostgres.define('campoCorregir', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idTabla: {
        type: DataTypes.INTEGER,
    },
    campo: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    }
}, {
    tableName: "campoCorregir",
    schema: 'operaciones',
    timestamps: false
});

export default CampoCorregirModel;
