import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const penalizacionesVerificacionModel = dbPostgres.define('penalizacionesVerificacionModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idVerificacionRegistro: {
        type: DataTypes.INTEGER
    },
    idEstadoPenalizacion: {
        type: DataTypes.INTEGER
    },
    idCatalogoErrores: {
        type: DataTypes.INTEGER
    },
    montoPenalizacionEjecutivo: {
        type: DataTypes.DOUBLE
    },
    montoPenalizacionSupervisor: {
        type: DataTypes.DOUBLE
    },
    montoAplicado: {
        type: DataTypes.DOUBLE
    },
    montoAplicadoSupervisor: {
        type: DataTypes.DOUBLE
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    fechaAplicacion: {
        type: DataTypes.DATE
    },
}, {
    timestamps: false,
    schema: "generales",
    tableName: "penalizacionesVerificacion",
});

export default penalizacionesVerificacionModel

