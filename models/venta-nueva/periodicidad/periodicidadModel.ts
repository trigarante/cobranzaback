import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const PeriodicidadModel = dbPostgres.define('periodicidad', {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    cantidadPagos: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    }
}, {
    tableName: "periodicidad",
    schema: 'operaciones',
    timestamps: false
});

export default PeriodicidadModel;
