import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const SubRamoViewModel = dbPostgres.define('subRamoView', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRamo: {
        type: DataTypes.INTEGER,
    },
    idTipoSubRamo: {
        type: DataTypes.INTEGER
    },

    prioridad: {
        type: DataTypes.SMALLINT
    },
    prioridades: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.SMALLINT
    },
    idSocio: {
        type: DataTypes.INTEGER
    },
    tipoRamo: {
        type: DataTypes.STRING
    },
    nombreComercial: {
        type: DataTypes.STRING
    },
    alias: {
        type: DataTypes.STRING
    },
    idEstadoSocio: {
        type: DataTypes.SMALLINT
    },
    estado: {
        type: DataTypes.STRING
    },
}, {
    tableName: "subRamoView",
    schema: 'operaciones',
    timestamps: false
});

export default SubRamoViewModel;