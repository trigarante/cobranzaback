import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";


const AutorizacionPagoModel = dbPostgres.define('autorizacionPago', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idPago: {
        type: DataTypes.INTEGER,
    },
    idEstadoVerificacion: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER
    },
    verificadoPago: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    fechaConclusion: {
        type: DataTypes.DATE
    },
}, {
    tableName: "autorizacionPago",
    schema: "generales",
    timestamps: false
})

export default AutorizacionPagoModel