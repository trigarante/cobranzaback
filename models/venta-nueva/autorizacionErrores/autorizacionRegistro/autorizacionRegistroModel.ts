import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";


const AutorizacionRegistroModel = dbPostgres.define('autorizacionregistro', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    idEstadoVerificacion: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER
    },
    verificadoCliente: {
        type: DataTypes.INTEGER
    },
    verificadoProducto: {
        type: DataTypes.INTEGER
    },
    verificadoRegistro: {
        type: DataTypes.INTEGER
    },
    verificadoPago: {
        type: DataTypes.INTEGER
    },
    verificadoInspeccion: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    fechaConclusion: {
        type: DataTypes.DATE
    },
}, {
    tableName: "autorizacionRegistro",
    schema: "operaciones",
    timestamps: false
})

export default AutorizacionRegistroModel
