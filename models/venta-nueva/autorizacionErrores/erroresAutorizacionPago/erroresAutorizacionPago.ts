import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";

const ErroresAutorizacionPagoModel = dbPostgres.define('ErroresAutorizacionPagoModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idAutorizacionPago: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaCorreccion: {
        type: DataTypes.DATE
    },
    correcciones: {
        type: DataTypes.JSON
    },
}, {
    timestamps: false,
    schema: "generales",
    tableName: "erroresAutorizacionPago",
});

export default ErroresAutorizacionPagoModel