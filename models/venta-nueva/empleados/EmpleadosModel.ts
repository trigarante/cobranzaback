import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const EmpleadosModel = dbPostgres.define('EmpleadosModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idPlaza: {
        type: DataTypes.INTEGER
    },
    idCandidato: {
        type: DataTypes.INTEGER
    },
    fechaAltaIMSS: {
        type: DataTypes.DATE
    },
    documentos: {
        type: DataTypes.JSON
    },
    fechaIngreso: {
        type: DataTypes.DATE
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    fechaFiniquito: {
        type: DataTypes.DATE
    },
    idEstadoFiniquito: {
        type: DataTypes.INTEGER
    },
    montoFiniquito: {
        type: DataTypes.DOUBLE
    },
    cantidadVacaciones: {
        type: DataTypes.BIGINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    timestamps: false,
    schema: "recursosHumanos",
    tableName: "empleado",
});

export default EmpleadosModel

