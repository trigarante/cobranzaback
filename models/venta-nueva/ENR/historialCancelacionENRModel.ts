import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const historialCancelacionENRModel = dbPostgres.define('historialCancelacionENRModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    poliza: {
        type: DataTypes.STRING
    },
    idEmpleadoBaja: {
        type: DataTypes.INTEGER
    },
    fecha: {
        type: DataTypes.DATE
    },
    idMotivo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "historialCancelacionENR",
    schema: "operaciones",
    timestamps: false
})

export default historialCancelacionENRModel;
