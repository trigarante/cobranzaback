import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const catalogoENRModel = dbPostgres.define('catalogoENRModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: DataTypes.STRING,
    }
}, {
    tableName: "catalogoENR",
    schema: "operaciones",
    timestamps: false
})

export default catalogoENRModel;
