import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const ErroresVerificacionModel = dbPostgres.define('ErroresVerificacionModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idVerificacionRegistro: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaCorreccion: {
        type: DataTypes.DATE
    },
    correcciones: {
        type: DataTypes.JSON
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "erroresVerificacion",
});

export default ErroresVerificacionModel

