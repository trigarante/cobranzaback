import {Sequelize, Model, DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const sequelize = dbPostgres;

const solicitudesCarruselModel = dbPostgres.define('solicitudesCarrusel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idMotivoDesactivacion: {
        type: DataTypes.INTEGER
    },
    idEstadoSolicitudCarrusel: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEmpleadoSolicitante: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCoordinador: {
        type: DataTypes.INTEGER
    },
    idEmpleadoRRHH: {
        type: DataTypes.INTEGER
    },
    idflujo: {
        type: DataTypes.INTEGER
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    fechaAutorizacionCoordinador: {
        type: DataTypes.DATE
    },
    fechaAutorizacionRRHH: {
        type: DataTypes.DATE
    },
    archivo: {
        type: DataTypes.STRING
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "solicitudesCarrusel",
});


export default solicitudesCarruselModel;
