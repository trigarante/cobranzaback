import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const RamoModel = dbPostgres.define('ramo', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idSocio: {
        type: DataTypes.INTEGER,
    },
    idTipoRamo: {
        type: DataTypes.INTEGER
    },
    descripcion: {
        type: DataTypes.STRING
    },
    prioridad: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "ramo",
    schema: 'operaciones',
    timestamps: false
});

export default RamoModel;
