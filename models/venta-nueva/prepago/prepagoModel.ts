import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const PrepagoModel = dbPostgres.define('prepago', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.BIGINT,
    },
    nombre: {
        type: DataTypes.STRING
    },
    poliza: {
        type: DataTypes.STRING
    },
    numeroTarjeta: {
        type: DataTypes.STRING
    },
    titular: {
        type: DataTypes.STRING
    },
    csv: {
        type: DataTypes.STRING
    },
    primaTotal: {
        type: DataTypes.DOUBLE
    },
    fechaVigencia: {
        type: DataTypes.DATE
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaVista: {
        type: DataTypes.DATE
    },
    fechaCompra: {
        type: DataTypes.DATE
    },
    idMedioContacto: {
        type: DataTypes.INTEGER
    },
    idFormaPago: {
        type: DataTypes.INTEGER
    },
    idBanco: {
        type: DataTypes.INTEGER
    },
    idCarrier: {
        type: DataTypes.INTEGER
    },
    idRecibo: {
        type: DataTypes.INTEGER
    },
    url: {
        type: DataTypes.INTEGER
    },
    msi: {
        type: DataTypes.STRING
    },
    mesDeVigencia: {
        type: DataTypes.STRING
    },
    anioDeVigencia: {
        type: DataTypes.STRING
    }
}, {
    tableName: "prepago",
    schema: 'operaciones',
    timestamps: false
});

export default PrepagoModel;
