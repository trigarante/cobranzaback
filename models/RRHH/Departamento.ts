// import {Sequelize, Model, DataTypes} from "sequelize";
// import db from "../../configs/connection";
//
// const sequelize = db;
//
// class Departamento extends Model {
//     public id!: number; // Note that the `null assertion` `!` is required in strict mode.
//     public idPadre!: number;
//     public activo!: number;
//     public descripcion!: string; // for nullable fields
// }
//
// Departamento.init(
//     {
//         id: {
//             type: DataTypes.INTEGER.UNSIGNED,
//             autoIncrement: true,
//             primaryKey: true,
//         },
//         idPadre: {
//             type: DataTypes.INTEGER,
//         },
//         activo: {
//             type: DataTypes.TINYINT,
//         },
//         descripcion: {
//             type: new DataTypes.STRING(100)
//         },
//     },
//     {
//         tableName: "departamento",
//         timestamps: false,
//         sequelize, // passing the `sequelize` instance is required
//     }
// );
//
// export default Departamento;
