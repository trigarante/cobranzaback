import {DataTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

const Plaza = dbPostgres.define( 'Plaza',
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        idPadre: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        idDepartamento: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        idPuesto: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        idArea: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        idSede: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        idTipoPlaza: {
            type: DataTypes.SMALLINT,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        idEstadoPlaza: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        activo: {
            type: DataTypes.SMALLINT,
        },
        idTurnoEmpleado: {
            type: DataTypes.INTEGER,
        },
        puestoEspecifico: {
            type: DataTypes.STRING,
        },
        idEmpresaEmpleadora: {
            type: DataTypes.INTEGER,
        },
        idEmpresaPagadora: {
            type: DataTypes.INTEGER,
        },
        sueldoDiario: {
            type: DataTypes.INTEGER,
        },
        sueldoMensual: {
            type: DataTypes.INTEGER,
        },
        idKpi: {
            type: DataTypes.INTEGER,
        },
        ceCo: {
            type: DataTypes.INTEGER,
        }
    },
    {
        tableName: "plaza",
        timestamps: false,
        schema: 'recursosHumanos'
    }
);

export default Plaza;
