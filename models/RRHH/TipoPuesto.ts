// import {Sequelize, Model, DataTypes} from "sequelize";
// import db from "../../configs/connection";
//
//
// const sequelize = db;
//
// class TipoPuesto extends Model {
//     public id!: number; // Note that the `null assertion` `!` is required in strict mode.
//     public activo!: number;
//     public nombre!: string; // for nullable fields
// }
//
// TipoPuesto.init(
//     {
//         id: {
//             type: DataTypes.INTEGER.UNSIGNED,
//             autoIncrement: true,
//             primaryKey: true,
//         },
//         activo: {
//             type: DataTypes.TINYINT,
//         },
//         nombre: {
//             type: DataTypes.STRING(100)
//         },
//     },
//     {
//         tableName: "tipoPuesto",
//         timestamps: false,
//         sequelize, // passing the `sequelize` instance is required
//     }
// );
//
// export default TipoPuesto;
