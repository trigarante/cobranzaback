import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const VerificacionRegistroModel = dbPostgres.define('verificacionRegistro', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    idEstadoVerificacion: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER
    },
    verificadoCliente: {
        type: DataTypes.INTEGER
    },
    verificadoProducto: {
        type: DataTypes.INTEGER
    },
    verificadoRegistro: {
        type: DataTypes.INTEGER
    },
    verificadoPago: {
        type: DataTypes.INTEGER
    },
    verificadoInspeccion: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    fechaConclusion: {
        type: DataTypes.DATE
    },
}, {
    tableName: "verificacionRegistro",
    schema: "operaciones",
    timestamps: false
})

export default VerificacionRegistroModel
