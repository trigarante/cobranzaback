import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const TrigaranteEscucha = dbPostgres.define('trigaranteEscuchaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idEmpleado: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        idEstadoTrigaranteEscucha: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        },
        idMotivoTrigaranteEscucha: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        idArea: {
            type: DataTypes.INTEGER,
            allowNull: true,
            validate: {
                isInt: true,
            }
        },
        fechaRegistro: {
            type: DataTypes.DATE,
        },
        fechaFinal: {
            type: DataTypes.DATE,
        },
        comentarios: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
        comentariosFinales: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
        propuesta: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
        folio: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
    },
    {
        tableName: "trigaranteEscucha",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default TrigaranteEscucha;
