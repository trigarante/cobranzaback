import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const ImagenesGacetaModel = dbPostgres.define('imagenesGacetaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idGaceta:{
            type: DataTypes.INTEGER,
        },
        numero:{
            type: DataTypes.TINYINT,
        },
        archivo: {
            type: DataTypes.STRING,
        },
        link: {
            type: DataTypes.STRING,
        },
        activo: {
            type: DataTypes.TINYINT,
        },
        fechaRegistro: {
            type: new DataTypes.DATE(100)
        },
    },
    {
        tableName: "imagenesGaceta",
        schema: "recursosHumanos",
        timestamps: false,
    })


export default ImagenesGacetaModel;
