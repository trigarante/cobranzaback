import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const VisualizacionesGacetaModel = dbPostgres.define('VisualizacionesGacetaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idEmpleado:{
            type: DataTypes.INTEGER,
        },
        idGaceta:{
            type: DataTypes.INTEGER,
        },
        fechaRegistro: {
            type: new DataTypes.DATE(100)
        },
    },
    {
        tableName: "visualizacionesGaceta",
        schema: "recursosHumanos",
        timestamps: false,
    }
);

export default VisualizacionesGacetaModel;
