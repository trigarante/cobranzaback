import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const MotivoTrigaranteEscucha = dbPostgres.define('MotivoTrigaranteEscuchaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        idTemaTrigaranteEscucha: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        idNivelTrigaranteEscucha: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true,
            }
        },
        descripcion: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
        activo: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        }
    },
    {
        tableName: "motivoTrigaranteEscucha",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default MotivoTrigaranteEscucha;
