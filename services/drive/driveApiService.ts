import * as stream from 'stream';
import * as fs from 'fs';
import * as readline from 'readline';
import {google} from 'googleapis';

export default class DriveApiService {
    // static carpetaPrueba = '16zpomczZlwi3WeB-g6BK-yPtVavUnvFX'; // usuarios@ahorraseguros
     static carpetaPrueba = '1Tbs3CfS-S9qxN4FIflG_TFqq6icYQ--j'; // usuarios@ahorraseguros
    static carpetaJustificaciones = '1HsgJ3blJCoJZgaWpGZ5AKJDUHm5CP-L5';

    static async obtenerArchivo(idFile) {
        const auth = await DriveApiService.authorize();
        const drive = google.drive({version: 'v3', auth});

        return await new Promise<void>((resolve, reject) => drive.files.get({
            fileId: idFile,
            alt: 'media',
        }, {responseType: 'stream'}, (err, {data}) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                const buf = [];
                // @ts-ignore
                data.on('data', (info) => buf.push(info));
                // @ts-ignore
                data.on('end', () => resolve(Buffer.concat(buf).toString('base64')));
            }
        }));
    }

    static async getIdOfMostRecentFile(folderId: any, nombreArchivo: string): Promise<any> {
        const auth = await DriveApiService.authorize();

        return new Promise(resolve => {
            const drive = google.drive({version: 'v3', auth});
            const query = "'" + folderId + "' in parents and name contains '" + nombreArchivo + "' and trashed=false";

            drive.files.list({
                q: query,
                supportsAllDrives: true,
                supportsTeamDrives: true,
                includeTeamDriveItems: true,
                includeItemsFromAllDrives: true,
                fields: 'files(id)',
                pageSize: 1
            }, (err, res: any) => {
                if (err) resolve(null)
                console.log(res);
                resolve(res.data.files[0].id);
            });
        });
    }

    static async subirArchivo(idCarpeta, archivo, nombreArchivo?) {
        // tslint:disable-next-line:no-shadowed-variable
        const auth = await DriveApiService.authorize();
        const drive = google.drive({version: 'v3', auth});
        const archivoSubida = new stream.PassThrough();
        archivoSubida.end(archivo.data);
        return await new Promise<void>((resolve, reject) => drive.files.create({
            requestBody: {
                parents: [idCarpeta],
                name: nombreArchivo || archivo.name,
            },
            media: {
                mimeType: archivo.mimeType,
                body: archivoSubida,
            },
        }, (err, file) => {
            if (err) {
                console.error(err);
                reject(err);
            } else resolve(file.data.id);
        }));
    }

    /** Funciones necesarias para que jale drive **/
    /*Esta función busca una carpeta que coincida con el nombre dado y caso de que no exista, la crea*/
    static async findOrCreateFolder(opcion: number, folderName, folderHijo?): Promise<any> {
        let idParentFolder;
        // Para buscar en la carpeta que se necesita
        switch (opcion) {
            case 1:
                idParentFolder = this.carpetaPrueba;
                break;
            case 2:
                idParentFolder = folderHijo;
            case 3:
                idParentFolder = this.carpetaJustificaciones;
                break;
        }
        const auth = await DriveApiService.authorize();

        return new Promise(resolve => {
            const drive = google.drive({version: 'v3', auth});
            const query = "'" + idParentFolder + "' in parents and name = '" + folderName +
                "' and trashed=false and mimeType= 'application/vnd.google-apps.folder'";

            drive.files.list({
                q: query,
                fields: 'files(id)',
            }, (err, res: any) => {
                if (err)
                    console.log(err);
                if (res.data.files.length <= 0) {
                    drive.files.create({
                        requestBody: {
                            mimeType: 'application/vnd.google-apps.folder',
                            parents: [idParentFolder],
                            name: folderName,
                        },
                        fields: 'id',
                        // tslint:disable-next-line:only-arrow-functions
                    }, function (errr, file: any) {
                        if (errr)
                            console.log(errr);
                        else
                            resolve(file.data.id);
                    });
                } else {
                    resolve(res.data.files[0].id);
                }
            });
        });
    }

    // Funciones necesarias para interactuar con la API
    // Load client secrets from a local file.
    private static loadClienteSecret(): Promise<any> {
        return new Promise((resolve, reject) => {
            fs.readFile('configs/drive/credentials.json', (err, content) => {
                if (err) reject(console.log('Error loading client secret file:', err));
                // Authorize a client with credentials, then call the Google drive API.
                resolve(JSON.parse(content.toString()).installed);
            })
        });
    }

    /*
        Esta función manda a llamar el cliente secreto y valida que exista algún token en el proyecto, en caso de que
        no sea así, manda a que este sea creado, la variable de scope es usada con el fin de identificar el SCOPE necesario
        para la interacción con la API
     */
    private static async authorize(): Promise<any> {
        const {client_secret, client_id, redirect_uris} = await this.loadClienteSecret();
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        // If modifying these scopes, delete token.json.
        const SCOPES = ['https://www.googleapis.com/auth/drive'];
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        const TOKEN_PATH = 'configs/drive/token.json';

        return new Promise((resolve) => {
            // Check if we have previously stored a token.
            fs.readFile(TOKEN_PATH, (err, token) => {
                if (err) resolve(DriveApiService.getAccessToken(oAuth2Client, SCOPES, TOKEN_PATH));
                oAuth2Client.setCredentials(JSON.parse(token.toString()));
                resolve(oAuth2Client);
            });
        });
    }

    /**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
     * @param {getEventsCallback} callback The callback for the authorized client.
     */
    private static async getAccessToken(oAuth2Client: any, scopes: any, path: any): Promise<any> {
        return new Promise(resolve => {
            const authUrl = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: scopes,
            });
            console.log('Authorize this app by visiting this url:', authUrl);
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout,
            });
            rl.question('Enter the code from that page here: ', (code) => {
                rl.close();
                oAuth2Client.getToken(code, (err: any, token: any) => {
                    if (err) console.error('Error retrieving access token', err);
                    oAuth2Client.setCredentials(token);
                    // Store the token to disk for later program executions
                    fs.writeFile(path, JSON.stringify(token), (errr) => {
                        if (errr) console.error(errr);
                        console.log('Token stored to', path);
                        resolve(oAuth2Client);
                    });
                });
            });
        });
    }
}
