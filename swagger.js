const swaggerAutogen = require('swagger-autogen')()

const doc = {
    info: {
        version: "1.0.0",
        title: "Cobranza",
        description: "Documentación de los servicios del proyecto de Cobranza"
    },
    // host: "localhost:8080",
    // host de producción, para cuando se suba se debe descomentar
    host: "cobranza.trigarante2022.com",
    basePath: "/v1/",
    docExpansion: "none",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
}

const outputFile = 'swagger-output.json'
const endpointsFiles = ['routes/*.ts']

swaggerAutogen(outputFile, endpointsFiles, doc);
