
export const generarDiasArray = (diasMes: number, mes: number, anio: number) => {
    let header1;
    let header2;
    const dias = [];
    if (mes.toString().length === 1)
        header1 = '0' + mes.toString();
    else
        header1 = mes.toString();
    for (let i = 1; i <= diasMes; i++) {
        if (i < 10)
            header2 = '0' + i;
        else
            header2 = i;
        const dia = { def: `${anio}-${mes < 10 ? '0'+mes : mes}-${i < 10 ? '0'+i : i}`, header: header2 + '/' + header1, num: i, fecha: `${anio}-${mes < 10 ? '0'+mes : mes}-${i < 10 ? '0'+i : i}` }
        dias.push(dia)
    }
    return dias;
}
