export default  {
    escucha: {
        title: 'SEGUIMIENTO TRIGARANTE ESCUCHA',
        mensaje: `¡Hola!

Esperando te encuentres bien, te informamos que tu solicitud ha sido recibida y registrada, en breve nos comunicaremos contigo.

En caso de tener alguna duda o comentario, por favor no dudes en contactarnos al correo: trigarantescucha@trigarante.com

Le deseo tenga un excelente día`,
    }
}
