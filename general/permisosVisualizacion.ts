import DashboardQueries from "../queries/dashboard/dashboardQueries";
import PlazaQueries from "../queries/RRHH/plazaQueries";
import UsuariosDepartamentoQueries from "../queries/usuariosDepartamento/usuariosDepartamentoQueries";

async function switchPermisosVisualizacion (permisos, fechaInicio, fechaFin, idEmpleado): Promise<any[] | undefined> {
    try{
        let dash;
        let aux2;
        const aux: any[] = [];
        const {idPuesto, idDepartamento, idPermisoVisualizacion, id, idUsuario} = permisos;
        switch (+idPermisoVisualizacion) {
            case 1:
                return dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin, aux2=``, aux).catch(e => console.log(e));
                break;
            case 2:
                // if (+idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     const hijos: any[] = await PlazaQueries.getPlazasHijo(id);
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     // let aux=[24, 25, 25, 28]
                //     aux2 = `AND operaciones.solicitudes."idEmpleado" IN (${aux})`;
                //     return dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin, aux2, aux).catch(e => console.log(e));
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(idUsuario);
                    for (const departamento of departamentos) {
                        aux.push(departamento.idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(+idDepartamento)
                    }
                    aux2 = `AND "recursosHumanos".plaza."idDepartamento" IN (${aux})`;
                    return dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin, aux2, aux).catch(e => console.log(e));
                // }
                break;
            case 3:
                aux2 = `AND "recursosHumanos".plaza."idDepartamento" = ${idDepartamento} AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}`;
                return dash = await DashboardQueries.getDashboard(fechaInicio, fechaFin, aux2, aux).catch(e => console.log(e));
                break;
        }
    } catch (e){
        throw (e)
    }
}

async function switchPermisosVisualizacionByArea (permisos, fechaInicio, fechaFin, idEmpleado, idSubarea): Promise<any[] | undefined> {
    try{
        let dash;
        let aux2;
        const aux: any[] = [];
        const {idPuesto, idDepartamento, idPermisoVisualizacion, id, idUsuario} = permisos;
        switch (+idPermisoVisualizacion) {
            case 1:
                aux2 = `AND "recursosHumanos".departamento.id = ${idSubarea}`;
                return dash = await DashboardQueries.getDashboardIdSubarea(fechaInicio, fechaFin,idSubarea, aux2, aux).catch(e => console.log(e));
                break;
            case 2:
                // if (+idPuesto === 7) {
                //     // Supervisores solo deben de ver lo de sus chavos
                //     const hijos: any[] = await PlazaQueries.getPlazasHijo(id);
                //     for (const hijo of hijos) {
                //         aux.push(hijo.id)
                //     }
                //     if (hijos.length === 0){
                //         aux.push(+idEmpleado)
                //     }
                //     // let aux=[24, 25, 25, 28]
                //     aux2 = `AND "recursosHumanos".departamento.id = ${idSubarea} AND operaciones.solicitudes."idEmpleado" IN (${aux})`;
                //     return dash = await DashboardQueries.getDashboardIdSubarea(fechaInicio, fechaFin,idSubarea, aux2, aux).catch(e => console.log(e));
                // } else {
                    const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(idUsuario);
                    for (const departamento of departamentos) {
                        aux.push(+idDepartamento)
                    }
                    if (departamentos.length === 0){
                        aux.push(+idDepartamento)
                    }
                    aux2 = `AND "recursosHumanos".departamento.id = ${idSubarea} AND "recursosHumanos".plaza."idDepartamento" IN (${aux})`;
                    return dash = await DashboardQueries.getDashboardIdSubarea(fechaInicio, fechaFin,idSubarea, aux2, aux).catch(e => console.log(e));
                // }
                break;
            case 3:
                aux2 = `AND "recursosHumanos".departamento.id = ${idSubarea} AND "recursosHumanos".plaza."idDepartamento" = ${idDepartamento} AND operaciones.solicitudes."idEmpleado" = ${idEmpleado}`;
                return dash = await DashboardQueries.getDashboardIdSubarea(fechaInicio, fechaFin,idSubarea, aux2, aux).catch(e => console.log(e));
                break;
        }
    } catch (e){
        throw (e)
    }
}

module.exports.switchPermisosVisualizacion = switchPermisosVisualizacion;
module.exports.switchPermisosVisualizacionByArea = switchPermisosVisualizacionByArea;
